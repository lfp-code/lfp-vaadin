package com.lfp.vaadin.component.token.field;

import java.util.Objects;
import java.util.function.Function;
import java.util.regex.Pattern;

import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.css.Style;
import com.vaadin.data.HasItems;
import com.vaadin.data.HasValue;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;

@SuppressWarnings({ "serial" })
public class CssLayoutTokenField<T, I extends HasItems<T> & HasValue<T>> extends AbstractTokenField<T, I, CssLayout> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Style FLEX_STYLE = Style.build(styleName -> {
		var css = Utils.Resources.getResourceAsString("com/lfp/vaadin/component/token/field/flex.css").get();
		css = css.replaceAll(Pattern.quote("_STYLE_NAME_"), styleName);
		return css;
	});
	private final Function<T, Token<T>> tokenFunction;

	public CssLayoutTokenField(I inputField, Function<T, Token<T>> tokenFunction) {
		super(inputField);
		this.tokenFunction = Objects.requireNonNull(tokenFunction);
		this.getInputField().setWidthFull();
		FLEX_STYLE.add(this);
		this.getInputField().addValueChangeListener(new ValueChangeListener<T>() {
			private boolean updating;

			@Override
			public void valueChange(ValueChangeEvent<T> event) {

				if (updating)
					return;
				updating = true;
				try {
					var value = event.getValue();
					if (value == null)
						return;
					setValue(Utils.Lots.stream(getValue()).append(value).toList());
					getInputField().clear();
				} finally {
					updating = false;
				}

			}
		});

	}

	@Override
	protected Token<T> createToken(T value) {
		return this.tokenFunction.apply(value);
	}

	@Override
	protected CssLayout createMainLayout() {
		return this;
	}

	@Override
	protected void addComponent(CssLayout mainLayout, Component c, int index) {
		mainLayout.addComponent(c, index);
	}

}
