package com.lfp.vaadin.component.token.field;

import java.util.Objects;
import java.util.function.Function;
import java.util.regex.Pattern;

import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.css.CSSs;
import com.lfp.vaadin.base.css.Style;
import com.vaadin.data.HasItems;
import com.vaadin.data.HasValue;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;

@SuppressWarnings({ "serial", "unchecked" })
public class HorizontalLayoutTokenField<T, I extends HasItems<T> & HasValue<T>>
		extends AbstractTokenField<T, I, HorizontalLayout> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private final Function<T, Token<T>> tokenFunction;

	public HorizontalLayoutTokenField(I inputField, Function<T, Token<T>> tokenFunction) {
		super(inputField);
		this.tokenFunction = Objects.requireNonNull(tokenFunction);
		this.getInputField().setWidthFull();
		this.getMainLayout().setExpandRatio(inputField, 1f);
		this.getMainLayout().setSpacing(false);
		this.getMainLayout().setMargin(false);
		this.getInputField().addValueChangeListener(new ValueChangeListener<T>() {
			private boolean updating;

			@Override
			public void valueChange(ValueChangeEvent<T> event) {

				if (updating)
					return;
				updating = true;
				try {
					var value = event.getValue();
					if (value == null)
						return;
					setValue(Utils.Lots.stream(getValue()).append(value).toList());
					getInputField().clear();
				} finally {
					updating = false;
				}

			}
		});

	}

	@Override
	protected Token<T> createToken(T value) {
		return this.tokenFunction.apply(value);
	}

	@Override
	protected HorizontalLayout createMainLayout() {
		return new HorizontalLayout();
	}

	@Override
	protected void addComponent(HorizontalLayout mainLayout, Component c, int index) {
		mainLayout.addComponent(c, index);
	}

}
