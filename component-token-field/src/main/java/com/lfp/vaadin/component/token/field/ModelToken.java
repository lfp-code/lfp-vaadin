package com.lfp.vaadin.component.token.field;

public class ModelToken<T> extends com.fo0.advancedtokenfield.model.Token {

	private final Token<T> token;

	public ModelToken(Token<T> token) {
		super(token.getId(), token.getCaption(), token.getDescription(), token.getDescriptionContentMode(),
				token.getStyle());
		this.token = token;
	}

	public Token<T> getToken() {
		return token;
	}
}
