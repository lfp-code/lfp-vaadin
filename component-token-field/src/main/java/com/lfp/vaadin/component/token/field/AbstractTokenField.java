package com.lfp.vaadin.component.token.field;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.css.CSSs;
import com.lfp.vaadin.base.css.Style;
import com.lfp.vaadin.base.data.HasValues;

import com.fo0.advancedtokenfield.model.TokenLayout;
import com.vaadin.data.HasItems;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Layout;

import one.util.streamex.EntryStream;

public abstract class AbstractTokenField<T, I extends HasItems<T> & HasValue<T>, L extends Layout> extends CssLayout
		implements TokenField<T, I> {
	private static final String BASE_STYLE = "advancedtokenfield-layouttokens";
	private static final Style CLOSE_BUTTON_STYLE = Style.build(styleName -> {
		String css = ".%s { margin-left: 4px !important;}";
		return String.format(css, styleName);
	});
	private static final Style READ_ONLY_HTML_STYLE = Style.build(styleName -> {
		var css = Utils.Resources.getResourceAsString("com/lfp/vaadin/component/token/field/readOnlyHtml.css").get();
		css = css.replaceAll(Pattern.quote("_STYLE_NAME_"), styleName);
		return css;
	});

	private final I inputField;
	private final L mainLayout;
	private final HasValue<List<T>> hasValueDelegate;
	private boolean tokenCloseButtonEnabled = true;

	public AbstractTokenField(I inputField) {
		CSSs.addStyleName(this, BASE_STYLE);
		this.inputField = Objects.requireNonNull(inputField);
		this.mainLayout = Objects.requireNonNull(this.createMainLayout());
		this.hasValueDelegate = HasValues.create(this, () -> {
			return streamContent().values().map(Token::getValue).toImmutableList();
		}, event -> {
			var values = Utils.Lots.stream(event.getValue()).nonNull().toList();
			for (var ent : streamContent().toList()) {
				if (values.remove(ent.getValue().getValue()))
					continue;
				this.mainLayout.removeComponent(ent.getKey());
			}
			int addIndex = (int) (streamContent().count() - 1);
			for (var value : values) {
				var tokenLayout = createTokenLayout(value);
				if (tokenLayout == null)
					continue;
				addIndex++;
				this.addComponent(this.mainLayout, tokenLayout, addIndex);
			}
		});
		if (mainLayout != this) {
			this.addComponent(mainLayout);
			mainLayout.setSizeFull();
		}
		this.mainLayout.addComponent(this.inputField);

	}

	@SuppressWarnings("unchecked")
	public String readOnlyHtml() {
		return readOnlyHtml(streamContent().values().toArray(Token.class));
	}

	@SuppressWarnings("unchecked")
	public String readOnlyHtml(Token<T>... tokens) {
		List<String> htmls = new ArrayList<>();
		for (var token : Utils.Lots.stream(tokens).nonNull()) {
			String styleNames = Utils.Lots.stream(READ_ONLY_HTML_STYLE.getName(), token.getStyle())
					.filter(Utils.Strings::isNotBlank).joining(" ");
			var html = String.format("<span class=\"%s\">%s</span>", styleNames, token.getCaption());
			htmls.add(html);
		}
		var html = Utils.Lots.stream(htmls).joining();
		if (Utils.Strings.isNotBlank(html))
			READ_ONLY_HTML_STYLE.add();
		return html;
	}

	public void setTokenCloseButtonEnabled(boolean enabled) {
		this.tokenCloseButtonEnabled = enabled;
	}

	public boolean isTokenCloseButtonEnabled() {
		return tokenCloseButtonEnabled;
	}

	@Override
	public void setValue(List<T> value) {
		hasValueDelegate.setValue(value);
	}

	@Override
	public List<T> getValue() {
		return hasValueDelegate.getValue();
	}

	@Override
	public Registration addValueChangeListener(ValueChangeListener<List<T>> listener) {
		return hasValueDelegate.addValueChangeListener(listener);
	}

	@Override
	public void setRequiredIndicatorVisible(boolean requiredIndicatorVisible) {
		hasValueDelegate.setRequiredIndicatorVisible(requiredIndicatorVisible);
	}

	@Override
	public boolean isRequiredIndicatorVisible() {
		return hasValueDelegate.isRequiredIndicatorVisible();
	}

	@Override
	public void setReadOnly(boolean readOnly) {
		hasValueDelegate.setReadOnly(readOnly);
	}

	@Override
	public boolean isReadOnly() {
		return hasValueDelegate.isReadOnly();
	}

	@Override
	public I getInputField() {
		return inputField;
	}

	@SuppressWarnings("unchecked")
	protected EntryStream<TokenLayout, Token<T>> streamContent() {
		return Utils.Lots.stream(this.mainLayout.iterator()).select(TokenLayout.class).mapToEntry(v -> v, v -> {
			return ((ModelToken<T>) v.getToken()).getToken();
		});
	}

	protected TokenLayout createTokenLayout(T value) {
		if (value == null)
			return null;
		var token = createToken(value);
		if (token == null)
			return null;
		TokenLayout tokenLayout = new TokenLayout(token.getModelToken(), "", null, isTokenCloseButtonEnabled());
		if (tokenLayout.getBtn() != null) {
			CLOSE_BUTTON_STYLE.add(tokenLayout.getBtn());
			tokenLayout.getBtn().setIcon(VaadinIcons.CLOSE_CIRCLE);
			if (isTokenCloseButtonEnabled())
				tokenLayout.getBtn().addClickListener(e -> {
					var layoutValueList = streamContent().toList();
					layoutValueList.removeIf(ent -> {
						return tokenLayout == ent.getKey();
					});
					setValue(Utils.Lots.streamEntries(layoutValueList).values().map(Token::getValue).toList());
				});
		}
		return tokenLayout;
	}

	public L getMainLayout() {
		return mainLayout;
	}

	protected abstract L createMainLayout();

	protected abstract void addComponent(L mainLayout, Component c, int index);

	protected abstract Token<T> createToken(T value);

}
