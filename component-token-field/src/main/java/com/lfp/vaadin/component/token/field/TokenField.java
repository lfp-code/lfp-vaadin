package com.lfp.vaadin.component.token.field;

import java.util.List;

import com.vaadin.data.HasItems;
import com.vaadin.data.HasValue;
import com.vaadin.ui.Layout;

public interface TokenField<T, I extends HasItems<T> & HasValue<T>> extends HasValue<List<T>>, Layout {

	void setTokenCloseButtonEnabled(boolean enabled);

	boolean isTokenCloseButtonEnabled();

	I getInputField();

	String readOnlyHtml();

	String readOnlyHtml(Token<T>... tokens);
}