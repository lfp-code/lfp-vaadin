package com.lfp.vaadin.component.token.field;

import com.lfp.vaadin.base.component.ComponentEvent;

public interface TokenFieldEvent<T> extends ComponentEvent {

	@Override
	HorizontalLayoutTokenField<T, ?> getComponent();

	Token<T> getToken();

	public static interface Add<T> extends TokenFieldEvent<T> {

	}

	public static interface Remove<T> extends TokenFieldEvent<T> {

	}

	public static interface Click<T> extends TokenFieldEvent<T> {

	}

}
