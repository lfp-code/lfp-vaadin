var selector = "{{{SELECTOR}}}"
var enable = JSON.parse("{{{ENABLE}}}");
var styleName = "shiftUserSelectDisable";
var contextKey = (selector + "_" + styleName).split("").reduce((hex, c) => hex += c.charCodeAt(0).toString(16).padStart(4, "0"), "");
for (var element of document.querySelectorAll(selector)) {
	var context = window[contextKey]
	if (enable) {
		if (context != null)
			return;
		context = {};
		window[contextKey] = context;
		context.keydown = (e) => {
			if (e.key == 'Shift' && e.shiftKey)
				element.classList.add(styleName);
		};
		context.keyup = () => {
			element.classList.remove(styleName);
		};
		window.addEventListener('keydown', context.keydown);
		window.addEventListener('keyup', context.keyup);
	} else {
		if (context == null)
			return;
		window.removeEventListener('keydown', context.keydown);
		window.removeEventListener('keyup', context.keyup);
		delete window[contextKey];
	}
}