var callback = arguments[arguments.length - 1];
var scriptId = "js" + Array.from({ length: 10 }, () => Math.random().toString(36)[2]).join('');
console.log(`scriptId:${scriptId}`)
var callbackSuccessName = "success_" + scriptId;
var callbackErrorName = "error_" + scriptId;
function cleanup(){
	document.getElementById(scriptId).remove();
	delete window[callbackSuccessName]
	delete window[callbackErrorName]
}

window[callbackSuccessName] = function () {
	cleanup()
	callback(arguments)
}
window[callbackErrorName] = function () {
	cleanup()
	var errorDatas = [];
	if (arguments != null) {
		for (var argument of arguments)
			errorDatas.push(argument);
	}
	for (var i = 0; i < errorDatas.length; i++) {
		var errorData = errorDatas[i];
		if (errorData == null || errorData === "") {
			errorDatas.splice(i, 1);
			i--;
		}
	}
	if (errorDatas.length == 0)
		errorDatas = ["[unknown error]"]
	var result = errorDatas.length == 1 ? errorDatas[0] : errorDatas;
	callback(JSON.parse(JSON.stringify(result)));
}
var scriptElement = document.createElement("script");
scriptElement.setAttribute("id", scriptId);
scriptElement.setAttribute("src", `https://api.ipify.org?format=jsonp&callback=${callbackSuccessName}`);
scriptElement.setAttribute("onerror", `${callbackErrorName}()`);
scriptElement.setAttribute("type", "text/javascript");
document.head.appendChild(scriptElement);
