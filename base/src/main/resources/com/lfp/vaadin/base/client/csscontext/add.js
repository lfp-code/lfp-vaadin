var id = '{{{ID}}}';
var css = '{{{CSS}}}';
var style = document.createElement('style');
(document.head || document.getElementsByTagName('head')[0]).appendChild(style);
style.type = 'text/css';
style.id = id;
if (style.styleSheet)
	style.styleSheet.cssText = css; // This is required for IE8 and below.
else
	style.appendChild(document.createTextNode(css));