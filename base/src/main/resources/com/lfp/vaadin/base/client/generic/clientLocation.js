var callback = arguments[arguments.length - 1];

function cloneAsObject(obj) {
	if (obj === null || !(obj instanceof Object))
		return obj;
	var temp = (obj instanceof Array) ? [] : {};
	for (var key in obj)
		temp[key] = cloneAsObject(obj[key]);
	return temp;
}

function getCurrentPositionCallback() {
	var result = cloneAsObject(arguments[0]);
	callback(result);
}

if (navigator.geolocation)
	navigator.geolocation.getCurrentPosition(getCurrentPositionCallback, getCurrentPositionCallback);
else
	getCurrentPositionCallback();