package com.lfp.vaadin.base.v7.container;

public interface ContainerEvent<ID, B> {

	ContainerLFP<ID, B> getContainer();
}
