package com.lfp.vaadin.base.session;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.vaadin.base.attributes.Attributes;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WrappedSession;
import com.vaadin.ui.UI;

public abstract class SessionAttributes extends Attributes {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private static final String SESSION_ATTRIBUTES_KEY = "SESSION_ATTRIBUTES_" + UUID.randomUUID().toString();

	@SuppressWarnings("deprecation")
	public static SessionAttributes getCurrent() {
		var session = VaadinSession.getCurrent();
		if (session == null || session.isClosing()) {
			var ui = UI.getCurrent();
			if (ui != null && !ui.isClosing())
				session = ui.getSession();
		}
		return get(session, true);
	}

	public static SessionAttributes get(VaadinSession session, boolean create) {
		Objects.requireNonNull(session, () -> String.format("%s lookup error. thread:%s",
				VaadinSession.class.getSimpleName(), Thread.currentThread().getName()));
		SessionAttributes sessionAttributes = (SessionAttributes) session.getAttribute(SESSION_ATTRIBUTES_KEY);
		if (sessionAttributes == null) {
			synchronized (session) {
				sessionAttributes = (SessionAttributes) session.getAttribute(SESSION_ATTRIBUTES_KEY);
				if (sessionAttributes == null) {
					sessionAttributes = Attributes.create(SessionAttributes.class, session);
					session.setAttribute(SESSION_ATTRIBUTES_KEY, sessionAttributes);
				}
			}
		}
		return sessionAttributes;
	}

	private final String id;
	private final VaadinSession session;

	protected SessionAttributes(VaadinSession session) {
		super();
		this.session = Objects.requireNonNull(session);
		this.id = UUID.randomUUID().toString();
	}

	@Override
	public void close() {
		if (MachineConfig.isDeveloper()) {
			var summary = String.format("id:%s sessiondId:%s uiIds:%s", id,
					Optional.ofNullable(this.session)
							.map(VaadinSession::getSession)
							.map(WrappedSession::getId)
							.orElse(null),
					com.lfp.joe.stream.Streams.of(this.session.getUIs()).map(UI::getUIId).toList());
			logger.info("closing session attributes. " + summary);
		}
		super.close();
	}

}
