package com.lfp.vaadin.base.attributes;

import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;
import java.util.function.Supplier;

import org.threadly.concurrent.future.FutureUtils;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.Primitives;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.executor.ExecutorServices.ShutdownUnsupported;

import javassist.util.proxy.Proxy;

@SuppressWarnings("unchecked")
public abstract class Attributes implements Map<String, Object>, AutoCloseable {

	public static Attributes create() {
		return create(Attributes.class);
	}

	public static <U extends Attributes> U create(Class<U> superclass, Object... arguments) {
		var proxyFactory = JavaCode.Proxies.createFactory(superclass);
		var instance = Throws.unchecked(() -> {
			return proxyFactory.create(arguments);
		});
		((Proxy) instance).setHandler((self, thisMethod, proceed, args) -> {
			try {
				if (proceed != null)
					return proceed.invoke(self, args);
				return ((Attributes) self).accessBackingMap(map -> thisMethod.invoke(map, args));
			} catch (Throwable t) {
				throw Throws.unwrap(t, InvocationTargetException.class);
			}
		});
		return (U) instance;
	}

	private static final org.slf4j.Logger LOGGER = Logging.logger(Attributes.class);
	private static final Duration CLOSE_WARN_DURATION = Duration.ofSeconds(5);

	private final UUID instanceKeyPostfix;
	private final ReadWriteLock readWriteLock;
	private Map<String, Object> _backingMap;
	private boolean closed;

	public Attributes() {
		instanceKeyPostfix = UUID.randomUUID();
		readWriteLock = new ReentrantReadWriteLock();
	}

	public <X> X get(Class<X> classType) {
		return (X) get(getInstanceKey(classType));
	}

	public <X> X remove(Class<X> classType) {
		return (X) remove(getInstanceKey(classType));
	}

	public <X> X put(X value) {
		Objects.requireNonNull(value);
		return (X) put(getInstanceKey(value.getClass()), value);
	}

	public <X> X compute(Class<X> classType, Function<? super X, ? extends X> remappingFunction) {
		Objects.requireNonNull(remappingFunction);
		var key = getInstanceKey(classType);
		return (X) compute(key, (k, v) -> {
			return remappingFunction.apply((X) v);
		});
	}

	public <X> X computeIfAbsent(Class<X> classType, Supplier<? extends X> mappingSupplier) {
		Objects.requireNonNull(mappingSupplier);
		var key = getInstanceKey(classType);
		return (X) computeIfAbsent(key, (k) -> {
			return mappingSupplier.get();
		});
	}

	public <X> X computeIfPresent(Class<X> classType, Function<? super X, ? extends X> remappingFunction) {
		Objects.requireNonNull(remappingFunction);
		var key = getInstanceKey(classType);
		return (X) compute(key, (k, v) -> {
			return remappingFunction.apply((X) v);
		});
	}

	protected String getInstanceKey(Class<?> classType) {
		Objects.requireNonNull(classType);
		classType = CoreReflections.getNamedClassType(classType);
		classType = Primitives.getWrapperType(classType).orElse(classType);
		return classType.getName() + instanceKeyPostfix.toString();
	}

	@Override
	protected void finalize() throws Throwable {
		close();
		super.finalize();
	}

	@Override
	public void close() {
		if (this.closed)
			return;
		readWriteLock.writeLock().lock();
		try {
			closeLocked();
		} finally {
			readWriteLock.writeLock().unlock();
		}
	}

	protected void closeLocked() {
		if (this.closed)
			return;
		if (_backingMap == null || _backingMap.isEmpty()) {
			this.closed = true;
			return;
		}
		var closeExecutor = Threads.Pools.centralPool().limit();
		var closeFutures = EntryStreams.of(_backingMap).values().nonNull().<AutoCloseable>map(v -> {
			if (v instanceof AutoCloseable)
				return (AutoCloseable) v;
			if (v instanceof ExecutorService && !(v instanceof ShutdownUnsupported))
				return () -> ((ExecutorService) v).shutdownNow();
			return null;
		}).nonNull().distinct().map(autoCloseable -> {
			var future = closeExecutor.submit(() -> {
				autoCloseable.close();
				return null;
			});
			Threads.Futures.logFailureWarn(future, true, "attribute close error");
			return future;
		}).toList();
		var remaining = new AtomicInteger(closeFutures.size());
		closeFutures.forEach(v -> v.listener(remaining::decrementAndGet));
		var closeFuture = FutureUtils.makeCompleteFuture(closeFutures);
		_backingMap.clear();
		_backingMap = null;
		var startedAt = System.currentTimeMillis();
		var warnFuture = FutureUtils.executeWhile(() -> {
			return Threads.Pools.centralPool().submitScheduled(() -> {
				LOGGER.warn("attributes close stuck. remaining:{} elapsed:{}s", remaining.get(),
						Duration.ofMillis(System.currentTimeMillis() - startedAt).toSeconds());
			}, CLOSE_WARN_DURATION.toMillis());
		}, nil -> {
			return !closeFuture.isDone();
		});
		closeFuture.listener(() -> warnFuture.cancel(true));
		this.closed = true;
	}

	protected <U, T extends Throwable> U accessBackingMap(ThrowingFunction<Map<String, Object>, U, T> accessor)
			throws T {
		validateNotClosed();
		if (_backingMap == null) {
			readWriteLock.writeLock().lock();
			try {
				validateNotClosed();
				if (_backingMap == null)
					_backingMap = new ConcurrentHashMap<>();
			} finally {
				readWriteLock.writeLock().unlock();
			}
		}
		readWriteLock.readLock().lock();
		try {
			validateNotClosed();
			return accessor.apply(_backingMap);
		} finally {
			readWriteLock.readLock().unlock();
		}
	}

	protected void validateNotClosed() {
		if (this.closed)
			throw new IllegalStateException("attributes closed");
	}

	public static void main(String[] args) throws InterruptedException {
		var attrs = Attributes.create();
		for (int i = 0; i < 10; i++) {
			Thread.sleep(500);
			System.out.println(attrs.computeIfAbsent(Date.class, Date::new));
		}
		attrs.close();
		System.out.println(attrs.computeIfAbsent(Date.class, Date::new));
	}

}
