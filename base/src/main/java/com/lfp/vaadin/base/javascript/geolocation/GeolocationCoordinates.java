package com.lfp.vaadin.base.javascript.geolocation;

import java.util.Optional;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lfp.joe.location.Location;

public class GeolocationCoordinates {

	private Integer errorCode;

	private String errorMessage;

	@SerializedName("coords")
	@Expose
	private Coords coords;
	@SerializedName("timestamp")
	@Expose
	private Double timestamp;

	public Optional<Location> toLocation() {
		if (isError())
			return Optional.empty();
		var coords = getCoords();
		if (coords.isEmpty() || coords.get().getLatitude() == null || coords.get().getLongitude() == null)
			return Optional.empty();
		var result = Location.builder().latitude(coords.get().latitude).longitude(coords.get().getLongitude()).build();
		return Optional.of(result);
	}

	public boolean isError() {
		return errorCode != null && errorCode >= 0;
	}

	public Optional<Coords> getCoords() {
		return Optional.ofNullable(coords);
	}

	public void setCoords(Coords coords) {
		this.coords = coords;
	}

	public Double getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Double timestamp) {
		this.timestamp = timestamp;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public static class Coords {

		@SerializedName("latitude")
		@Expose
		private Double latitude;
		@SerializedName("longitude")
		@Expose
		private Double longitude;
		@SerializedName("altitude")
		@Expose
		private Double altitude;
		@SerializedName("accuracy")
		@Expose
		private long accuracy;
		@SerializedName("altitudeAccuracy")
		@Expose
		private Double altitudeAccuracy;
		@SerializedName("heading")
		@Expose
		private Double heading;
		@SerializedName("speed")
		@Expose
		private Double speed;

		public Double getLatitude() {
			return latitude;
		}

		public void setLatitude(Double latitude) {
			this.latitude = latitude;
		}

		public Double getLongitude() {
			return longitude;
		}

		public void setLongitude(Double longitude) {
			this.longitude = longitude;
		}

		public Double getAltitude() {
			return altitude;
		}

		public void setAltitude(Double altitude) {
			this.altitude = altitude;
		}

		public long getAccuracy() {
			return accuracy;
		}

		public void setAccuracy(long accuracy) {
			this.accuracy = accuracy;
		}

		public Double getAltitudeAccuracy() {
			return altitudeAccuracy;
		}

		public void setAltitudeAccuracy(Double altitudeAccuracy) {
			this.altitudeAccuracy = altitudeAccuracy;
		}

		public Double getHeading() {
			return heading;
		}

		public void setHeading(Double heading) {
			this.heading = heading;
		}

		public Double getSpeed() {
			return speed;
		}

		public void setSpeed(Double speed) {
			this.speed = speed;
		}

		@Override
		public String toString() {
			return "Coords [latitude=" + latitude + ", longitude=" + longitude + ", altitude=" + altitude
					+ ", accuracy=" + accuracy + ", altitudeAccuracy=" + altitudeAccuracy + ", heading=" + heading
					+ ", speed=" + speed + "]";
		}

	}

	@Override
	public String toString() {
		return "GeolocationCoordinates [errorCode=" + errorCode + ", errorMessage=" + errorMessage + ", coords="
				+ coords + ", timestamp=" + timestamp + "]";
	}

}
