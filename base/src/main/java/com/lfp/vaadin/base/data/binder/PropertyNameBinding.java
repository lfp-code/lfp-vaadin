package com.lfp.vaadin.base.data.binder;

import java.util.Objects;

import org.apache.commons.lang3.Validate;

import com.vaadin.data.Binder.Binding;
import com.vaadin.data.BindingValidationStatus;
import com.vaadin.data.BindingValidationStatusHandler;
import com.vaadin.data.HasValue;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.ui.Grid;

public interface PropertyNameBinding<B, V> extends Binding<B, V> {

	String getPropertyName();

	default PropertyNameBinding<B, V> bindToColumnEditor(Grid<B> grid) {
		grid.getColumn(this.getPropertyName()).setEditorBinding(this);
		return this;
	}

	@SuppressWarnings("serial")
	public static <B, V> PropertyNameBinding<B, V> create(Binding<B, V> binding, String propertyName) {
		Objects.requireNonNull(binding);
		Validate.notBlank(propertyName);
		return new PropertyNameBinding<B, V>() {

			@Override
			public String getPropertyName() {
				return propertyName;
			}

			@Override
			public HasValue<?> getField() {
				return binding.getField();
			}

			@Override
			public BindingValidationStatus<V> validate() {
				return binding.validate();
			}

			@Override
			public BindingValidationStatus<V> validate(boolean fireEvent) {
				return binding.validate(fireEvent);
			}

			@Override
			public BindingValidationStatusHandler getValidationStatusHandler() {
				return binding.getValidationStatusHandler();
			}

			@Override
			public void unbind() {
				binding.unbind();
			}

			@Override
			public void read(B bean) {
				binding.read(bean);
			}

			@Override
			public void setReadOnly(boolean readOnly) {
				binding.setReadOnly(readOnly);
			}

			@Override
			public boolean isReadOnly() {
				return binding.isReadOnly();
			}

			@Override
			public ValueProvider<B, V> getGetter() {
				return binding.getGetter();
			}

			@Override
			public Setter<B, V> getSetter() {
				return binding.getSetter();
			}

			@Override
			public void setAsRequiredEnabled(boolean asRequiredEnabled) {
				binding.setAsRequiredEnabled(asRequiredEnabled);
			}

			@Override
			public boolean isAsRequiredEnabled() {
				return binding.isAsRequiredEnabled();
			}

			@Override
			public void setValidatorsDisabled(boolean validatorsDisabled) {
				binding.setValidatorsDisabled(validatorsDisabled);
			}

			@Override
			public boolean isValidatorsDisabled() {
				return binding.isValidatorsDisabled();
			}

			@Override
			public boolean isConvertBackToPresentation() {
				return binding.isConvertBackToPresentation();
			}

			@Override
			public void setConvertBackToPresentation(boolean convertBackToPresentation) {
				binding.setConvertBackToPresentation(convertBackToPresentation);
			}

		};
	}
}
