package com.lfp.vaadin.base.data.property;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.v7.property.PropertyDescriptorLFP;

import org.apache.commons.lang3.Validate;
import org.joda.beans.MetaProperty;

import com.github.throwable.beanref.BeanPath;
import com.vaadin.data.PropertyDefinition;
import com.vaadin.data.PropertySet;

import one.util.streamex.StreamEx;

@SuppressWarnings({ "deprecation", "serial", "unchecked", "rawtypes" })
public interface PropertySetLFP<X> extends PropertySet<X> {

	@Override
	StreamEx<PropertyDefinition<X, ?>> getProperties();

	default <P> Optional<PropertyDefinitionLFP<X, P>> getProperty(MetaProperty<P> metaProperty) {
		var op = this.getProperty((Object) metaProperty);
		if (op.isEmpty())
			return Optional.empty();
		var mpType = Utils.Types.wrapperToPrimitive(metaProperty.propertyType());
		var resultType = Utils.Types.wrapperToPrimitive(op.get().getType());
		Validate.isAssignableFrom(mpType, resultType);
		return (Optional) op;
	}

	default <P> Optional<PropertyDefinitionLFP<X, P>> getProperty(BeanPath<X, P> beanPath) {
		var op = this.getProperty((Object) beanPath);
		if (op.isEmpty())
			return Optional.empty();
		var mpType = Utils.Types.wrapperToPrimitive(beanPath.getType());
		var resultType = Utils.Types.wrapperToPrimitive(op.get().getType());
		Validate.isAssignableFrom(mpType, resultType);
		return (Optional) op;
	}

	default Optional<PropertyDefinitionLFP<X, ?>> getProperty(Object propertyId) {
		var propertyNameOp = Properties.tryGetName(propertyId);
		if (propertyNameOp.isEmpty())
			return Optional.empty();
		return getProperty(propertyNameOp.get()).map(PropertyDefinitionLFP::create);
	}

	default PropertySetLFP<X> filter(Predicate<PropertyDefinition<X, ?>> filter) {
		return filter(filter, false);
	}

	default PropertySetLFP<X> filter(Predicate<PropertyDefinition<X, ?>> filter, boolean disableCache) {
		if (filter == null)
			return this;
		PropertySetLFP<X> self = this;
		return new PropertySetLFP<X>() {

			private final Map<PropertyDefinition<X, ?>, Boolean> filterCache = disableCache ? null
					: new ConcurrentHashMap<>();

			private boolean passesFilter(PropertyDefinition<X, ?> propertyDefinition) {
				if (propertyDefinition == null)
					return false;
				if (filterCache == null)
					return filter.test(propertyDefinition);
				return filterCache.computeIfAbsent(propertyDefinition, nil -> filter.test(propertyDefinition));
			}

			@Override
			public Optional<PropertyDefinition<X, ?>> getProperty(String name) {
				var op = self.getProperty(name);
				var passes = passesFilter(op.orElse(null));
				if (!passes)
					return Optional.empty();
				return op;
			}

			@Override
			public StreamEx<PropertyDefinition<X, ?>> getProperties() {
				var stream = self.getProperties();
				stream = stream.filter(this::passesFilter);
				return stream;
			}
		};
	}

	default StreamEx<PropertyDescriptorLFP<X, ?>> streamPropertyDescriptors() {
		StreamEx<PropertyDefinitionLFP<X, ?>> pdefStream = Optional.ofNullable(this.getProperties())
				.orElse(StreamEx.empty()).map(PropertyDefinitionLFP::create);
		StreamEx<PropertyDescriptorLFP<X, ?>> pdescStream = pdefStream.map(PropertyDefinitionLFP::asPropertyDescriptor);
		return pdescStream;
	}

}
