package com.lfp.vaadin.base.client;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.EventObject;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.threadly.concurrent.future.ListenableFuture;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.ReflectionMethodCache;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.session.SessionEventBus;
import com.lfp.vaadin.base.ui.RefreshEvent;
import com.vaadin.event.EventRouter;
import com.vaadin.server.AbstractClientConnector;
import com.vaadin.server.ClientConnector;
import com.vaadin.server.ClientConnector.AttachListener;
import com.vaadin.server.ClientConnector.DetachListener;
import com.vaadin.ui.UI;

@SuppressWarnings({ "unchecked", "deprecation" })
public class Connectors {

	public static void fireEvent(AbstractClientConnector component, EventObject eventObject) {
		ReflectionMethodCache.getDefault(AbstractClientConnector.class).invokeVoid("fireEvent", component, eventObject);
	}

	// some fuckery to make this work in vaadin 7

	// *** ATTACH ***

	private static final Method ClientConnector_addAttachListener_METHOD;
	static {
		ClientConnector_addAttachListener_METHOD = Utils.Functions.unchecked(() -> {
			return ClientConnector.class.getMethod("addAttachListener", AttachListener.class);
		});
	}

	public static Scrapable addAttachListener(ClientConnector clientConnector, AttachListener attachListener) {
		Objects.requireNonNull(clientConnector);
		Utils.Functions.unchecked(() -> {
			ClientConnector_addAttachListener_METHOD.invoke(clientConnector, attachListener);
		});
		return Scrapable.create(() -> clientConnector.removeAttachListener(attachListener));
	}

	// *** DETACH ***

	private static final Method ClientConnector_addDetachListener_METHOD;
	static {
		ClientConnector_addDetachListener_METHOD = Utils.Functions.unchecked(() -> {
			return ClientConnector.class.getMethod("addDetachListener", DetachListener.class);
		});
	}

	public static Scrapable addDetachListener(ClientConnector clientConnector, DetachListener detachListener) {
		Objects.requireNonNull(clientConnector);
		Utils.Functions.unchecked(() -> {
			ClientConnector_addDetachListener_METHOD.invoke(clientConnector, detachListener);
		});
		return Scrapable.create(() -> clientConnector.removeDetachListener(detachListener));
	}

	public static <C extends ClientConnector> Scrapable addStateChangeListener(C input,
			Consumer<ConnectorStateChangeEvent> listener) {
		return addStateChangeListener(input, listener, null);
	}

	public static <C extends ClientConnector> Scrapable addStateChangeListener(C input,
			Consumer<ConnectorStateChangeEvent> listener,
			Consumer<ConnectorStateChangeEventOptions.Builder> configureOptions) {
		Objects.requireNonNull(input);
		ConnectorStateChangeEventOptions options;
		{
			var optionsBuilder = ConnectorStateChangeEventOptions.builder();
			if (configureOptions != null)
				configureOptions.accept(optionsBuilder);
			options = optionsBuilder.build();
		}
		var scrapable = Scrapable.create();
		BiConsumer<Boolean, Boolean> fireEvent = (attached, refreshTrigger) -> {
			attached = Boolean.TRUE.equals(attached);
			refreshTrigger = Boolean.TRUE.equals(refreshTrigger);
			listener.accept(new ConnectorStateChangeEvent(input, attached, refreshTrigger));
			if (!attached && options.isScrapOnDetach())
				scrapable.scrap();
		};
		try {
			{// on attach
				var listenerScrapable = addAttachListener(input, evt -> fireEvent.accept(true, false));
				scrapable.onScrap(listenerScrapable::scrap);
			}
			{// on detach
				var listenerScrapable = addDetachListener(input, evt -> fireEvent.accept(false, false));
				scrapable.onScrap(listenerScrapable::scrap);
			}
			{// on refresh
				var eventBus = SessionEventBus.getCurrent();
				var refreshListener = eventBus.subscribe((Consumer<RefreshEvent>) evt -> {
					var uiId = Optional.ofNullable(input.getUI()).map(UI::getUIId).orElse(null);
					if (uiId == null)
						return;
					if (Objects.equals(uiId, evt.getComponent().getUIId()))
						fireEvent.accept(input.isAttached(), true);
				});
				scrapable.onScrap(() -> eventBus.unsubscribe(refreshListener));
			}
			if (options.isFireIfAttached() && input.isAttached())
				fireEvent.accept(input.isAttached(), false);
			if (options.isFireIfDeteached() && !input.isAttached())
				fireEvent.accept(input.isAttached(), false);
		} catch (Throwable t) {
			scrapable.scrap();
			throw Utils.Exceptions.asRuntimeException(t);
		}
		return scrapable;
	}

	private static final LoadingCache<Class<? extends ClientConnector>, Optional<Field>> ClientConnector_eventRouter_FIELD_CACHE = Caffeine
			.newBuilder().expireAfterAccess(Duration.ofSeconds(1)).build(classType -> {
				var stream = JavaCode.Reflections.streamFields(classType, true,
						v -> Utils.Strings.equalsIgnoreCase(v.getName(), "eventRouter"),
						v -> EventRouter.class.equals(v.getType()));
				var list = stream.toList();
				if (list.size() != 1)
					return Optional.empty();
				var field = list.get(0);
				field.setAccessible(true);
				return Optional.of(field);
			});

	public static Optional<EventRouter> getEventRouter(ClientConnector clientConnector) {
		if (clientConnector == null)
			return Optional.empty();
		var fieldOp = ClientConnector_eventRouter_FIELD_CACHE.get(clientConnector.getClass());
		if (fieldOp.isEmpty())
			return Optional.empty();
		EventRouter eventRouter;
		try {
			eventRouter = (EventRouter) fieldOp.get().get(clientConnector);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw (((Object) e) instanceof java.lang.RuntimeException) ? java.lang.RuntimeException.class.cast(e)
					: new RuntimeException(e);
		}
		return Optional.ofNullable(eventRouter);
	}


}
