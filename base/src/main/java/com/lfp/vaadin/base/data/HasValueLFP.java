package com.lfp.vaadin.base.data;

import com.vaadin.data.HasValue;

public interface HasValueLFP<V> extends HasValue<V> {

	default void setValue(V value) {
		setValue(value, false);
	}

	void setValue(V value, boolean userOriginated);
}
