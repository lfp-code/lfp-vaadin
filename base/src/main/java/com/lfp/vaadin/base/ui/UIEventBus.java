package com.lfp.vaadin.base.ui;

import com.lfp.joe.events.bobbus.BobBus;

public class UIEventBus extends BobBus<Object> {

	public static UIEventBus getCurrent() {
		return UIX.getCurrent().getAttributes().computeIfAbsent(UIEventBus.class, () -> new UIEventBus());
	}
}
