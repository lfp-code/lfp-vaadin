package com.lfp.vaadin.base.session;

import java.util.concurrent.Executor;

import com.lfp.joe.events.bobbus.BobBus;
import com.lfp.joe.threads.Executors;

import net.engio.mbassy.bus.config.Feature;

public class SessionEventBus extends BobBus<Object> {

	private SessionEventBus(Executor executor) {
		super(ops -> {
			var asyncInvoker = ops.getFeature(Feature.AsynchronousHandlerInvocation.class);
			asyncInvoker.setExecutor(Executors.asExecutorService(executor));
		});
	}

	public static SessionEventBus getCurrent() {
		// pass this to avoid recursive updates
		var sessionExecutor = SessionExecutor.getCurrent();
		return SessionAttributes.getCurrent().computeIfAbsent(SessionEventBus.class,
				() -> new SessionEventBus(sessionExecutor));
	}

}
