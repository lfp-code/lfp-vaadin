package com.lfp.vaadin.base.data;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Supplier;

import com.lfp.vaadin.base.client.Connectors;
import com.lfp.vaadin.base.component.Components;

import com.vaadin.data.HasValue;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.shared.Registration;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Component;

@SuppressWarnings({ "deprecation", "serial" })
public class HasValues {

	public static <V> HasValueLFP<V> create(AbstractComponent component) {
		var valueRef = new AtomicReference<V>();
		return create(component, valueRef::get, evt -> valueRef.set(evt.getValue()));
	}

	public static <V> HasValueLFP<V> create(AbstractComponent component, Supplier<V> getter,
			Consumer<ValueChangeEvent<V>> setter) {
		Objects.requireNonNull(getter);
		Objects.requireNonNull(setter);
		return new AbstractHasValue<V>() {

			@Override
			protected Component getComponent() {
				return component;
			}

			@Override
			public Registration addValueChangeListener(ValueChangeListener<V> listener) {
				return component.addListener(ValueChangeEvent.class, listener, ValueChangeListener.VALUE_CHANGE_METHOD);
			}

			@Override
			public void setRequiredIndicatorVisible(boolean requiredIndicatorVisible) {
				Components.setRequiredIndicatorVisible(component, requiredIndicatorVisible);

			}

			@Override
			public boolean isRequiredIndicatorVisible() {
				return Components.isRequiredIndicatorVisible(component);
			}

			@Override
			public void setReadOnly(boolean readOnly) {
				Components.setReadOnly(component, readOnly);
			}

			@Override
			public boolean isReadOnly() {
				return Components.isReadOnly(component);
			}

			@Override
			protected void fireEvent(ValueChangeEvent<V> valueChangeEvent) {
				Connectors.fireEvent(component, valueChangeEvent);
			}

			@Override
			public V getValue() {
				return getter.get();
			}

			@Override
			protected void setValueInternal(ValueChangeEvent<V> valueChangeEvent) {
				setter.accept(valueChangeEvent);
			}

		};
	}
}
