package com.lfp.vaadin.base.v7.data;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.v7.property.PropertiesV7;
import com.vaadin.v7.data.Property;
import com.vaadin.v7.data.util.BeanItem;
import com.vaadin.v7.data.util.VaadinPropertyDescriptor;

@SuppressWarnings({ "unchecked", "deprecation", "serial", "rawtypes" })
public class BeanItemLFP<BT> extends BeanItem<BT> {

	private static final Object DUMMY_BEAN = new Object();
	private static final FieldAccessor<BeanItem, Object> BEAN_ACCESSOR = JavaCode.Reflections
			.getFieldAccessorUnchecked(BeanItem.class, true, v -> "bean".equals(v.getName()));
	protected final Nada setupPropertyDescriptorsComplete;
	private boolean beanSet;

	public BeanItemLFP(ItemMetadata itemMetadata) {
		this(itemMetadata, Collections.emptyList());
	}

	public BeanItemLFP(ItemMetadata itemMetadata, Iterable<VaadinPropertyDescriptor<BT>> propertyDescriptors) {
		this(itemMetadata, propertyDescriptors, Optional.empty());
	}

	public BeanItemLFP(ItemMetadata itemMetadata, Iterable<VaadinPropertyDescriptor<BT>> propertyDescriptors, BT bean) {
		this(itemMetadata, propertyDescriptors, Optional.of(bean));
	}

	protected BeanItemLFP(ItemMetadata itemMetadata, Iterable<VaadinPropertyDescriptor<BT>> propertyDescriptors,
			Optional<BT> beanOp) {
		super((BT) DUMMY_BEAN, Collections.emptyList());
		BEAN_ACCESSOR.set(this, null);
		setupPropertyDescriptorsComplete = setupPropertyDescriptors(propertyDescriptors);
		this.addItemProperty(ItemMetadata.propertyDescriptor(), (Property) null);
		this.getItemProperty(ItemMetadata.propertyDescriptor().getName()).setValue(itemMetadata);
		if (beanOp.isPresent())
			setBean(beanOp.get());
	}

	private Nada setupPropertyDescriptors(Iterable<VaadinPropertyDescriptor<BT>> propertyDescriptors) {
		Objects.requireNonNull(propertyDescriptors);
		com.lfp.joe.stream.Streams.of(this.getItemPropertyIds()).forEach(v -> this.removeItemProperty(v));
		for (VaadinPropertyDescriptor<BT> pd : com.lfp.joe.stream.Streams.of(propertyDescriptors).nonNull()) {
			if (ItemMetadata.propertyDescriptor().getName().equals(pd.getName()))
				continue;
			addItemProperty(pd);
		}
		return Nada.get();
	}

	public ItemMetadata getItemMetadata() {
		var property = this.getItemProperty(ItemMetadata.propertyDescriptor().getName());
		if (property == null)
			return null;
		return (ItemMetadata) property.getValue();
	}

	public boolean addItemProperty(VaadinPropertyDescriptor<BT> propertyDescriptor) {
		return addItemProperty(propertyDescriptor, (Object) null);
	}

	public boolean addItemProperty(VaadinPropertyDescriptor<BT> propertyDescriptor, Object unsetBeanValue) {
		Objects.requireNonNull(propertyDescriptor);
		var unsetBeanProperty = PropertiesV7.create(propertyDescriptor.getName(),
				(Class) propertyDescriptor.getPropertyType(), () -> unsetBeanValue, null);
		return addItemProperty(propertyDescriptor, unsetBeanProperty);
	}

	public <P> boolean addItemProperty(VaadinPropertyDescriptor<BT> propertyDescriptor, Property<P> unsetBeanProperty) {
		Objects.requireNonNull(propertyDescriptor);
		super.removeItemProperty(propertyDescriptor.getName());
		if (unsetBeanProperty == null)
			return addItemProperty(propertyDescriptor.getName(), propertyDescriptor.createProperty(getBean()));
		MemoizedSupplier<Property> propertySupplier = Utils.Functions
				.memoize(() -> propertyDescriptor.createProperty(getBean()));
		Property<P> property = PropertiesV7.create(propertyDescriptor.getName(),
				(Class) propertyDescriptor.getPropertyType(), () -> {
					if (!isBeanSet())
						return unsetBeanProperty.getValue();
					return propertySupplier.get().getValue();
				}, v -> {
					if (!isBeanSet()) {
						unsetBeanProperty.setValue((P) v);
						return;
					}
					propertySupplier.get().setValue(v);
				});
		return addItemProperty(propertyDescriptor.getName(), property);
	}

	@Override
	public void setBean(BT bean) {
		this.setBean(bean, null);
	}

	public void setBean(BT bean, Predicate<Object> filterPropertyIds) {
		Objects.requireNonNull(bean);
		if (getBean() == bean)
			return;
		Validate.isTrue(this.getBean() == null, "bean already set, can't replace");
		BEAN_ACCESSOR.set(this, bean);
		if (filterPropertyIds == null)
			filterPropertyIds = pid -> true;
		com.lfp.joe.stream.Streams.of(this.getItemPropertyIds()).filter(filterPropertyIds).map(v -> this.getItemProperty(v))
				.nonNull().forEach(p -> PropertiesV7.fireValueChange(p));
		this.beanSet = true;
	}

	public boolean isBeanSet() {
		return beanSet;
	}

}
