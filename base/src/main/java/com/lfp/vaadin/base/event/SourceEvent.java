package com.lfp.vaadin.base.event;

import java.util.EventObject;

public class SourceEvent<X> extends EventObject {

	public SourceEvent(X source) {
		super(source);
	}

	@Override
	public X getSource() {
		return (X) super.getSource();
	}

}
