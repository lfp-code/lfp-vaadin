package com.lfp.vaadin.base.v7.grid;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.function.BiFunction;

import org.apache.commons.lang3.Validate;
import org.vaadin.teemusav7.gridextensions.client.tableselection.TableSelectionState;
import org.vaadin.teemusav7.gridextensions.tableselection.TableSelectionModel;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

public enum TableSelectionModelGetStateFunction
		implements BiFunction<TableSelectionModel, Boolean, TableSelectionState> {
	INSTANCE;

	@SuppressWarnings("unchecked")
	private static final MemoizedSupplier<Method> TableSelectionModel_getState_METHOD_SUPPLIER = Utils.Functions
			.memoize(() -> {
				var list = JavaCode.Reflections.streamMethods(TableSelectionModel.class, false,
						v -> "getState".equals(v.getName()), v -> v.getParameterCount() == 1,
						v -> boolean.class.isAssignableFrom(v.getParameterTypes()[0]),
						v -> TableSelectionState.class.isAssignableFrom(v.getReturnType())).toList();
				Validate.isTrue(list.size() == 1, "could not find get state method");
				var method = list.get(0);
				method.setAccessible(true);
				return method;
			});

	@Override
	public TableSelectionState apply(TableSelectionModel tableSelectionModel, Boolean markAsDirty) {
		Objects.requireNonNull(tableSelectionModel);
		markAsDirty = Boolean.TRUE.equals(markAsDirty);
		try {
			return (TableSelectionState) TableSelectionModel_getState_METHOD_SUPPLIER.get().invoke(tableSelectionModel,
					markAsDirty);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw (((Object) e) instanceof java.lang.RuntimeException) ? java.lang.RuntimeException.class.cast(e)
					: new RuntimeException(e);
		}
	}

	public static void main(String[] args) {
		TableSelectionModel_getState_METHOD_SUPPLIER.get();
	}
}
