package com.lfp.vaadin.base.v7.container.sort;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.v7.container.ContainerLFP;
import com.lfp.vaadin.base.v7.property.PropertiesV7;

import one.util.streamex.StreamEx;

public interface Sorter<IDTYPE, BEANTYPE> {

	void sort(ContainerLFP<IDTYPE, BEANTYPE> container, Sorts sorts, List<IDTYPE> itemIds);

	
}
