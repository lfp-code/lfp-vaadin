package com.lfp.vaadin.base.grid;

import java.util.Collection;

import org.vaadin.viritin.fluency.ui.FluentAbstractComponent;
import org.vaadin.viritin.grid.MGrid;

import com.vaadin.data.PropertySet;
import com.vaadin.data.provider.DataCommunicator;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.ui.Grid;

@SuppressWarnings("serial")
public class GridLFP<T> extends Grid<T> implements FluentAbstractComponent<MGrid<T>> {

	public GridLFP() {
		super();

	}

	public GridLFP(Class<T> beanType) {
		super(beanType);

	}

	public GridLFP(Class<T> beanType, DataCommunicator<T> dataCommunicator) {
		super(beanType, dataCommunicator);

	}

	public GridLFP(DataCommunicator<T> dataCommunicator) {
		super(dataCommunicator);

	}

	public GridLFP(DataProvider<T, ?> dataProvider) {
		super(dataProvider);

	}

	public GridLFP(PropertySet<T> propertySet) {
		super(propertySet);

	}

	public GridLFP(PropertySet<T> propertySet, DataCommunicator<T> dataCommunicator) {
		super(propertySet, dataCommunicator);

	}

	public GridLFP(String caption) {
		super(caption);

	}

	public GridLFP(String caption, Collection<T> items) {
		super(caption, items);

	}

	public GridLFP(String caption, DataProvider<T, ?> dataProvider) {
		super(caption, dataProvider);

	}

}
