package com.lfp.vaadin.base.v7.container;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.data.property.Properties;
import com.lfp.vaadin.base.session.SessionEventBus;
import com.lfp.vaadin.base.v7.container.events.GetItemIdsEvent;
import com.lfp.vaadin.base.v7.container.filter.BatchFilterFunction;
import com.lfp.vaadin.base.v7.container.filter.FilterInterceptEvent;
import com.lfp.vaadin.base.v7.container.filter.IDFilter;
import com.lfp.vaadin.base.v7.container.sort.Sorter;
import com.lfp.vaadin.base.v7.container.sort.SorterImpl;
import com.lfp.vaadin.base.v7.container.sort.Sorts;
import com.lfp.vaadin.base.v7.data.BeanItemLFP;
import com.lfp.vaadin.base.v7.data.ItemMetadata;
import com.lfp.vaadin.base.v7.property.PropertiesV7;
import com.vaadin.data.PropertySet;
import com.vaadin.event.EventRouter;
import com.vaadin.shared.Registration;
import com.vaadin.v7.data.Container;
import com.vaadin.v7.data.Container.Filterable;
import com.vaadin.v7.data.Container.PropertySetChangeNotifier;
import com.vaadin.v7.data.Container.SimpleFilterable;
import com.vaadin.v7.data.Container.Sortable;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.Property;
import com.vaadin.v7.data.Property.ValueChangeEvent;
import com.vaadin.v7.data.Property.ValueChangeListener;
import com.vaadin.v7.data.Property.ValueChangeNotifier;
import com.vaadin.v7.data.util.AbstractInMemoryContainer;
import com.vaadin.v7.data.util.BeanItem;
import com.vaadin.v7.data.util.ItemSorter;
import com.vaadin.v7.data.util.ListSet;
import com.vaadin.v7.data.util.VaadinPropertyDescriptor;
import com.vaadin.v7.data.util.filter.SimpleStringFilter;
import com.vaadin.v7.data.util.filter.UnsupportedFilterException;
import com.vaadin.v7.server.communication.data.RpcDataProviderExtension;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

@SuppressWarnings({ "unchecked", "deprecation", "rawtypes", "serial" })
public class ContainerLFP<IDTYPE, BEANTYPE> extends AbstractInMemoryContainer<IDTYPE, String, BeanItem<BEANTYPE>>
		implements Filterable, SimpleFilterable, Sortable, ValueChangeListener, PropertySetChangeNotifier {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int APPLY_FILTERS_BATCH_SIZE_DEFAULT = 100;

	private int applyFiltersBatchSize = APPLY_FILTERS_BATCH_SIZE_DEFAULT;

	/**
	 * Maps all item ids in the container (including filtered) to their
	 * corresponding BeanItem.
	 */
	private final Map<IDTYPE, BeanItem<BEANTYPE>> itemIdToItem = new LinkedHashMap<IDTYPE, BeanItem<BEANTYPE>>();

	/**
	 * The type of the beans in the container.
	 */
	private final Class<? super BEANTYPE> type;

	/**
	 * A description of the properties found in beans of type {@link #type}.
	 * Determines the property ids that are present in the container.
	 */
	private final Map<String, Entry<VaadinPropertyDescriptor<BEANTYPE>, Property>> model = new LinkedHashMap<>();
	private final Map<Object, Boolean> forcePropertyIdSortEnabled = new ConcurrentHashMap<>();
	private final EventRouter eventRouter = new EventRouter();
	private BatchFilterFunction<Entry<IDTYPE, BeanItemLFP<BEANTYPE>>> batchFilterFunction;

	private Supplier<Sorter<IDTYPE, BEANTYPE>> itemSorterSupplier = Utils.Functions.memoize(() -> new SorterImpl<>());
	private Sorts sorts = Sorts.builder().build();

	public ContainerLFP(Class<BEANTYPE> beanType, PropertySet<BEANTYPE> propertySet) {
		this(beanType, Properties.createSet(propertySet).streamPropertyDescriptors());
	}

	public ContainerLFP(Class<BEANTYPE> beanType,
			Iterable<? extends VaadinPropertyDescriptor<BEANTYPE>> propertyDescriptors) {
		this.type = Objects.requireNonNull(beanType);
		Objects.requireNonNull(propertyDescriptors);
		StreamEx<VaadinPropertyDescriptor<BEANTYPE>> pdStream;
		{
			pdStream = com.lfp.joe.stream.Streams.of(propertyDescriptors).map(v -> v);
			pdStream = pdStream.append(ItemMetadata.propertyDescriptor());
			pdStream = pdStream.map(Objects::requireNonNull);
			var uniqueTracker = new HashSet<String>();
			pdStream = pdStream.map(v -> {
				Validate.isTrue(uniqueTracker.add(v.getName()), "duplicate property id:" + v.getName());
				return v;
			});
		}
		pdStream.forEach(pd -> {
			this.model.put(pd.getName(), Utils.Lots.entry(pd, null));
		});
		this.setItemSorter(new ItemSorter() {

			@Override
			public void setSortProperties(Sortable container, Object[] propertyIds, boolean[] ascendings) {
				ContainerLFP.this.sorts = Sorts.builder(propertyIds, ascendings).build();
			}

			@Override
			public int compare(Object itemId1, Object itemId2) {
				return 0;
			}
		});
	}

	public BeanItemLFP<BEANTYPE> add(IDTYPE itemId) {
		Objects.requireNonNull(itemId);
		var map = addInternal(EntryStream.of(itemId, null));
		return map.isEmpty() ? null : map.values().iterator().next();
	}

	public BeanItemLFP<BEANTYPE> add(IDTYPE itemId, BEANTYPE bean) {
		Objects.requireNonNull(itemId);
		Objects.requireNonNull(bean);
		var map = addInternal(EntryStream.of(itemId, bean));
		return map.isEmpty() ? null : map.values().iterator().next();
	}

	public Map<IDTYPE, BeanItemLFP<BEANTYPE>> addAll(Iterable<IDTYPE> itemIds) {
		return addInternal(com.lfp.joe.stream.Streams.of(itemIds).mapToEntry(v -> v, v -> null));
	}

	public Map<IDTYPE, BeanItemLFP<BEANTYPE>> addAll(Map<IDTYPE, BEANTYPE> itemIdToBeanMap) {
		return addInternal(EntryStreams.of(itemIdToBeanMap).mapValues(v -> v));
	}

	public void addContainerFilterWithId(Object filterId, Filter filter) {
		removeContainerFilterWithId(filterId);
		var idFilter = IDFilter.create(filterId, filter);
		this.addContainerFilter(idFilter);
	}

	public boolean addContainerProperty(VaadinPropertyDescriptor<BEANTYPE> propertyDescriptor) {
		return addContainerProperty(propertyDescriptor, (Object) null);
	}

	public <P> boolean addContainerProperty(VaadinPropertyDescriptor<BEANTYPE> propertyDescriptor, P unsetBeanValue) {
		Property<P> unsetBeanProperty = PropertiesV7.create(propertyDescriptor.getName(),
				(Class) propertyDescriptor.getPropertyType(), () -> unsetBeanValue, null);
		return addContainerProperty(propertyDescriptor, unsetBeanProperty);
	}

	public <P> boolean addContainerProperty(VaadinPropertyDescriptor<BEANTYPE> propertyDescriptor,
			Property<P> unsetBeanProperty) {
		Objects.requireNonNull(propertyDescriptor);
		Objects.requireNonNull(unsetBeanProperty);
		removeContainerProperty(propertyDescriptor.getName());
		model.put(propertyDescriptor.getName(), Utils.Lots.entry(propertyDescriptor, unsetBeanProperty));
		// If remove the Property from all Items
		for (final IDTYPE id : getAllItemIds())
			getUnfilteredItem(id).addItemProperty(propertyDescriptor, unsetBeanProperty);
		// Sends a change event
		fireContainerPropertySetChange();
		return true;
	}

	public Filter createBeanFilter(Predicate<BEANTYPE> beanFilter) {
		Objects.requireNonNull(beanFilter);
		return new Filter() {

			@Override
			public boolean appliesToProperty(Object propertyId) {
				return true;
			}

			@Override
			public boolean passesFilter(Object itemId, Item item) throws UnsupportedOperationException {
				BeanItemLFP<BEANTYPE> beanItem = Utils.Types.tryCast(item, BeanItemLFP.class).orElse(null);
				if (!beanItem.isBeanSet())
					return false;
				var bean = beanItem.getBean();
				if (bean == null)
					return false;
				return beanFilter.test(bean);
			}
		};
	}

	public Optional<BEANTYPE> getBean(Object itemId) {
		var item = this.getItem(itemId);
		return Optional.ofNullable(item).map(BeanItemLFP::getBean);
	}

	@Override
	public BeanItemLFP<BEANTYPE> getItem(Object itemId) {
		return getUnfilteredItem(itemId);
	}

	@Override
	public List<IDTYPE> getItemIds(int startIndex, int numberOfIds) {
		var itemIds = super.getItemIds(startIndex, numberOfIds);
		var callingClass = CoreReflections.getCallingClass(THIS_CLASS);
		GetItemIdsEvent.Action action;
		if (RpcDataProviderExtension.class.isAssignableFrom(callingClass))
			action = GetItemIdsEvent.Action.RPC;
		else
			action = GetItemIdsEvent.Action.DIRECT;
		this.eventRouter.fireEvent(new GetItemIdsEvent<>(this, action, itemIds));
		return itemIds;
	}

	@Override
	public Collection<?> getSortableContainerPropertyIds() {
		var pids = com.lfp.joe.stream.Streams.of(forcePropertyIdSortEnabled.keySet());
		{// add all comparable props
			var estream = com.lfp.joe.stream.Streams.of(this.getContainerPropertyIds())
					.mapToEntry(v -> v, v -> this.getType(v));
			estream = estream.filterValues(v -> Comparable.class.isAssignableFrom(v));
			pids = pids.append(estream.keys());
		}
		pids = pids.nonNull().distinct();
		pids = pids.filter(v -> {
			var sortEnabled = forcePropertyIdSortEnabled.get(v);
			if (sortEnabled == null)
				return true;
			return sortEnabled;
		});
		var pidList = pids.toList();
		return pidList;
	}

	@Override
	public Class<?> getType(Object propertyId) {
		var entry = model.get(propertyId);
		if (entry == null)
			return null;
		VaadinPropertyDescriptor<BEANTYPE> descriptor = entry.getKey();
		if (descriptor == null)
			return null;
		return descriptor.getPropertyType();
	}

	public int indexOfIdUnfiltered(IDTYPE itemId) {
		return getAllItemIds().indexOf(itemId);
	}

	public int loadIndexOf(IDTYPE itemId) {
		if (itemId == null)
			return -1;
		int index = -1;
		for (var key : this.itemIdToItem.keySet()) {
			index++;
			if (Objects.equals(itemId, key))
				return index;
		}
		return -1;
	}

	public boolean removeContainerFilterWithId(Object filterId) {
		if (filterId == null)
			return false;
		var removeFilters = com.lfp.joe.stream.Streams.of(this.getFilters())
				.select(IDFilter.class)
				.filter(v -> filterId.equals(v.getId()))
				.toList();
		if (removeFilters.isEmpty())
			return false;
		removeFilters.forEach(v -> removeContainerFilter(v));
		return true;
	}

	public void setSorts(Sorts sorts) {
		this.setSorts(sorts, false);
	}

	public void setSorts(Sorts sorts, boolean forceSort) {
		if (this.sorts == null)
			this.sorts = Sorts.builder().build();
		if (!forceSort && this.sorts.getSortId().equals(sorts.getSortId()))
			return;
		this.sorts = sorts;
		this.sort(sorts.getPropertyIds(), sorts.getAscending());
	}

	public void setPropertyIdSortable(Object propertyId, boolean sortable) {
		for (var pid : this.getContainerPropertyIds()) {
			if (Objects.equals(pid, propertyId)) {
				this.forcePropertyIdSortEnabled.put(pid, sortable);
				return;
			}
		}
		throw new IllegalArgumentException("propertyId not found in container:" + propertyId);
	}

	public EntryStream<IDTYPE, BeanItemLFP<BEANTYPE>> stream() {
		return this.stream(false);
	}

	public EntryStream<IDTYPE, BeanItemLFP<BEANTYPE>> streamAll() {
		return this.stream(true);
	}

	@Override
	public void filterAll() {
		if (doFilterContainer(!getFilters().isEmpty())) {
			fireItemSetChange();
		}
	}

	public Registration addGetItemIdsListener(GetItemIdsEvent.Listener<IDTYPE, BEANTYPE> listener) {
		return this.eventRouter.addListener(GetItemIdsEvent.class, listener, GetItemIdsEvent.METHOD);
	}

	public void removeGetItemIdsListener(GetItemIdsEvent.Listener<IDTYPE, BEANTYPE> listener) {
		this.eventRouter.removeListener(GetItemIdsEvent.class, listener);
	}

	protected Map<IDTYPE, BeanItemLFP<BEANTYPE>> addInternal(EntryStream<IDTYPE, BEANTYPE> estream) {
		if (estream == null)
			return Collections.emptyMap();
		estream = estream.nonNullKeys();
		estream = estream.filterKeys(v -> !this.containsId(v));
		estream = estream.distinctKeys();
		var entryIterator = estream.iterator();
		int origSize = size();
		Map<IDTYPE, BeanItemLFP<BEANTYPE>> result = new LinkedHashMap<>();
		boolean modified = false;
		while (entryIterator.hasNext()) {
			var entry = entryIterator.next();
			var itemId = entry.getKey();
			var bean = entry.getValue();
			var beanItem = (BeanItemLFP<BEANTYPE>) internalAddItemAtEnd(itemId, createBeanItem(itemId, bean), false);
			result.put(itemId, beanItem);
			modified = true;
		}
		if (modified) {
			// Filter the contents when all items have been added
			if (isFiltered()) {
				doFilterContainer(!getFilters().isEmpty());
			}
			if (size() > origSize) {
				// fire event about added items
				int firstPosition = origSize;
				IDTYPE firstItemId = getVisibleItemIds().get(firstPosition);
				int affectedItems = size() - origSize;
				fireItemsAdded(firstPosition, firstItemId, affectedItems);
			}
		}
		return Collections.unmodifiableMap(result);
	}

	protected EntryStream<IDTYPE, BeanItemLFP<BEANTYPE>> applyFilters(
			EntryStream<IDTYPE, BeanItemLFP<BEANTYPE>> idToItemStream) {
		if (this.batchFilterFunction != null)
			this.batchFilterFunction.scrap();
		var newBatchFilterFunction = new BatchFilterFunction<Entry<IDTYPE, BeanItemLFP<BEANTYPE>>>(
				applyFiltersBatchSize, batch -> {
					var itemIds = com.lfp.joe.stream.Streams.of(batch).map(v -> v.getKey()).toImmutableList();
					this.eventRouter.fireEvent(new GetItemIdsEvent<>(this, GetItemIdsEvent.Action.FILTER, itemIds));
					return com.lfp.joe.stream.Streams.of(batch)
							.filter(ent -> this.passesFilters(ent.getKey()))
							.toList();
				});
		this.batchFilterFunction = newBatchFilterFunction;
		return newBatchFilterFunction.apply(idToItemStream).mapToEntry(Entry::getKey, Entry::getValue);
	}

	protected BeanItemLFP<BEANTYPE> createBeanItem(IDTYPE itemId, BEANTYPE bean) {
		Supplier<EntryStream<VaadinPropertyDescriptor<BEANTYPE>, Property>> entryStreamSupplier = () -> Utils.Lots
				.stream(model)
				.values()
				.mapToEntry(Entry::getKey, Entry::getValue);
		long loadIndex = Long.valueOf(this.itemIdToItem.size());
		var itemIdMetadata = ItemMetadata.builder().itemId(itemId).loadIndex(loadIndex).build();
		var beanItem = new BeanItemLFP<BEANTYPE>(itemIdMetadata,
				entryStreamSupplier.get().filterValues(v -> v == null).keys());
		entryStreamSupplier.get()
				.nonNullValues()
				.forEach(ent -> beanItem.addItemProperty(ent.getKey(), ent.getValue()));
		if (bean != null)
			beanItem.setBean(bean);
		return beanItem;
	}

	@Override
	protected boolean doFilterContainer(boolean hasFilters) {
		AtomicBoolean disabled = new AtomicBoolean();
		var event = FilterInterceptEvent.create(this, () -> {
			disabled.set(true);
		});
		SessionEventBus.getCurrent().publish(event);
		if (disabled.get())
			return false;
		if (!hasFilters) {
			boolean changed = getAllItemIds().size() != getVisibleItemIds().size();
			setFilteredItemIds(null);
			return changed;
		}

		// Reset filtered list
		List<IDTYPE> originalFilteredItemIds = getFilteredItemIds();
		boolean wasUnfiltered = false;
		if (originalFilteredItemIds == null) {
			originalFilteredItemIds = Collections.emptyList();
			wasUnfiltered = true;
		}
		setFilteredItemIds(new ListSet<IDTYPE>());

		// Filter
		boolean equal = true;
		Iterator<IDTYPE> origIt = originalFilteredItemIds.iterator();
		var idToItemStream = streamAll();
		idToItemStream = applyFilters(idToItemStream);
		var idIterator = idToItemStream.keys().iterator();
		while (idIterator.hasNext()) {
			IDTYPE id = idIterator.next();
			// filtered list comes from the full list, can use ==
			equal = equal && origIt.hasNext() && origIt.next() == id;
			getFilteredItemIds().add(id);
		}
		return (wasUnfiltered && !getAllItemIds().isEmpty()) || !equal || origIt.hasNext();
	}

	@Override
	protected BeanItemLFP<BEANTYPE> getUnfilteredItem(Object itemId) {
		return (BeanItemLFP<BEANTYPE>) itemIdToItem.get(itemId);
	}

	protected EntryStream<IDTYPE, BeanItemLFP<BEANTYPE>> stream(boolean all) {
		var estream = com.lfp.joe.stream.Streams.of(all ? this.getAllItemIds() : this.getItemIds())
				.mapToEntry(v -> v, v -> this.getItem(v));
		estream = estream.nonNullKeys().nonNullValues();
		return estream;
	}

	/**
	 * Perform the sorting of the data structures in the container. This is invoked
	 * when the <code>itemSorter</code> has been prepared for the sort operation.
	 * Typically this method calls
	 * <code>Collections.sort(aCollection, getItemSorter())</code> on all arrays
	 * (containing item ids) that need to be sorted.
	 *
	 */
	@Override
	protected void doSort() {
		var itemSorter = this.itemSorterSupplier.get();
		if (itemSorter == null)
			return;
		this.eventRouter.fireEvent(new GetItemIdsEvent<>(this, GetItemIdsEvent.Action.FILTER, getAllItemIds()));
		itemSorter.sort(this, this.sorts, getAllItemIds());
	}

	// ************ BORROWED FROM ABSTRACT BEAN CONTAINER ************************

	/**
	 * Returns the type of beans this Container can contain.
	 *
	 * This comes from the bean type constructor parameter, and bean metadata
	 * (including container properties) is based on this.
	 *
	 * @return
	 */
	public Class<? super BEANTYPE> getBeanType() {
		return type;
	}

	@Override
	public Collection<String> getContainerPropertyIds() {
		return model.keySet();
	}

	@Override
	public Property getContainerProperty(Object itemId, Object propertyId) {
		Item item = getItem(itemId);
		if (item == null) {
			return null;
		}
		return item.getItemProperty(propertyId);
	}

	@Override
	public boolean removeAllItems() {
		int origSize = size();
		IDTYPE firstItem = getFirstVisibleItem();

		internalRemoveAllItems();

		// detach listeners from all Items
		for (Item item : itemIdToItem.values()) {
			removeAllValueChangeListeners(item);
		}
		itemIdToItem.clear();

		// fire event only if the visible view changed, regardless of whether
		// filtered out items were removed or not
		if (origSize != 0) {
			fireItemsRemoved(0, firstItem, origSize);
		}

		return true;
	}

	@Override
	public List<IDTYPE> getItemIds() {
		return (List<IDTYPE>) super.getItemIds();
	}

	@Override
	public boolean removeItem(Object itemId) {
		// TODO should also remove items that are filtered out
		int origSize = size();
		Item item = getItem(itemId);
		int position = indexOfId(itemId);

		if (internalRemoveItem(itemId)) {
			// detach listeners from Item
			removeAllValueChangeListeners(item);

			// remove item
			itemIdToItem.remove(itemId);

			// fire event only if the visible view changed, regardless of
			// whether filtered out items were removed or not
			if (size() != origSize) {
				fireItemRemoved(position, itemId);
			}

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Re-filter the container when one of the monitored properties changes.
	 */
	@Override
	public void valueChange(ValueChangeEvent event) {
		// if a property that is used in a filter is changed, refresh filtering
		filterAll();
	}

	@Override
	public void addContainerFilter(Object propertyId, String filterString, boolean ignoreCase,
			boolean onlyMatchPrefix) {
		try {
			addFilter(new SimpleStringFilter(propertyId, filterString, ignoreCase, onlyMatchPrefix));
		} catch (UnsupportedFilterException e) {
			// the filter instance created here is always valid for in-memory
			// containers
		}
	}

	@Override
	public void removeAllContainerFilters() {
		if (!getFilters().isEmpty()) {
			for (Item item : itemIdToItem.values()) {
				removeAllValueChangeListeners(item);
			}
			removeAllFilters();
		}
	}

	@Override
	public void removeContainerFilters(Object propertyId) {
		Collection<Filter> removedFilters = super.removeFilters(propertyId);
		if (!removedFilters.isEmpty()) {
			// stop listening to change events for the property
			for (Item item : itemIdToItem.values()) {
				removeValueChangeListener(item, propertyId);
			}
		}
	}

	@Override
	public void addContainerFilter(Filter filter) throws UnsupportedFilterException {
		addFilter(filter);
	}

	@Override
	public void removeContainerFilter(Filter filter) {
		removeFilter(filter);
	}

	@Override
	public boolean hasContainerFilters() {
		return super.hasContainerFilters();
	}

	@Override
	public Collection<Filter> getContainerFilters() {
		return super.getContainerFilters();
	}

	/**
	 * Make this container listen to the given property provided it notifies when
	 * its value changes.
	 *
	 * @param item       The {@link Item} that contains the property
	 * @param propertyId The id of the property
	 */
	private void addValueChangeListener(Item item, Object propertyId) {
		Property<?> property = item.getItemProperty(propertyId);
		if (property instanceof ValueChangeNotifier) {
			// avoid multiple notifications for the same property if
			// multiple filters are in use
			ValueChangeNotifier notifier = (ValueChangeNotifier) property;
			notifier.removeListener(this);
			notifier.addListener(this);
		}
	}

	/**
	 * Remove this container as a listener for the given property.
	 *
	 * @param item       The {@link Item} that contains the property
	 * @param propertyId The id of the property
	 */
	private void removeValueChangeListener(Item item, Object propertyId) {
		Property<?> property = item.getItemProperty(propertyId);
		if (property instanceof ValueChangeNotifier) {
			((ValueChangeNotifier) property).removeListener(this);
		}
	}

	/**
	 * Remove this contains as a listener for all the properties in the given
	 * {@link Item}.
	 *
	 * @param item The {@link Item} that contains the properties
	 */
	private void removeAllValueChangeListeners(Item item) {
		for (Object propertyId : item.getItemPropertyIds()) {
			removeValueChangeListener(item, propertyId);
		}
	}

	@Override
	public void sort(Object[] propertyId, boolean[] ascending) {
		sortContainer(propertyId, ascending);
	}

	@Override
	protected void registerNewItem(int position, IDTYPE itemId, BeanItem<BEANTYPE> item) {
		itemIdToItem.put(itemId, item);

		// add listeners to be able to update filtering on property
		// changes
		for (Filter filter : getFilters()) {
			for (String propertyId : getContainerPropertyIds()) {
				if (filter.appliesToProperty(propertyId)) {
					// addValueChangeListener avoids adding duplicates
					addValueChangeListener(item, propertyId);
				}
			}
		}
	}

	/**
	 * @deprecated As of 7.0, replaced by {@link #addPropertySetChangeListener}
	 */
	@Deprecated
	@Override
	public void addListener(Container.PropertySetChangeListener listener) {
		addPropertySetChangeListener(listener);
	}

	@Override
	public void addPropertySetChangeListener(Container.PropertySetChangeListener listener) {
		super.addPropertySetChangeListener(listener);
	}

	/**
	 * @deprecated As of 7.0, replaced by
	 *             {@link #removePropertySetChangeListener(Container.PropertySetChangeListener)}
	 */
	@Deprecated
	@Override
	public void removeListener(Container.PropertySetChangeListener listener) {
		removePropertySetChangeListener(listener);
	}

	@Override
	public void removePropertySetChangeListener(Container.PropertySetChangeListener listener) {
		super.removePropertySetChangeListener(listener);
	}

	@Override
	public boolean removeContainerProperty(Object propertyId) throws UnsupportedOperationException {
		// Fails if the Property is not present
		if (!model.containsKey(propertyId)) {
			return false;
		}

		// Removes the Property to Property list and types
		model.remove(propertyId);

		// If remove the Property from all Items
		for (final IDTYPE id : getAllItemIds()) {
			getUnfilteredItem(id).removeItemProperty(propertyId);
		}

		// Sends a change event
		fireContainerPropertySetChange();

		return true;
	}

	public void setApplyFiltersBatchSize(int applyFiltersBatchSize) {
		this.applyFiltersBatchSize = applyFiltersBatchSize;
	}

	public void setItemSorter(Sorter<IDTYPE, BEANTYPE> itemSorter) {
		if (itemSorter == null)
			this.itemSorterSupplier = () -> null;
		else
			this.itemSorterSupplier = Utils.Functions.memoize(() -> itemSorter);
	}

}
