package com.lfp.vaadin.base.data.filter;

public interface HasFilter<F> {

	F getFilter();
}
