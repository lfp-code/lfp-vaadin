package com.lfp.vaadin.base.grid;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import com.lfp.vaadin.base.data.property.Properties;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.components.grid.HeaderRow;

@SuppressWarnings("serial")
public class Grids {

	public static HeaderRow getHeaderRow(Grid<?> grid, int index) {
		var addRows = (index + 1) - grid.getHeaderRowCount();
		for (int i = 0; i < addRows; i++)
			grid.appendHeaderRow();
		return grid.getHeaderRow(index);
	}

	public static <X> Column<X, ?> setDescription(Grid<X> grid, Object propertyId,
			Function<Optional<X>, String> descriptionGenerator) {
		return setDescription(grid, propertyId, descriptionGenerator, false);
	}

	public static <X> Column<X, ?> setDescription(Grid<X> grid, Object propertyId,
			Function<Optional<X>, String> descriptionGenerator, boolean disableHeaderApply) {
		var col = grid.getColumn(Properties.getName(propertyId));
		col.setDescriptionGenerator(descriptionGenerator == null ? null : bean -> {
			return descriptionGenerator.apply(Optional.ofNullable(bean));
		});
		for (int i = 0; !disableHeaderApply && i < grid.getHeaderRowCount(); i++) {
			var cell = grid.getHeaderRow(i).getCell(col);
			cell.setDescription(descriptionGenerator == null ? null : descriptionGenerator.apply(Optional.empty()));
		}
		return col;
	}

	public static <X, C extends Component> C setHeaderComponent(Grid<X> grid, Column<X, ?> column, C component) {
		Objects.requireNonNull(grid);
		Objects.requireNonNull(column);
		var headerRow = getHeaderRow(grid, 1);
		var headerCell = headerRow.getCell(column);
		headerCell.setComponent(component);
		if (component != null) {
			component.setWidth(100, Unit.PERCENTAGE);
			component.setHeight(100, Unit.PERCENTAGE);
		}
		return component;
	}

}
