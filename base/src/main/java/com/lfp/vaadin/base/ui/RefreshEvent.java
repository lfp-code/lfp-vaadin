package com.lfp.vaadin.base.ui;

import com.lfp.vaadin.base.component.ComponentEvent;

import com.vaadin.server.VaadinRequest;

public interface RefreshEvent extends ComponentEvent {

	@Override
	UIX getComponent();

	VaadinRequest getRequest();
}
