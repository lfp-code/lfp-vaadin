package com.lfp.vaadin.base.service;

import java.lang.reflect.Method;
import java.util.Objects;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.utils.Utils;
import com.vaadin.server.SessionDestroyListener;
import com.vaadin.server.SessionInitListener;
import com.vaadin.server.VaadinService;

@SuppressWarnings({ "deprecation" })
public class Services {

	// some fuckery to make this work in vaadin 7

	// *** INIT ***

	private static final Method VaadinService_addSessionInitListener_METHOD;
	static {
		VaadinService_addSessionInitListener_METHOD = Utils.Functions.unchecked(() -> {
			return VaadinService.class.getMethod("addSessionInitListener", SessionInitListener.class);
		});
	}

	public static Scrapable addSessionInitListener(VaadinService vaadinService, SessionInitListener listener) {
		Objects.requireNonNull(vaadinService);
		Utils.Functions.unchecked(() -> {
			VaadinService_addSessionInitListener_METHOD.invoke(vaadinService, listener);
		});
		return Scrapable.create(() -> vaadinService.removeSessionInitListener(listener));
	}

	// *** DESTROY ***

	private static final Method VaadinService_addSessionDestroyListener_METHOD;
	static {
		VaadinService_addSessionDestroyListener_METHOD = Utils.Functions.unchecked(() -> {
			return VaadinService.class.getMethod("addSessionDestroyListener", SessionDestroyListener.class);
		});
	}

	public static Scrapable addSessionDestroyListener(VaadinService vaadinService, SessionDestroyListener listener) {
		Objects.requireNonNull(vaadinService);
		Utils.Functions.unchecked(() -> {
			VaadinService_addSessionDestroyListener_METHOD.invoke(vaadinService, listener);
		});
		return Scrapable.create(() -> vaadinService.removeSessionDestroyListener(listener));
	}

}
