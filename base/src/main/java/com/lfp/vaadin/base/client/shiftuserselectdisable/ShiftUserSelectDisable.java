package com.lfp.vaadin.base.client.shiftuserselectdisable;

import java.util.function.Function;
import java.util.regex.Pattern;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.client.Connectors;
import com.lfp.vaadin.base.css.CSSs;
import com.lfp.vaadin.base.css.Style;
import com.lfp.vaadin.base.javascript.JavaScripts;

import com.vaadin.ui.Component;

public class ShiftUserSelectDisable implements Function<Component, Scrapable> {

	public static ShiftUserSelectDisable INSTANCE = new ShiftUserSelectDisable();

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private static final String SCRIPT_SELECTOR = "x" + Utils.Crypto.hashMD5(THIS_CLASS, VERSION).encodeHex();
	private static final MemoizedSupplier<Style> STYLE_S = Utils.Functions.memoize(() -> {
		var css = Utils.Resources.getResourceAsString(Configs.get(ShiftUserSelectDisableConfig.class).cssPath()).get();
		return Style.build(s -> css.replaceAll(Pattern.quote("_STYLE_NAME_"), s));
	});

	@Override
	public Scrapable apply(Component input) {
		return Connectors.addStateChangeListener(input, evt -> {
			if (evt.isAttached())
				setup(input);
			else
				tearDown(input);
		}, ops -> ops.fireIfAttached(true));
	}

	protected void setup(Component input) {
		STYLE_S.get().add(input);
		JavaScripts.execute(getScript(true));
	}

	protected void tearDown(Component input) {
		JavaScripts.execute(getScript(false));
	}

	private static String getScript(boolean enable) {
		var cfg = Configs.get(ShiftUserSelectDisableConfig.class);
		String template = Utils.Resources.getResourceAsString(cfg.scriptPath()).get();
		String js = Utils.Strings.templateApply(template, "SELECTOR", "." + SCRIPT_SELECTOR, "ENABLE", enable);
		return js;
	}

}
