package com.lfp.vaadin.base.session;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.ImmutableMemberCache.MethodRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.EmptyArrays;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.vaadin.util.CurrentInstance;
import com.vaadin.util.CurrentInstanceFallbackResolver;

@SuppressWarnings({ "unchecked", "serial" })
public class CurrentInstances {

	protected CurrentInstances() {}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final BiConsumer<Class<?>, Object> SET_CONSUMER;
	private static final Supplier<Map<Class<?>, CurrentInstance>> GET_INSTANCES_SUPPLIER;
	private static final boolean INIT_COMPLETE;
	static {
		SET_CONSUMER = Throws.unchecked(() -> {
			var method = MemberCache
					.getMethod(MethodRequest.of(CurrentInstance.class, null, "set", Class.class, Object.class));
			return Throws.biConsumerUnchecked((ct, v) -> method.invoke(null, ct, v));
		});
		GET_INSTANCES_SUPPLIER = Throws.unchecked(() -> {
			var name = "getInstances";
			var method = MemberCache.tryGetMethod(MethodRequest.of(CurrentInstance.class, null, name), false)
					.orElseGet(() -> {
						return Throws.unchecked(() -> MemberCache
								.getMethod(MethodRequest.of(CurrentInstance.class, null, name, boolean.class)));
					});
			Object[] args = method.getParameterCount() == 0 ? EmptyArrays.of(Object.class) : new Object[] { false };
			return Throws.supplierUnchecked(() -> (Map<Class<?>, CurrentInstance>) method.invoke(null, args));
		});
		var fallbackResolvers = new ConcurrentHashMap<Class<?>, CurrentInstanceFallbackResolver<?>>() {

			@Override
			public CurrentInstanceFallbackResolver<?> get(Object key) {
				var instance = Optional.ofNullable(super.get(key))
						.map(CurrentInstanceFallbackResolver::resolve)
						.orElse(null);
				if (instance != null)
					return () -> instance;
				if (!(key instanceof Class))
					return null;
				return getCurrentInstanceFallbackResolver((Class<?>) key);
			}
		};
		var fieldRequest = FieldRequest.of(CurrentInstance.class, Map.class, "fallbackResolvers");
		fallbackResolvers.putAll(MemberCache.getFieldValue(fieldRequest, null));
		MemberCache.setFieldValue(fieldRequest, null, fallbackResolvers);
		INIT_COMPLETE = true;
	}

	public static boolean init() {
		return INIT_COMPLETE;
	}

	public static <U> U get(Class<U> type) {
		return CurrentInstance.get(type);
	}

	public static <T> void set(Class<T> type, T instance) {
		SET_CONSUMER.accept(type, instance);
	}

	public static Map<Class<?>, CurrentInstance> getInstances() {
		return GET_INSTANCES_SUPPLIER.get();
	}

	public static Runnable intercept(Runnable runnable) {
		var function = intercept(runnable == null ? null : (Function<Void, Void>) nil -> {
			runnable.run();
			return null;
		});
		return () -> function.apply(null);
	};

	public static <X> Supplier<X> intercept(Supplier<X> supplier) {
		var function = intercept(supplier == null ? null : (Function<Void, X>) nil -> {
			return supplier.get();
		});
		return () -> function.apply(null);
	};

	public static <Y> Consumer<Y> intercept(Consumer<Y> consumer) {
		var function = intercept(consumer == null ? null : (Function<Y, Void>) input -> {
			consumer.accept(input);
			return null;
		});
		return input -> function.apply(input);
	}

	public static <X, Y> Function<X, Y> intercept(Function<X, Y> function) {
		Objects.requireNonNull(function);
		return new Function<X, Y>() {

			private Map<Class<?>, CurrentInstance> instances = getInstances();

			@Override
			public Y apply(X input) {
				var currentInstances = getInstances();
				try {
					restoreInstances(instances, currentInstances);
					return function.apply(input);
				} finally {
					CurrentInstance.restoreInstances(currentInstances);
				}
			}
		};
	};

	public static Executor intercept(Executor executor) {
		return new Executor() {

			private Map<Class<?>, CurrentInstance> requestInstances = getInstances();

			@Override
			public void execute(Runnable command) {
				var executeInstances = concatInstances(requestInstances, getInstances());
				executor.execute(() -> {
					var currentInstances = getInstances();
					try {
						restoreInstances(executeInstances, currentInstances);
						command.run();
					} finally {
						CurrentInstance.restoreInstances(currentInstances);
					}
				});
			}

		};
	}

	@SafeVarargs
	public static Map<Class<?>, CurrentInstance> concatInstances(Map<Class<?>, CurrentInstance>... instanceMaps) {
		return Streams.of(instanceMaps)
				.nonNull()
				.map(Map::entrySet)
				.flatMap(Collection::stream)
				.chain(EntryStreams::of)
				.nonNullValues()
				.distinctKeys()
				.toMap();
	}

	@SafeVarargs
	public static Map<Class<?>, CurrentInstance> restoreInstances(Map<Class<?>, CurrentInstance>... instanceMaps) {
		var restoreInstances = concatInstances(instanceMaps);
		CurrentInstance.restoreInstances(restoreInstances);
		return restoreInstances;
	}

	protected static CurrentInstanceFallbackResolver<?> getCurrentInstanceFallbackResolver(Class<?> type) {
		if (MachineConfig.isDeveloper())
			LOGGER.warn("failed to resolve current instance. type:{} thread:{}", type,
					Thread.currentThread().getName());
		return null;
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		set(Date.class, new Date());
		System.out.println(get(Date.class));
		Threads.Pools.lowPriorityPool().submit(() -> {
			Thread.sleep(1_000);
			System.out.println(get(Date.class));
			return null;
		}).get();
	}
}
