package com.lfp.vaadin.base.client;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.threadly.concurrent.SameThreadSubmitterExecutor;
import org.threadly.concurrent.event.RunnableListenerHelper;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.process.future.Completion;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.VaadinSession.FutureAccess;

public class FutureAccessLFP extends FutureAccess implements ListenableFuture<Void> {

	private static final Field FutureTask_runner_FIELD = Throws
			.unchecked(() -> MemberCache.getField(FieldRequest.of(FutureTask.class, Thread.class, "runner")));
	private final RunnableListenerHelper listeners = new RunnableListenerHelper(true);

	public FutureAccessLFP(VaadinSession session, Runnable runnable) {
		super(session, runnable);
	}

	@Override
	public ListenableFuture<Void> listener(Runnable listener, Executor executor,
			ListenerOptimizationStrategy optimizeExecution) {
		if (ListenerOptimizationStrategy.InvokingThreadIfDone.equals(optimizeExecution) && this.isDone())
			executor = SameThreadSubmitterExecutor.instance();
		if (executor != null)
			listeners.addListener(listener, executor);
		else
			listeners.addListener(listener);
		return this;
	}

	@Override
	public boolean isCompletedExceptionally() {
		return Completion.tryGet(this).map(Completion::isFailure).orElse(false);
	}

	@Override
	public Throwable getFailure() throws InterruptedException {
		return Completion.get(this).failure();
	}

	@Override
	public Throwable getFailure(long timeout, TimeUnit unit) throws InterruptedException, TimeoutException {
		var completion = Completion.get(this, Durations.from(timeout, unit)).orElse(null);
		if (completion == null)
			throw new TimeoutException();
		return completion.failure();
	}

	@Override
	public StackTraceElement[] getRunningStackTrace() {
		var thread = (Thread) Throws.unchecked(() -> FutureTask_runner_FIELD.get(this));
		return Optional.ofNullable(thread).map(Thread::getStackTrace).orElse(null);
	}

	@Override
	protected void done() {
		super.done();
		this.listeners.callListeners();
	}
}
