package com.lfp.vaadin.base.v7.container.sort;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;
import org.ehcache.config.units.MemoryUnit;

import com.google.common.reflect.TypeToken;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.ehcache.EhcacheOptions;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.FNV;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.v7.container.ContainerLFP;
import com.lfp.vaadin.base.v7.data.Items;
import com.lfp.vaadin.base.v7.property.PropertiesV7;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

public class SorterImpl<IDTYPE, BEANTYPE> implements Sorter<IDTYPE, BEANTYPE> {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private static final MemoizedSupplier<org.ehcache.Cache<Long, CachedSortMap>> SORT_CACHE_S = Utils.Functions
			.memoize(() -> {
				var blder = EhcacheOptions.builder(TypeToken.of(Long.class), TypeToken.of(CachedSortMap.class),
						THIS_CLASS, VERSION);
				blder.resourcePoolsBuilderConfiguration(rpb -> {
					rpb = rpb.disk(1, MemoryUnit.GB);
					return rpb;
				});
				return Caches.newEhcache(blder.build());
			});

	public SorterImpl() {}

	@Override
	public void sort(ContainerLFP<IDTYPE, BEANTYPE> container, Sorts sorts, List<IDTYPE> itemIds) {
		Objects.requireNonNull(container);
		if (sorts == null)
			return;
		if (itemIds == null || itemIds.size() < 2)
			return;
		if (MachineConfig.isDeveloper())
			logger.info("starting sort. size:{} sorts:{}", itemIds.size(), sorts.stream().toList());
		Comparator<IDTYPE> comparator = (id1, id2) -> compare(container, sorts, id1, id2);
		StopWatch totalTimer = StopWatch.createStarted();
		StopWatch cacheReadTimer = new StopWatch();
		StopWatch cacheWriteTimer = new StopWatch();
		StopWatch hashTimer = new StopWatch();
		StopWatch sortTimer = new StopWatch();
		try {
			hashTimer.start();
			var comparablesHash = hashComparables(container, sorts, itemIds);
			hashTimer.stop();
			for (int i = 0; i < 2; i++) {
				var inverse = i > 0;
				var cacheKey = getCacheKey(comparablesHash, sorts, inverse);
				cacheReadTimer.start();
				var sortMap = SORT_CACHE_S.get().get(cacheKey);
				cacheReadTimer.stop();
				if (sortMap == null) {
					cacheReadTimer.reset();
					continue;
				}
				syncOrder(itemIds, sortMap, inverse);
				return;
			}
			AtomicBoolean syncComplete = new AtomicBoolean();
			sortTimer.start();
			var itemIdsCopy = Utils.Lots.stream(itemIds).filter(itemId -> {
				for (var propertyId : sorts.stream().keys()) {
					var hasValue = streamComparables(container, itemId, propertyId).findFirst().isPresent();
					if (hasValue)
						return true;
				}
				return false;
			}).distinct().chain(Streams.parallelSort(comparator)).toList();
			sortTimer.stop();
			var sortMap = CachedSortMap.create(Utils.Lots.stream(itemIdsCopy).map(this::getCacheKey),
					itemIdsCopy.size());
			var cacheKey = getCacheKey(comparablesHash, sorts, false);
			cacheWriteTimer.start();
			SORT_CACHE_S.get().put(cacheKey, sortMap);
			cacheWriteTimer.stop();
			if (syncComplete.get())
				return;
			syncOrder(itemIds, sortMap, false);
		} finally {
			if (MachineConfig.isDeveloper()) {
				logger.info(
						"completed sort. size:{} sorts:{} sortTime:{} hashTime:{} cacheReadTime:{} cacheWriteTime:{} totalTime:{}",
						itemIds.size(), sorts.stream().toList(), sortTimer.getTime(), hashTimer.getTime(),
						cacheReadTimer.getTime(), cacheWriteTimer.getTime(), totalTimer.getTime());
			}
		}
	}

	protected void syncOrder(List<IDTYPE> itemIds, CachedSortMap sortMap, boolean inverse) {
		Function<IDTYPE, Integer> indexOf = itemId -> {
			var key = getCacheKey(itemId);
			if (key == null)
				return Integer.MAX_VALUE;
			var index = sortMap.get(key);
			if (index == null)
				return Integer.MAX_VALUE;
			if (inverse)
				index = index * -1;
			return index;
		};
		Comparator<IDTYPE> comparator = (itemId1, itemId2) -> {
			var index1 = indexOf.apply(itemId1);
			var index2 = indexOf.apply(itemId2);
			return index1.compareTo(index2);
		};
		var sortedIter = Streams.of(itemIds).chain(Streams.parallelSort(comparator)).iterator();
		for (int i = 0; i < itemIds.size(); i++)
			itemIds.set(i, sortedIter.next());
	}

	protected Long getCacheKey(IDTYPE itemId) {
		var barr = Items.hashId(itemId).array();
		return FNV.hash64(barr);
	}

	public static <IDTYPE, BEANTYPE> StreamEx<Comparable<?>> streamComparables(ContainerLFP<IDTYPE, BEANTYPE> container,
			IDTYPE itemId, Object propertyId) {
		if (itemId == null || propertyId == null)
			return StreamEx.empty();
		var value = PropertiesV7.tryGetValue(container.getItem(itemId), propertyId).orElse(null);
		return streamComparables(value);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected static StreamEx<Comparable<?>> streamComparables(Object value) {
		if (value == null)
			return StreamEx.empty();
		StreamEx<Object> objStream;
		if (value instanceof Stream)
			objStream = Utils.Lots.stream((Stream) value);
		else if (value instanceof Iterator)
			objStream = Utils.Lots.stream((Iterator) value);
		else if (value instanceof Iterable)
			objStream = Utils.Lots.stream((Iterable) value);
		else
			objStream = StreamEx.of(value);
		return objStream.map(v -> toComparable(v));
	}

	protected static Comparable<?> toComparable(Object value) {
		if (value == null)
			return null;
		if (!(value instanceof Comparable))
			value = value.toString();
		return (Comparable<?>) value;
	}

	protected int compare(ContainerLFP<IDTYPE, BEANTYPE> container, Sorts sorts, IDTYPE itemId1, IDTYPE itemId2) {
		try (var stream = sorts.stream()) {
			for (var ent : stream) {
				var propertyId = ent.getKey();
				var ascending = ent.getValue();
				var comparables1 = streamComparables(container, itemId1, propertyId);
				var comparables2 = streamComparables(container, itemId2, propertyId);
				var comparison = compare(ascending, comparables1, comparables2);
				if (comparison != 0)
					return comparison;
			}
		}
		return 0;

	}

	@SuppressWarnings("rawtypes")
	protected int compare(boolean ascending, StreamEx<Comparable<?>> comparablesStream1,
			StreamEx<Comparable<?>> comparablesStream2) {
		var comparables1 = comparablesStream1.iterator();
		var comparables2 = comparablesStream2.iterator();
		while (comparables1.hasNext() || comparables2.hasNext()) {
			var next1 = comparables1.hasNext() ? comparables1.next() : null;
			var next2 = comparables2.hasNext() ? comparables2.next() : null;
			Comparator<Comparable<?>> comparator;
			if ((next1 instanceof String) && (next2 instanceof String))
				comparator = (Comparator) StringSortComparator.INSTANCE;
			else
				comparator = Utils.Lots.comparatorNaturalOrder();
			if (!ascending)
				comparator = comparator.reversed();
			comparator = Comparator.nullsLast(comparator);
			var comparison = comparator.compare(next1, next2);
			if (comparison == 0)
				continue;
			return comparison;
		}
		return 0;
	}

	protected static long getCacheKey(Bytes comparablesHash, Sorts sorts, boolean inverse) {
		return FNV.hash64(comparablesHash.array(), sorts.getSortHash(inverse).array());
	}

	@SuppressWarnings("resource")
	protected static <IDTYPE> Bytes hashComparables(ContainerLFP<IDTYPE, ?> container, Sorts sorts,
			List<IDTYPE> itemIds) {
		AtomicInteger hashCode = new AtomicInteger();
		var pool = Threads.Pools.centralPool().limit(Utils.Machine.logicalProcessorCount());
		var completeTracker = Threads.Futures.createFutureTracker();
		for (var itemId : itemIds) {
			var future = pool.submit(() -> {
				var messageDigest = Utils.Crypto.getMessageDigestMD5();
				Utils.Crypto.update(messageDigest, Items.hashId(itemId));
				for (var propertyId : sorts.stream().keys()) {
					Utils.Crypto.update(messageDigest, propertyId);
					for (var comparable : streamComparables(container, itemId, propertyId)) {
						Utils.Crypto.update(messageDigest, comparable.hashCode());
					}
				}
				var itemHash = Bytes.from(messageDigest.digest());
				hashCode.addAndGet(itemHash.encodeHex().hashCode());
				return Nada.get();
			}).mapFailure(Throwable.class, t -> {
				if (!Utils.Exceptions.isCancelException(t))
					logger.error("error during comparable hash:{}", itemId, t);
				return Nada.get();
			});
			completeTracker.add(future);
		}
		Utils.Functions.unchecked(() -> completeTracker.blockAll(true));
		return Bytes.from(hashCode.get());
	}

}