package com.lfp.vaadin.base.ui;

import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.function.Function;

import org.threadly.concurrent.SameThreadSubmitterExecutor;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.session.CurrentInstances;
import com.vaadin.server.ClientConnector;
import com.vaadin.ui.UI;

import one.util.streamex.StreamEx;

public class UIs {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static UI getCurrent() {
		return getCurrent(UI.class, (ClientConnector[]) null);
	}

	public static UI getCurrent(ClientConnector... clientConnectors) {
		return getCurrent(UI.class, clientConnectors);
	}

	public static <X extends UI> X getCurrent(Class<X> uiClassType) {
		return getCurrent(uiClassType, (ClientConnector[]) null);
	}

	@SuppressWarnings("unchecked")
	public static <X extends UI> X getCurrent(Class<X> uiClassType, ClientConnector... clientConnectors) {
		if (uiClassType != null) {
			var ui = UI.getCurrent();
			if (ui != null && uiClassType.isInstance(ui))
				return (X) ui;
		}
		return getCurrentStream(uiClassType, clientConnectors).findFirst().orElse(null);
	}

	private static <X extends UI> StreamEx<X> getCurrentStream(Class<X> uiClassType,
			ClientConnector... clientConnectors) {
		Objects.requireNonNull(uiClassType, "ui class type required");
		Function<Iterable<? extends ClientConnector>, StreamEx<UI>> uiStreamer = ible -> {
			if (ible == null)
				return StreamEx.empty();
			var stream = com.lfp.joe.stream.Streams.of(clientConnectors).nonNull().distinct().map(v -> {
				if (v instanceof UI)
					return (UI) v;
				return v.getUI();
			});
			return stream.nonNull().distinct().filter(UI::isAttached);
		};
		var ccStream = com.lfp.joe.stream.Streams.of(clientConnectors).append(UI.getCurrent());
		var uiStream = uiStreamer.apply(ccStream);
		return uiStream.select(uiClassType);
	}

	public static Future<Void> accessOrExecute(Runnable runnable) {
		return accessOrExecute(runnable, null, (ClientConnector[]) null);
	}

	public static Future<Void> accessOrExecute(Runnable runnable, ClientConnector... clientConnectors) {
		return accessOrExecute(runnable, null, (ClientConnector[]) null);
	}

	public static Future<Void> accessOrExecute(Runnable runnable, Executor fallbackExecutor,
			ClientConnector... clientConnectors) {
		if (runnable == null)
			runnable = Utils.Functions.doNothingRunnable();
		runnable = CurrentInstances.intercept(runnable);
		var ui = UIs.getCurrent(clientConnectors);
		if (ui != null)
			return ui.access(runnable);
		else {
			logger.warn("ui access lookup failed. executing");
			var submitterExecutor = fallbackExecutor == null ? SameThreadSubmitterExecutor.instance()
					: SubmitterExecutorAdapter.adaptExecutor(fallbackExecutor);
			ListenableFuture<Void> future = submitterExecutor.submit(runnable).map(nil -> null);
			Threads.Futures.logFailureError(future, true, "access error");
			return future;
		}
	}

}
