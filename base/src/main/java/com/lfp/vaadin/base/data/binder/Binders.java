package com.lfp.vaadin.base.data.binder;

import java.util.Objects;
import java.util.function.Function;

import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;
import com.lfp.vaadin.base.component.Components;
import com.lfp.vaadin.base.data.property.Properties;

import org.joda.beans.BeanBuilder;
import org.joda.beans.MetaProperty;

import com.github.throwable.beanref.BeanPath;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue;
import com.vaadin.data.PropertyDefinition;
import com.vaadin.data.PropertySet;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Component;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class Binders {

	private static final FieldAccessor<Binder, PropertySet<?>> Binder_propertySet_FIELD_ACCESSOR = JavaCode.Reflections
			.getFieldAccessorUnchecked(Binder.class, true, v -> "propertySet".equals(v.getName()),
					v -> PropertySet.class.isAssignableFrom(v.getType()));

	public static <B extends BeanBuilder<?>, V> PropertyNameBinding<B, V> bind(Binder<B> binder, HasValue<V> hasValue,
			MetaProperty<V> metaProperty) {
		var binding = binder.bind(hasValue, b -> JodaBeans.get(b, metaProperty), (b, v) -> b.set(metaProperty, v));
		if (hasValue instanceof Component && ((Component) hasValue).getCaption() == null)
			((Component) hasValue).setCaption(Components.getCaption(metaProperty.name()));
		return PropertyNameBinding.create(binding, metaProperty.name());
	}

	public static <B, V> PropertyNameBinding<B, V> bind(Binder<B> binder, HasValue<V> hasValue,
			BeanPath<B, V> beanPath) {
		var binding = binder.bind(hasValue, beanPath::get, beanPath::set);
		if (hasValue instanceof Component && ((Component) hasValue).getCaption() == null)
			((Component) hasValue).setCaption(Components.getCaption(beanPath.getPath()));
		return PropertyNameBinding.create(binding, beanPath.getPath());
	}

	public static <B, V> PropertyNameBinding<B, V> bind(Binder<B> binder, HasValue<V> hasValue, Object propertyId) {
		var propertySet = getPropertySet(binder);
		PropertyDefinition<B, V> pd = (PropertyDefinition<B, V>) propertySet.getProperty(Properties.getName(propertyId))
				.get();
		return bind(binder, hasValue, pd);
	}

	public static <B, V, P> PropertyNameBinding<B, V> bind(Binder<B> binder, HasValue<V> hasValue, Object propertyId,
			Function<P, V> getterMapper, Function<V, P> setterMapper) {
		var propertySet = getPropertySet(binder);
		PropertyDefinition<B, P> pd = (PropertyDefinition<B, P>) propertySet.getProperty(Properties.getName(propertyId))
				.get();
		return bind(binder, hasValue, pd, getterMapper, setterMapper);
	}

	public static <B, V> PropertyNameBinding<B, V> bind(Binder<B> binder, HasValue<V> hasValue,
			PropertyDefinition<B, V> propertyDefinition) {
		return bind(binder, hasValue, propertyDefinition, v -> {
			if (hasValue instanceof AbstractTextField && v == null)
				v = (V) "";
			return v;
		}, v -> v);
	}

	public static <B, V, P> PropertyNameBinding<B, V> bind(Binder<B> binder, HasValue<V> hasValue,
			PropertyDefinition<B, P> propertyDefinition, Function<P, V> getterMapper, Function<V, P> setterMapper) {
		Objects.requireNonNull(binder);
		Objects.requireNonNull(propertyDefinition);
		Objects.requireNonNull(getterMapper);
		Objects.requireNonNull(setterMapper);
		var pdefSetter = propertyDefinition.getSetter().orElse(null);
		Setter<B, V> setter = pdefSetter == null ? null : (b, v) -> pdefSetter.accept(b, setterMapper.apply(v));
		ValueProvider<B, V> getter = b -> propertyDefinition.getGetter().andThen(getterMapper).apply(b);
		var binding = binder.forField(hasValue).bind(getter, setter);
		if (hasValue instanceof Component && ((Component) hasValue).getCaption() == null)
			((Component) hasValue).setCaption(propertyDefinition.getCaption());
		return PropertyNameBinding.create(binding, propertyDefinition.getName());
	}

	public static <B> PropertySet<B> getPropertySet(Binder<B> binder) {
		Objects.requireNonNull(binder);
		return (PropertySet<B>) Binder_propertySet_FIELD_ACCESSOR.get(binder);
	}
}
