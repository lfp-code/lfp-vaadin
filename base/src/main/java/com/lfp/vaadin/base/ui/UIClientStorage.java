package com.lfp.vaadin.base.ui;

import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.Validate;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;
import org.vaadin.olli.ClientStorage;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.vaadin.base.session.SessionExecutor;

public class UIClientStorage {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final boolean IS_ON_CLASS_PATH = JavaCode.Reflections.tryForName("org.vaadin.olli.ClientStorage")
			.isPresent();

	public static UIClientStorage getCurrent() {
		return UIX.getCurrent().getAttributes().computeIfAbsent(UIClientStorage.class, UIClientStorage::new);
	}

	private final ClientStorage clientStorage;
	private final SettableListenableFuture<Boolean> supportFuture;

	private UIClientStorage() {
		this.supportFuture = new SettableListenableFuture<Boolean>(false);
		if (!IS_ON_CLASS_PATH) {
			this.clientStorage = null;
			supportFuture.setResult(false);
		} else
			this.clientStorage = new ClientStorage(supportFuture::setResult);
	}

	public Optional<ClientStorage> getClientStorage() {
		return Optional.ofNullable(this.clientStorage);
	}

	public ListenableFuture<Boolean> isAvailable() {
		return this.supportFuture;
	}

	public ListenableFuture<String> read(String key, boolean local) {
		Validate.notBlank(key);
		ListenableFuture<ListenableFuture<String>> futureFuture = access(scsOp -> {
			if (scsOp.isEmpty())
				return FutureUtils.immediateResultFuture((String) null);
			var scs = scsOp.get();
			SettableListenableFuture<String> sfuture = new SettableListenableFuture<>(false);
			if (local)
				scs.getLocalItem(key, sfuture::setResult);
			else
				scs.getSessionItem(key, sfuture::setResult);
			if (MachineConfig.isDeveloper())
				sfuture.resultCallback(v -> {
					logger.info("client storage read. local:{} key:{} value:{}", local, key, v);
				});
			return sfuture;
		});
		var result = futureFuture.flatMap(v -> v);
		return result;
	}

	public ListenableFuture<Boolean> write(String key, String value, boolean local) {
		Validate.notBlank(key);
		var future = access(scsOp -> {
			if (scsOp.isEmpty())
				return false;
			var scs = scsOp.get();
			if (value == null) {
				if (local)
					scs.removeLocalItem(key);
				else
					scs.removeSessionItem(key);
			} else {
				if (local)
					scs.setLocalItem(key, value);
				else
					scs.setSessionItem(key, value);
			}
			if (MachineConfig.isDeveloper())
				logger.info("client storage write. local:{} key:{} value:{}", local, key, value);
			return true;
		});
		return future;
	}

	public <X> ListenableFuture<X> access(ThrowingFunction<Optional<ClientStorage>, X, ? extends Throwable> accessor) {
		Objects.requireNonNull(accessor);
		return isAvailable().flatMap(support -> {
			if (!support)
				try {
					return FutureUtils.immediateResultFuture(accessor.apply(Optional.empty()));
				} catch (Throwable t) {
					return FutureUtils.immediateFailureFuture(t);
				}
			Optional<ClientStorage> scsOp = Optional.of(this.clientStorage);
			SettableListenableFuture<X> sfuture = new SettableListenableFuture<>(false);
			UIs.getCurrent().access(() -> sfuture.setResult(accessor.onError(sfuture::setFailure).apply(scsOp)));
			return sfuture;
		}, SessionExecutor.getCurrent());

	}
}
