package com.lfp.vaadin.base.event;

import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Date;

import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.MethodPredicate;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.function.Requires;
import com.vaadin.event.EventRouter;
import com.vaadin.event.SerializableEventListener;
import com.vaadin.shared.Registration;

@SuppressWarnings({ "unchecked", "serial" })
public class EventRouterLFP extends EventRouter {

	public <E extends SourceEvent<?>> Registration addListener(Class<E> sourceEventClassType,
			SourceEventListener<? extends E> eventListener) {
		return super.addListener(sourceEventClassType, eventListener, SourceEventListener.LISTENER_METHOD);
	}

	public <E extends SourceEvent<?>> Registration addListener(Class<E> sourceEventClassType,
			SerializableEventListener listener) {
		Method method;
		if (listener == null)
			method = null;
		else {
			var methodPredicate = MethodPredicate.create().withModifierPUBLIC().withModifierNotSTATIC()
					.withParametersCount(1).withParameterTypes(sourceEventClassType);
			var methods = JavaCode.Reflections.getMethods(listener.getClass(), methodPredicate);
			if (methods.size() == 1) {
				method = methods.iterator().next();
				method.setAccessible(true);
			} else
				method = null;
		}
		Requires.notNull(method, "Listener method discovery failed:%s", listener == null ? null : listener.getClass());
		return super.addListener(sourceEventClassType, listener, method);
	}

	public static void main(String[] args) throws InterruptedException {
		var eventRouter = new EventRouterLFP();
		eventRouter.addListener(SourceEvent.class, v -> {
			System.out.println(v);
		});
		eventRouter.fireEvent(new SourceEvent<Date>(new Date()));
		eventRouter.fireEvent(new SourceEvent<String>("hi"));

		Threads.sleep(Duration.ofSeconds(5).toMillis());
		System.err.println("done");

	}

}
