package com.lfp.vaadin.base.javascript;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.lfp.joe.net.html.Htmls;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.session.SessionExecutor;
import com.lfp.vaadin.base.ui.UIs;
import com.vaadin.server.Page;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.UI;

import elemental.json.JsonValue;

public class JavaScripts {

	private static final String SELECTOR_TEMPLATE = "var arguments=document.querySelectorAll('{{{SELECTOR}}}');if(!arguments||arguments.length==0){return}{{{SCRIPT}}}";
	private static final String DISPATCH_CLICK_TEMPLATE = "dispatchEvent(new MouseEvent('click', {{{OPTIONS}}}))";

	public static JavaScript getCurrent() {
		var result = JavaScript.getCurrent();
		if (result == null)
			result = Optional.ofNullable(UI.getCurrent()).map(UI::getPage).map(Page::getJavaScript).orElse(null);
		Objects.requireNonNull(result, () -> "JavaScript lookup error. thread:" + Thread.currentThread().getName());
		return result;
	}

	public static void execute(String script) {
		getCurrent().execute(script);
	}

	public static ListenableFuture<List<JsonValue>> executeAsync(String script) {
		var js = getCurrent();
		String functionName = Htmls.generateSecureRandomKey("js");
		SettableListenableFuture<List<JsonValue>> sfuture = new SettableListenableFuture<>(false);
		js.addFunction(functionName, arguments -> {
			SessionExecutor.getCurrent().submit(() -> {
				if (arguments == null || arguments.length() == 0)
					return List.<JsonValue>of();
				List<JsonValue> list = new ArrayList<JsonValue>(arguments.length());
				for (int i = 0; i < arguments.length(); i++)
					list.add(arguments.get(i));
				list = Utils.Lots.unmodifiable(list);
				return list;
			}).resultCallback(sfuture::setResult).failureCallback(sfuture::setFailure);

		});
		sfuture.listener(() -> js.removeFunction(functionName));
		String prepend = "if ((typeof arguments === \"undefined\") || arguments == null || !Array.isArray(arguments))\n"
				+ "    arguments = [];\n" + "arguments.push(window[\"%s\"])";
		prepend = String.format(prepend, functionName);
		prepend += "\n";
		var executeScript = prepend + Optional.ofNullable(script).orElse("arguments[arguments.length -1]()");
		UIs.getCurrent().access(() -> js.execute(executeScript));
		return sfuture;
	}

	public static void selectorExecute(String selector, String script) {
		if (selector == null)
			selector = "";
		if (script == null)
			script = "";
		String js = Utils.Strings.templateApply(SELECTOR_TEMPLATE, "SELECTOR", selector, "SCRIPT", script);
		getCurrent().execute(js);
	}

	public static void click(String selector, Map<Object, Object> mouseEventOptions) {
		selectorExecute(selector,
				String.format("arguments.forEach(element => element.%s);", getDispatchClick(mouseEventOptions)));
	}

	public static void clickParent(String selector, Map<Object, Object> mouseEventOptions) {
		selectorExecute(selector, String.format("arguments.forEach(element => element.parentNode.%s);",
				getDispatchClick(mouseEventOptions)));
	}

	public static void clickChild(String selector, int childIndex, Map<Object, Object> mouseEventOptions) {
		selectorExecute(selector, String.format("arguments.forEach(element => element.children[%s].%s);", childIndex,
				getDispatchClick(mouseEventOptions)));
	}

	public static void select(String selector) {
		selectorExecute(selector, "arguments.forEach(element => element.select());");

	}

	// utils

	private static String getDispatchClick(Map<Object, Object> mouseEventOptions) {
		if (mouseEventOptions != null)
			mouseEventOptions = Collections.emptyMap();
		String json = Serials.Gsons.get().toJson(mouseEventOptions);
		String js = Utils.Strings.templateApply(DISPATCH_CLICK_TEMPLATE, "OPTIONS", json);
		return js;
	}

}
