package com.lfp.vaadin.base.session;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.encryptor4j.Encryptor;
import org.threadly.concurrent.future.ListenableFuture;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.cache.caffeine.WrapperCache;
import com.lfp.joe.threads.Threads;
import com.lfp.vaadin.base.session.SessionCacheOptions.StorageMode;
import com.lfp.vaadin.base.storage.ClientStorageWriterLoader;

import at.favre.lib.bytes.Bytes;

public class SessionCache<K, V> implements WrapperCache<K, K, ListenableFuture<V>, ListenableFuture<StatValue<V>>> {

	private final LoadingCache<K, ListenableFuture<StatValue<V>>> delegte;

	public SessionCache(SessionCacheOptions<K> sessionCacheOptions, Function<K, ListenableFuture<V>> loader) {
		this.delegte = createDelegate(sessionCacheOptions, loader);
	}

	@Override
	public K wrapKey(K key) {
		return key;
	}

	@Override
	public K unwrapKey(K wrappedKey) {
		return wrappedKey;
	}

	@Override
	public ListenableFuture<StatValue<V>> wrapValue(ListenableFuture<V> value) {
		return value.map(v -> StatValue.build(v));
	}

	@Override
	public ListenableFuture<V> unwrapValue(ListenableFuture<StatValue<V>> wrappedValue) {
		return wrappedValue.map(sv -> {
			return Optional.ofNullable(sv).map(StatValue::getValue).orElse(null);
		});
	}

	@Override
	public Cache<K, ListenableFuture<StatValue<V>>> getDelegate() {
		return delegte;
	}

	private static <K, V> LoadingCache<K, ListenableFuture<StatValue<V>>> createDelegate(
			SessionCacheOptions<K> sessionCacheOptions, Function<K, ListenableFuture<V>> loader) {
		Objects.requireNonNull(sessionCacheOptions);
		Objects.requireNonNull(loader);
		Function<K, ListenableFuture<StatValue<V>>> loaderWrap = k -> {
			var f = loader.apply(k);
			if (f == null)
				return null;
			return f.map(v -> v == null ? null : StatValue.build(v));
		};

		var cb = Caches.newCaffeineBuilder(sessionCacheOptions.getMemoryMaximumSize(),
				sessionCacheOptions.getMemoryExpireAfterWrite(), sessionCacheOptions.getMemoryExpireAfterAccess());
		if (List.of(StorageMode.LOCAL, StorageMode.SESSION).contains(sessionCacheOptions.getStorageMode())) {
			var clientStorageWriterLoader = new ClientStorageWriterLoader<K, V>(
					StorageMode.LOCAL == sessionCacheOptions.getStorageMode(),
					sessionCacheOptions.getClientStorageTimeToLive()) {

				@Override
				protected String serializeKey(@NonNull K key) {
					return sessionCacheOptions.getClientStorageKeySerializer().apply(key);
				}

				@Override
				protected @Nullable Function<Bytes, Bytes> getSignatureFunction(@NonNull K key) {
					return sessionCacheOptions.getClientStorageSignatureFunction();
				}

				@Override
				protected @Nullable Encryptor getEncryptor(@NonNull K key) {
					return sessionCacheOptions.getClientStorageEncryptor();
				}

				@Override
				protected @Nullable ListenableFuture<StatValue<V>> loadFresh(@NonNull K key) throws Exception {
					var future = loaderWrap.apply(key);
					if (future == null)
						return future;
					Threads.Futures.logFailureError(future, true, "load error. key:{}", key);
					return future;
				}

			};
			return cb.writer(clientStorageWriterLoader).build(clientStorageWriterLoader);
		} else
			return cb.build(loaderWrap::apply);
	}

}
