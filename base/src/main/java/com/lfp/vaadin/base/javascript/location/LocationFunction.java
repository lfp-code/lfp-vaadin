package com.lfp.vaadin.base.javascript.location;

import java.util.Optional;

import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.location.Location;
import com.lfp.joe.location.search.bing.BingMapsLocationSearchService;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.vaadin.base.javascript.JavascriptFunctionAsync;
import com.lfp.vaadin.base.javascript.geolocation.GeolocationFunction;
import com.lfp.vaadin.base.javascript.ip.IPAddressFunction;
import com.lfp.vaadin.base.session.SessionExecutor;
import com.lfp.vaadin.base.ui.UIX;
import com.vaadin.server.ClientConnector;

public class LocationFunction implements JavascriptFunctionAsync<Location> {

	public static final LocationFunction INSTANCE = new LocationFunction(Optional.empty());

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final ThrowingFunction<Location, Location, Exception> locationModifier;

	public LocationFunction(ThrowingFunction<Location, Location, Exception> locationModifier) {
		this(Optional.of(locationModifier));
	}

	private LocationFunction(Optional<ThrowingFunction<Location, Location, Exception>> locationModifierOp) {
		this.locationModifier = locationModifierOp.orElse(location -> {
			if (location == null)
				return location;
			boolean removeNumberAndStreet = false;
			String updateQuery;
			if (Utils.Strings.isNotBlank(location.getPostalCode()) && !location.hasLatLong())
				updateQuery = location.getText();
			else if (Utils.Strings.isBlank(location.getPostalCode()) && location.hasLatLong()) {
				removeNumberAndStreet = true;
				updateQuery = String.format("%s,%s", location.getLatitude(), location.getLongitude());
			} else
				updateQuery = null;
			if (Utils.Strings.isNotBlank(updateQuery)) {
				location = BingMapsLocationSearchService.getDefault().searchLocations(updateQuery, 1).findFirst()
						.orElse(location);
				if (removeNumberAndStreet)
					location = location.toBuilder().number(null).street(null).build();
			}
			return location;
		});
	}

	@Override
	public ListenableFuture<Location> apply(ClientConnector clientConnector) {
		var future = geolocationFunctionApply(clientConnector);
		future = future.flatMap(l -> normalizeLocation(clientConnector, l));
		future = future.flatMap(l -> {
			if (isValid(clientConnector, l))
				return FutureUtils.immediateResultFuture(l);
			return ipAddressFunctionApply(clientConnector).flatMap(ll -> normalizeLocation(clientConnector, ll));
		});
		future = future.map(l -> isValid(clientConnector, l) ? l : null);
		return future;
	}

	private ListenableFuture<Location> geolocationFunctionApply(ClientConnector clientConnector) {
		return GeolocationFunction.INSTANCE.apply(clientConnector).map(geoCoord -> geoCoord.toLocation().orElse(null));
	}

	private ListenableFuture<Location> ipAddressFunctionApply(ClientConnector clientConnector) {
		return IPAddressFunction.INSTANCE.apply(clientConnector).flatMap(ipAddress -> {
			if (!IPs.isValidIpAddress(ipAddress))
				return FutureUtils.immediateResultFuture(null);
			return SessionExecutor.getCurrent().submit(() -> toLocation(ipAddress));
		});
	}

	private boolean isValid(ClientConnector clientConnector, Location location) {
		if (location == null || !location.hasLatLong())
			return false;
		return true;
	}

	private ListenableFuture<Location> normalizeLocation(ClientConnector clientConnector, Location location) {
		if (location == null || locationModifier == null)
			return FutureUtils.immediateResultFuture(location);
		var future = SessionExecutor.getCurrent().submit(() -> locationModifier.apply(location));
		Threads.Futures.logFailureError(future, true, "could not modify:{}", location);
		return future.mapFailure(Throwable.class, t -> null);
	}

	private static Location toLocation(String ipAddress) {
		var ipInfo = IPs.getIPAddressInfo(ipAddress).orElse(null);
		if (ipInfo == null)
			return null;
		var result = ipInfo.getLocation();
		return result;
	}

}
