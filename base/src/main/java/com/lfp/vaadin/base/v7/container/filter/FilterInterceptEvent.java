package com.lfp.vaadin.base.v7.container.filter;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import com.lfp.vaadin.base.v7.container.ContainerEvent;
import com.lfp.vaadin.base.v7.container.ContainerLFP;
import com.vaadin.v7.data.Container.Filter;

@SuppressWarnings({ "deprecation" })
public interface FilterInterceptEvent<IDTYPE, B> extends ContainerEvent<IDTYPE, B> {

	public static <IDTYPE, B> FilterInterceptEvent<IDTYPE, B> create(ContainerLFP<IDTYPE, B> container,
			Runnable cancelCallback) {
		Objects.requireNonNull(container);
		Objects.requireNonNull(cancelCallback);
		return new FilterInterceptEvent<IDTYPE, B>() {

			@Override
			public Collection<Filter> getFilters() {
				var filters = container.getContainerFilters();
				if (filters == null)
					return Collections.emptyList();
				return filters;
			}

			@Override
			public void cancelFilter() {
				cancelCallback.run();
			}

			@Override
			public ContainerLFP<IDTYPE, B> getContainer() {
				return container;
			}
		};
	}

	Collection<Filter> getFilters();

	void cancelFilter();

}
