package com.lfp.vaadin.base.v7.column;

import java.lang.reflect.Method;
import java.util.Objects;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;
import com.vaadin.v7.ui.Grid.Column;

@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
public class Columns {

	private static MemoizedSupplier<Method> Column_getModelType_METHOD_SUPPLIER = Utils.Functions.memoize(() -> {
		var stream = JavaCode.Reflections.streamMethods(Column.class, true, v -> "getModelType".equals(v.getName()),
				v -> Class.class.isAssignableFrom(v.getReturnType()), v -> v.getParameterCount() == 0);
		stream = stream.limit(2);
		var list = stream.toList();
		Validate.isTrue(list.size() == 1, "unable to find getModelType method");
		var result = list.get(0);
		result.setAccessible(true);
		return result;
	});

	public static <PRESENTATION> Class<PRESENTATION> getPresentationType(Column column) {
		Objects.requireNonNull(column);
		var converter = column.getConverter();
		Class classType = converter == null ? null : converter.getPresentationType();
		if (classType == null) {
			var renderer = column.getRenderer();
			classType = renderer == null ? null : renderer.getPresentationType();
		}
		return classType;
	}

	public static <MODEL> Class<MODEL> getModelType(Column column) {
		Objects.requireNonNull(column);
		var converter = column.getConverter();
		Class classType = converter == null ? null : converter.getModelType();
		if (classType == null) {
			var method = Column_getModelType_METHOD_SUPPLIER.get();
			classType = (Class) Utils.Functions.unchecked(() -> method.invoke(column));
		}
		return classType;
	}
}
