package com.lfp.vaadin.base.javascript.ip;

import java.util.ArrayList;
import java.util.List;

import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.javascript.JavascriptFunctionAsync;
import com.lfp.vaadin.base.session.SessionRequest;
import com.vaadin.server.ClientConnector;
import com.vaadin.server.VaadinRequest;

import elemental.json.JsonValue;

public class IPAddressFunction extends JavascriptFunctionAsync.Abs<String> {

	public static final IPAddressFunction INSTANCE = new IPAddressFunction();

	IPAddressFunction() {
	}

	@Override
	protected String getScript() {
		var script = Utils.Resources.getResourceAsString("com/lfp/vaadin/base/client/generic/clientIPAddress.js")
				.get();
		return script;
	}

	@Override
	protected String parseResult(ClientConnector clientConnector, List<JsonValue> arguments) {
		var stream = com.lfp.joe.stream.Streams.of(arguments).map(v -> {
			var je = Utils.Functions.catching(() -> Serials.Gsons.getJsonParser().parse(v.toJson()), t -> null);
			if (je == null)
				return null;
			for (int i = 0; i < 2; i++) {
				if (i > 0) {
					if (!je.isJsonObject())
						break;
					var jo = je.getAsJsonObject();
					if (jo.size() != 1)
						break;
					je = jo.get(jo.keySet().iterator().next());
				}
				var result = Serials.Gsons.tryGetAsString(je, "ip").orElse(null);
				if (IPs.isValidIpAddress(result))
					return result;
			}
			return null;
		});
		stream = stream.append(Utils.Lots.defer(() -> {
			List<String> append = new ArrayList<>();
			SessionRequest.getCurrent().getHeaderMap().stream(Headers.X_FORWARDED_FOR).values().forEach(append::add);
			var request = VaadinRequest.getCurrent();
			if (request != null)
				append.add(request.getRemoteAddr());
			return append;
		}));
		stream = stream.filter(IPs::isValidIpAddress);
		stream = stream.filter(v -> !Utils.Strings.startsWith(v, "0.0.0.0"));
		var result = stream.findFirst().orElse(null);
		return result;
	}

}
