package com.lfp.vaadin.base.v7.property;

import java.util.Objects;

import com.vaadin.v7.data.util.AbstractProperty;

@SuppressWarnings({ "deprecation", "unchecked", "serial" })
public abstract class AbstractPropertyLFP<P> extends AbstractProperty<P> {

	public void fireValueChange() {
		super.fireValueChange();
	}

	@Override
	public void setValue(Object newValue) throws ReadOnlyException {
		P setValue = (P) newValue;
		P currentValue = this.getValue();
		setValueInternal(setValue);
		if (!equals(currentValue, setValue))
			this.fireValueChange();
	}

	protected boolean equals(P currentValue, P newValue) {
		return Objects.equals(currentValue, newValue);
	}

	protected abstract void setValueInternal(P newValue) throws ReadOnlyException;

}
