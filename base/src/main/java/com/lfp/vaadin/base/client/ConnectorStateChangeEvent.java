package com.lfp.vaadin.base.client;

import com.vaadin.event.ConnectorEvent;
import com.vaadin.server.ClientConnector;

public class ConnectorStateChangeEvent extends ConnectorEvent {

	public static final String ATTACH_STATE_CHANGE_EVENT_IDENTIFIER = "clientConnectorStateChangeEvent";
	private final boolean attached;
	private final boolean refreshTrigger;

	public ConnectorStateChangeEvent(ClientConnector source, boolean attached, boolean refreshTrigger) {
		super(source);
		this.attached = attached;
		this.refreshTrigger = refreshTrigger;
	}

	@Override
	public ClientConnector getSource() {
		return (ClientConnector) super.getSource();
	}

	public boolean isAttached() {
		return this.attached;
	}

	public boolean isRefreshTrigger() {
		return refreshTrigger;
	}

}
