package com.lfp.vaadin.base.component;

import com.vaadin.ui.Component;

public interface ComponentEvent {

	Component getComponent();
}
