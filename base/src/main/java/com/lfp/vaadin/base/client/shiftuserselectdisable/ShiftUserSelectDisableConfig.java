package com.lfp.vaadin.base.client.shiftuserselectdisable;

import org.aeonbits.owner.Config;

public interface ShiftUserSelectDisableConfig extends Config {

	@DefaultValue("com/lfp/vaadin/base/client/shitfuserselectdisable/style.css")
	String cssPath();

	@DefaultValue("com/lfp/vaadin/base/client/shitfuserselectdisable/script.js")
	String scriptPath();

}
