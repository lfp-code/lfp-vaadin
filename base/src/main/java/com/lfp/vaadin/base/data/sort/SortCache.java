package com.lfp.vaadin.base.data.sort;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.StampedLock;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.immutables.value.Value;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.ListenableFutureTask;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.EmptyFunctions;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.net.html.Htmls;
import com.lfp.joe.stream.ForkJoinPoolContext;
import com.lfp.joe.stream.ParallelSequentialFunction;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.data.sort.ImmutableSortCache.SortCacheOptions;
import com.lfp.vaadin.base.data.sort.ImmutableSortCache.SortCacheValue;
import com.lfp.vaadin.base.session.CurrentInstances;

import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

@ValueLFP.Style
@Value.Enclosing
public class SortCache<I, T> implements BiFunction<Iterable<T>, Boolean, StreamEx<T>> {

	private final BlockingQueue<UpdateFuture> updateFutureQueue = new LinkedBlockingQueue<>();
	private ForkJoinPool updatePool;
	private final StampedLock indexMapLock = new StampedLock();
	private Map<I, Integer> indexMap;

	private final SortCacheOptions<I, T> options;

	public SortCache(SortCacheOptions<I, T> options) {
		super();
		this.options = Objects.requireNonNull(options);
	}

	@Override
	public StreamEx<T> apply(Iterable<T> items, Boolean reverse) {
		Function<T, Integer> itemToIndexFunction = item -> {
			var id = this.options.idFunction().apply(item);
			var index = Optional.ofNullable(indexMap).map(v -> v.get(id)).orElse(null);
			if (index != null && Boolean.TRUE.equals(reverse))
				index = index * -1;
			return index;
		};
		itemToIndexFunction = CurrentInstances.intercept(itemToIndexFunction);
		var stamp = indexMapLock.readLock();
		var itemToIndexStream = Streams.of(items).nonNull().<Integer>mapToEntry(itemToIndexFunction);
		itemToIndexStream = itemToIndexStream
				.sorted(Comparator.comparing(Entry::getValue, Comparator.nullsLast(Integer::compareTo)));
		var parallelSequentialFunction = ParallelSequentialFunction.<Entry<T, Integer>>builder()
				.forkJoinPoolConextFunction(nil -> ForkJoinPoolContext.create())
				.whenComplete((v, t) -> {
					indexMapLock.unlock(stamp);
				})
				.build();
		return parallelSequentialFunction.apply(itemToIndexStream).map(Entry::getKey);
	}

	public ListenableFuture<Nada> update(Iterable<T> items) {
		Runnable updateTask = () -> runUpdate(items);
		updateTask = CurrentInstances.intercept(updateTask);
		UpdateFuture updateFuture = new UpdateFuture(updateTask);
		updateFutureQueue.add(updateFuture);
		synchronized (updateFutureQueue) {
			if (!updateFutureQueue.isEmpty() && updatePool == null)
				updatePool = update();
		}
		return updateFuture;
	}

	protected ForkJoinPool update() {
		var fjp = CoreTasks.forkJoinPool();
		fjp.submit(() -> {
			while (true) {
				var updateFuture = nextUpdateFuture();
				if (updateFuture == null) {
					synchronized (updateFutureQueue) {
						updateFuture = nextUpdateFuture();
						if (updateFuture == null) {
							fjp.shutdown();
							this.updatePool = null;
							return;
						}
					}
				}
				try {
					updateFuture.run();
				} finally {
					// jic interrupted
					updateFuture.complete();
				}
			}
		});
		return fjp;
	}

	@SuppressWarnings("unchecked")
	protected void runUpdate(Iterable<T> items) {
		var itemToIdFunction = CurrentInstances.intercept(this.options.idFunction());
		var idItemToValueFunction = CurrentInstances.<Entry<I, T>, SortCacheValue>intercept(ent -> {
			var id = ent.getKey();
			var item = ent.getValue();
			var value = this.options.valueFunction().apply(id, item);
			if (value == null)
				return null;
			var sortCacheValue = SortCacheValue.of(value);
			if (sortCacheValue.isEmpty())
				return null;
			return sortCacheValue;
		});
		var idValueStream = Streams.of(items)
				.parallel()
				.nonNull()
				.mapToEntry(itemToIdFunction)
				.invert()
				.nonNullKeys()
				.distinctKeys()
				.mapToValue((id, item) -> idItemToValueFunction.apply(Map.entry(id, item)))
				.nonNullValues();
		idValueStream = idValueStream.sorted((ent1, ent2) -> {
			return compare(ent1.getValue(), ent2.getValue());
		});
		var idArr = idValueStream.keys().toArray();
		var indexCounter = new AtomicInteger();
		var indexMap = Stream.of(idArr)
				.map(v -> (I) v)
				.collect(Collectors.toMap(Function.identity(), nil -> indexCounter.incrementAndGet()));
		var stamp = indexMapLock.writeLock();
		try {
			this.indexMap = indexMap;
		} finally {
			indexMapLock.unlock(stamp);
		}
	}

	protected UpdateFuture nextUpdateFuture() {
		UpdateFuture updateFuture = null;
		while (true) {
			var pollUpdateFuture = updateFutureQueue.poll();
			if (pollUpdateFuture == null)
				break;
			if (updateFuture != null)
				pollUpdateFuture.listener(updateFuture::complete);
			updateFuture = pollUpdateFuture;
		}
		return updateFuture;
	}

	@SuppressWarnings("unchecked")
	protected static int compare(SortCacheValue value1, SortCacheValue value2) {
		if (value1.value() == value2.value())
			return 0;
		Comparator<SortCacheValue> comparator = EmptyFunctions.comparator();
		// big decimal
		if (shouldCompareBigDecimal(value1, value2))
			comparator = comparator.thenComparing(v -> v.tryGetBigDecimal().orElse(null),
					Comparator.nullsLast(BigDecimal::compareTo));
		// non charsequence compare
		if (shouldCompareComparable(value1, value2))
			comparator = comparator.thenComparing(v -> (Comparable<Object>) v.tryGetComparable().orElse(null),
					Comparable::compareTo);
		comparator = comparator.thenComparing(v -> v.tryGetStrippedString().orElse(null),
				Comparator.nullsLast(Utils.Strings::compareIgnoreCase));
		comparator = comparator.thenComparing(v -> v.tryGetString().orElse(null),
				Comparator.nullsLast(Utils.Strings::compareIgnoreCase));
		comparator = comparator.thenComparing(v -> v.tryGetString().orElse(null),
				Comparator.nullsLast(String::compareTo));
		comparator = comparator.thenComparing(Object::hashCode);
		var result = Comparator.nullsLast(comparator).compare(value1, value2);
		return result < 0 ? -1 : result > 0 ? 1 : 0;
	}

	private static boolean shouldCompareBigDecimal(SortCacheValue value1, SortCacheValue value2) {
		return value1.tryGetBigDecimal().isPresent() || value2.tryGetBigDecimal().isPresent();
	}

	private static boolean shouldCompareComparable(SortCacheValue value1, SortCacheValue value2) {
		var c1 = value1.tryGetComparable().orElse(null);
		if (c1 == null)
			return false;
		var c2 = value2.tryGetComparable().orElse(null);
		if (c2 == null)
			return false;
		return CoreReflections.getNamedClassType(c1.getClass())
				.equals(CoreReflections.getNamedClassType(c2.getClass()));
	}

	@Value.Immutable
	abstract static class AbstractSortCacheOptions<I, T> {

		public abstract Function<? super T, ? extends I> idFunction();

		public abstract BiFunction<? super I, ? super T, ? extends Object> valueFunction();
	}

	@Value.Immutable
	abstract static class AbstractSortCacheValue {

		private static final List<Method> OPTIONAL_METHODS = CoreReflections
				.streamMethods(AbstractSortCacheValue.class, true)
				.filter(v -> Modifier.isPublic(v.getModifiers()) && !Modifier.isStatic(v.getModifiers()))
				.filter(v -> Optional.class.isAssignableFrom(v.getReturnType()))
				.collect(Collectors.toList());

		@Value.Parameter
		public abstract Object value();

		public boolean isEmpty() {
			boolean empty = true;
			for (var method : OPTIONAL_METHODS) {
				var op = (Optional<?>) Throws.unchecked(() -> method.invoke(this));
				if (op.isPresent())
					empty = false;
			}
			return empty;
		}

		public Optional<Comparable<?>> tryGetComparable() {
			var value = value();
			if (CoreReflections.isInstance(Comparable.class, value)
					&& !CoreReflections.isInstance(CharSequence.class, value))
				return Optional.of((Comparable<?>) value);
			return Optional.empty();
		}

		@Value.Derived
		public Optional<String> tryGetString() {
			var str = value().toString();
			str = Htmls.stripHtml(str);
			return Utils.Strings.trimToNullOptional(str);
		}

		@Value.Derived
		public Optional<String> tryGetStrippedString() {
			var str = tryGetString().orElse(null);
			if (str == null)
				return Optional.empty();
			var chars = IntStreamEx.of(str.chars()).dropWhile(v -> {
				return !(Character.isLetter(v) || Character.isDigit(v));
			});
			chars = chars.filter(v -> {
				return Character.isWhitespace(v) || Character.isLetter(v) || Character.isDigit(v);
			});
			str = chars.mapToObj(c -> (char) c).joining();
			return Optional.of(str).filter(Predicate.not(String::isEmpty));
		}

		@Value.Derived
		public Optional<BigDecimal> tryGetBigDecimal() {
			var number = CoreReflections.tryCast(value(), Number.class).orElse(null);
			if (number == null)
				number = tryGetStrippedString().flatMap(Utils.Strings::parseNumber).orElse(null);
			if (number == null)
				return Optional.empty();
			if (CoreReflections.isInstance(Long.class, number))
				return Optional.of(BigDecimal.valueOf(number.longValue()));
			return Optional.of(BigDecimal.valueOf(number.doubleValue()));
		}

		@Override
		public int hashCode() {
			return value().hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			return value().equals(obj);
		}

	}

	private static class UpdateFuture extends ListenableFutureTask<Nada> {

		public UpdateFuture(Runnable task) {
			super(task == null ? null : () -> {
				task.run();
				return Nada.get();
			});
		}

		public boolean complete() {
			return super.completeWithResult(Nada.get());
		}

	}

	public static void main(String[] args) throws InterruptedException {
		var cfuture = CompletableFuture.runAsync(() -> {
			System.out.println("running");
		}, CoreTasks.delayedExecutor(Duration.ofSeconds(1)));
		IntStreamEx.range(10).forEach(i -> {
			cfuture.whenComplete((v, t) -> {
				System.out.println("done - " + i);
			});
		});
		Thread.sleep(5_000);
	}

}
