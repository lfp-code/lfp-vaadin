package com.lfp.vaadin.base.ui;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.attributes.Attributes;
import com.lfp.vaadin.base.client.Connectors;
import com.lfp.vaadin.base.client.FutureAccessLFP;
import com.lfp.vaadin.base.service.Services;
import com.lfp.vaadin.base.session.CurrentInstances;
import com.lfp.vaadin.base.session.SessionAttributes;
import com.lfp.vaadin.base.session.SessionRequest;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.ErrorHandlingRunnable;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession.FutureAccess;
import com.vaadin.server.WebBrowser;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.UI;
import com.vaadin.ui.UIDetachedException;

import nl.basjes.parse.useragent.UserAgent;

@SuppressWarnings("serial")
public abstract class UIX extends UI {

	public static UIX getCurrent() {
		return UIs.getCurrent(UIX.class);
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Duration SERVER_TO_CLIENT_POLL_INTERVAL = Duration.ofSeconds(5);
	static {
		// ensure init
		CurrentInstances.init();
		Services.addSessionDestroyListener(VaadinService.getCurrent(), evt -> {
			var sessionAttributes = SessionAttributes.get(evt.getSession(), false);
			if (sessionAttributes != null)
				sessionAttributes.close();
		});
		Services.addSessionInitListener(VaadinService.getCurrent(), evt -> {
			evt.getSession().addRequestHandler((session, request, response) -> {
				var ui = UI.getCurrent();
				if (ui != null)
					UIX.getCurrent().requestUpdate(request);
				return false;
			});
		});

	}
	private final Attributes attributes;

	public UIX() {
		{ // ping client for keep alive
			Runnable onAttach = () -> {
				if (MachineConfig.isDeveloper())
					logger.info("starting client ping poller");
				var pingCounter = new AtomicLong();
				var pingPollFuture = FutureUtils.executeWhile(() -> {
					var script = String.format("console.log(\"server -> client ping %s\");",
							pingCounter.incrementAndGet());
					Callable<ListenableFuture<Nada>> pingPollLoader = () -> {
						var sfuture = new SettableListenableFuture<Nada>(false);
						UIX.this.access(() -> {
							try {
								JavaScript.eval(script);
							} catch (Throwable t) {
								if (!Throws.isHalt(t))
									logger.warn("serve -> client ping error", t);
							} finally {
								sfuture.setResult(Nada.get());
							}
						});
						return sfuture;
					};
					return Threads.Pools.centralPool()
							.submitScheduled(pingPollLoader, SERVER_TO_CLIENT_POLL_INTERVAL.toMillis())
							.flatMap(Function.identity());
				}, nil -> this.isAttached());
				var registration = Connectors.addDetachListener(this, evt -> pingPollFuture.cancel(true));
				pingPollFuture.listener(registration::scrap);
			};
			Connectors.addAttachListener(this, evt -> onAttach.run());
			if (this.isAttached())
				onAttach.run();
		}
		{ // configure attributes
			this.attributes = Attributes.create();
			Connectors.addDetachListener(this, evt -> this.attributes.close());
		}
	}

	public Attributes getAttributes() {
		return attributes;
	}

	@Override
	protected void refresh(VaadinRequest request) {
		requestUpdate(request);
		super.refresh(request);
		UIEventBus.getCurrent().publish(new RefreshEvent() {

			@Override
			public UIX getComponent() {
				return UIX.this;
			}

			@Override
			public VaadinRequest getRequest() {
				return request;
			}
		});
	}

	@Override
	protected final void init(VaadinRequest request) {
		requestUpdate(request);
		var clientStorageOp = UIClientStorage.getCurrent().getClientStorage();
		if (clientStorageOp.isPresent())
			this.addExtension(clientStorageOp.get());
		this.initx(request);

	}

	@Override
	public Future<Void> access(Runnable runnable) {
		var session = this.getSession();
		if (session == null)
			return super.access(runnable);
		var service = session.getService();
		if (service == null)
			return super.access(runnable);
		var errorHandlingRunnable = new ErrorHandlingRunnable() {
			@Override
			public void run() {
				accessSynchronously(runnable);
			}

			@Override
			public void handleError(Exception exception) {
				try {
					exception = ErrorHandlingRunnable.processException(runnable, exception);
					if (exception instanceof UIDetachedException) {
						logger.warn(
								"access() task ignored " + "because UI got detached after the task was "
										+ "enqueued. To suppress this message, change "
										+ "the task to implement {0} and make it handle " + "{1}. Affected task: {2}",
								ErrorHandlingRunnable.class.getName(), UIDetachedException.class.getName(), runnable);
					} else if (exception != null) {
						ConnectorErrorEvent errorEvent = new ConnectorErrorEvent(UIX.this, exception);
						ErrorHandler errorHandler = com.vaadin.server.ErrorEvent.findErrorHandler(UIX.this);
						if (errorHandler == null && getSession() == null)
							errorHandler = com.vaadin.server.ErrorEvent.findErrorHandler(session);
						if (errorHandler == null)
							errorHandler = new DefaultErrorHandler();
						errorHandler.error(errorEvent);
					}
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		};
		FutureAccess future = new FutureAccessLFP(session, errorHandlingRunnable);
		session.getPendingAccessQueue().add(future);
		service.ensureAccessQueuePurged(session);
		return future;
	}

	protected void requestUpdate(VaadinRequest request) {
		SessionRequest.getCurrent().update(request);
	}

	public Optional<UserAgent> getUserAgent() {
		String userAgent = SessionRequest.getCurrent().getHeaderMap().firstValue(Headers.USER_AGENT, true).orElse(null);
		if (Utils.Strings.isBlank(userAgent))
			return Optional.empty();
		var toParse = userAgent;
		var parsed = Utils.Functions.catching(() -> UserAgents.getAnalyzer().parse(toParse), t -> {
			logger.warn("unable to parse user agent:{}", toParse, t);
			return null;
		});
		return Optional.ofNullable(parsed);
	}

	public Optional<WebBrowser> getWebBrowser() {
		return Optional.ofNullable(this.getPage()).map(Page::getWebBrowser);
	}

	public Optional<ZoneId> getZoneId() {
		return getWebBrowser().map(v -> {
			var timeZoneId = v.getTimeZoneId();
			var zoneId = Utils.Strings.isBlank(timeZoneId) ? null
					: Utils.Functions.catching(() -> ZoneId.of(timeZoneId), t -> null);
			if (zoneId != null)
				return zoneId;
			var timezoneOffset = v.getTimezoneOffset();
			if (timezoneOffset == 0)
				return null;
			return ZoneOffset.ofTotalSeconds((int) Duration.ofMillis(timezoneOffset).toSeconds());
		});
	}

	protected abstract void initx(VaadinRequest request);

}
