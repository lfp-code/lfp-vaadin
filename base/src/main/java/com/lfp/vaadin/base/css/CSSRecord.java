package com.lfp.vaadin.base.css;

import java.util.Objects;
import java.util.Optional;

import com.lfp.joe.utils.Utils;

public class CSSRecord {

	public static CSSRecord create(String value) {
		value = Utils.Strings.trimToNull(value);
		Objects.requireNonNull(value, "non-blank value required");
		return new CSSRecord(value);
	}

	public static CSSRecord empty() {
		return new CSSRecord(null);
	}

	private String value;
	private boolean writeRequired = true;

	private CSSRecord(String value) {
		this.value = value;
	}

	public Optional<String> getValue() {
		return Optional.ofNullable(value);
	}

	public boolean isWriteRequired() {
		if (getValue().isEmpty())
			return true;
		return writeRequired;
	}

	public void writeComplete() {
		this.writeRequired = false;
	}
}
