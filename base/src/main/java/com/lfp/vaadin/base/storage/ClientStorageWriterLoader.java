package com.lfp.vaadin.base.storage;

import java.time.Duration;
import java.util.Objects;
import java.util.function.Function;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.encryptor4j.Encryptor;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.cache.StatValue;
import com.lfp.joe.cache.caffeine.writerloader.CacheWriterLoader;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Base58;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.vaadin.base.ui.UIClientStorage;

import at.favre.lib.bytes.Bytes;

public abstract class ClientStorageWriterLoader<K, V> extends CacheWriterLoader<K, ListenableFuture<StatValue<V>>> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String DELIMITER = "_";
	private final boolean local;
	private final Duration timeToLive;

	public ClientStorageWriterLoader(boolean local, Duration timeToLive) {
		super();
		this.local = local;
		this.timeToLive = timeToLive;
	}

	@Override
	protected @Nullable ThrowingFunction<ThrowingSupplier<ListenableFuture<StatValue<V>>, Exception>, ListenableFuture<StatValue<V>>, Exception> getStorageLockAccessor(
			@NonNull K key) {
		return null;
	}

	@Override
	protected @Nullable Duration getStorageTTL(@NonNull K key, @NonNull ListenableFuture<StatValue<V>> result) {
		return this.timeToLive;
	}

	@Override
	protected @Nullable ListenableFuture<StatValue<V>> storageGet(@NonNull K key) throws Exception {
		return readClientStorage(key);
	}

	@Override
	protected void storageUpdate(@NonNull K key, @Nullable ListenableFuture<StatValue<V>> value, @Nullable Duration ttl)
			throws Exception {
		if (value == null)
			writeClientStorage(key, null);
		else
			value.resultCallback(sv -> writeClientStorage(key, sv));
	}

	private @Nullable StatValue<V> normalizeReadValue(@NonNull K key, @Nullable StatValue<V> statValue) {
		if (statValue == null)
			return null;
		if (statValue.isExpired(this.timeToLive))
			return null;
		return statValue;
	}

	private ListenableFuture<StatValue<V>> readClientStorage(@NonNull K key) {
		var keyStr = serializeKey(key);
		if (Utils.Strings.isBlank(keyStr))
			return null;
		var readFuture = UIClientStorage.getCurrent().read(keyStr, this.local);
		var statValueFuture = readFuture.map(v -> deserializeValue(key, v));
		statValueFuture = statValueFuture.flatMap(sv -> {
			sv = normalizeReadValue(key, sv);
			if (sv != null)
				return FutureUtils.immediateResultFuture(sv);
			ListenableFuture<StatValue<V>> loadFreshFuture;
			try {
				loadFreshFuture = this.loadFresh(key);
			} catch (Throwable t) {
				return FutureUtils.immediateFailureFuture(t);
			}
			if (loadFreshFuture == null)
				return FutureUtils.immediateResultFuture(null);
			return loadFreshFuture.map(svLoaded -> {
				writeClientStorage(key, svLoaded);
				return svLoaded;
			});
		});
		return statValueFuture;
	}

	protected void writeClientStorage(@NonNull K key, StatValue<V> sv) {
		var keyStr = serializeKey(key);
		if (Utils.Strings.isBlank(keyStr))
			return;
		UIClientStorage.getCurrent().write(keyStr, serializeValue(key, sv), this.local);
	}

	@SuppressWarnings("unchecked")
	protected StatValue<V> deserializeValue(@NonNull K key, String value) {
		if (Utils.Strings.isBlank(value))
			return null;
		var encryptor = getEncryptor(key);
		if (encryptor != null) {
			var decrypted = Utils.Functions.catching(value, v -> {
				return encryptor.decrypt(Base58.decode(v));
			}, t -> {
				logger.debug("client storage decrypt error", t);
				return Utils.Bits.emptyByteArray();
			});
			if (decrypted.length == 0)
				return null;
			value = new String(decrypted, Utils.Bits.getDefaultCharset());
		}
		var signer = getSignatureFunction(key);
		if (signer != null) {
			int splitAt = Utils.Strings.lastIndexOf(value, DELIMITER);
			String body = Utils.Strings.substring(value, 0, splitAt);
			var signatureBase58 = Utils.Strings.substring(value, splitAt + 1, value.length());
			var signature = Base58.decodeToBytes(signatureBase58);
			var checkSignature = signer.apply(Utils.Bits.from(body));
			if (!Objects.equals(signature, checkSignature))
				return null;
			value = body;
		}
		return Serials.Gsons.get().fromJson(value, StatValue.class);
	}

	protected String serializeValue(@NonNull K key, StatValue<V> statValue) {
		if (statValue == null)
			return null;
		String result = Serials.Gsons.get().toJson(statValue);
		if (Utils.Strings.isBlank(result))
			return null;
		var signer = getSignatureFunction(key);
		var signature = signer == null ? null : signer.apply(Utils.Bits.from(result));
		if (signature != null)
			result = result + DELIMITER + Base58.encodeBytes(signature);
		var encryptor = getEncryptor(key);
		if (encryptor == null)
			return result;
		var encrypted = Utils.Functions.catching(result, v -> {
			return encryptor.encrypt(Utils.Bits.from(v).array());
		}, t -> {
			logger.debug("client storage encrypt error", t);
			return Utils.Bits.emptyByteArray();
		});
		return Base58.encode(encrypted);
	}

	protected abstract String serializeKey(@NonNull K key);

	protected abstract @Nullable Function<Bytes, Bytes> getSignatureFunction(@NonNull K key);

	protected abstract @Nullable Encryptor getEncryptor(@NonNull K key);

}
