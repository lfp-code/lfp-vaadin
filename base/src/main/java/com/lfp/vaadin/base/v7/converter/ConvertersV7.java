package com.lfp.vaadin.base.v7.converter;

import java.util.Locale;
import java.util.Objects;
import java.util.function.Function;

import com.github.throwable.beanref.BeanPath;

import com.lfp.vaadin.base.data.property.Properties;
import com.lfp.vaadin.base.v7.column.Columns;

import org.joda.beans.MetaProperty;

import com.vaadin.v7.data.util.converter.Converter;
import com.vaadin.v7.ui.Grid;
import com.vaadin.v7.ui.Grid.Column;

@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
public class ConvertersV7 {

	public static <T, MODEL, PRESENTATION> Column setConverter(Grid grid, BeanPath<T, MODEL> beanPath,
			Function<MODEL, PRESENTATION> converterFunction) {
		return setConverter(grid, beanPath, converterFunction);
	}

	public static <MODEL, PRESENTATION> Column setConverter(Grid grid, MetaProperty<MODEL> beanPath,
			Function<MODEL, PRESENTATION> converterFunction) {
		return setConverter(grid, beanPath, converterFunction);
	}

	public static <MODEL, PRESENTATION> Column setConverter(Grid grid, Object propertyId,
			Function<MODEL, PRESENTATION> converterFunction) {
		var column = grid.getColumn(Properties.getName(propertyId));
		return setConverter(column, converterFunction);
	}

	public static <MODEL, PRESENTATION> Column setConverter(Column column,
			Function<MODEL, PRESENTATION> converterFunction) {
		var converter = createConverter(column, converterFunction);
		column.setConverter(converter);
		return column;
	}

	public static <MODEL, PRESENTATION> Converter<PRESENTATION, MODEL> createConverter(Column column,
			Function<MODEL, PRESENTATION> converterFunction) {
		Objects.requireNonNull(column);
		Objects.requireNonNull(converterFunction);
		var currentConverter = column.getConverter();
		var modelType = Columns.getModelType(column);
		var presentationType = Columns.getPresentationType(column);
		return new Converter<PRESENTATION, MODEL>() {

			@Override
			public MODEL convertToModel(PRESENTATION value, Class<? extends MODEL> targetType, Locale locale)
					throws ConversionException {
				if (currentConverter == null)
					throw new UnsupportedOperationException("convert to model function not set");
				MODEL model = (MODEL) ((Converter) currentConverter).convertToModel(value, targetType, locale);
				return model;
			}

			@Override
			public PRESENTATION convertToPresentation(MODEL value, Class<? extends PRESENTATION> targetType,
					Locale locale) throws ConversionException {
				return converterFunction.apply(value);
			}

			@Override
			public Class<MODEL> getModelType() {
				return (Class) modelType;
			}

			@Override
			public Class<PRESENTATION> getPresentationType() {
				return (Class) presentationType;
			}

		};
	}
}
