package com.lfp.vaadin.base.v7.container.filter;

import java.util.Optional;
import java.util.function.Supplier;

import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.v7.data.BeanItemLFP;
import com.vaadin.v7.data.Container.Filter;
import com.vaadin.v7.data.Item;

public interface FilterLFP<BT> extends Filter {

	default boolean appliesToProperty(Object propertyId) {
		return true;
	}

	@SuppressWarnings("unchecked")
	default boolean passesFilter(Object itemId, Item item) throws UnsupportedOperationException {
		BeanItemLFP<BT> beanItem = Utils.Types.tryCast(item, BeanItemLFP.class).orElse(null);
		Supplier<Optional<BT>> beanSupplier = () -> {
			if (beanItem == null || !beanItem.isBeanSet())
				return Optional.empty();
			return Optional.ofNullable(beanItem.getBean());
		};
		return passesFilter(itemId, beanItem, beanSupplier);
	}

	boolean passesFilter(Object itemId, BeanItemLFP<BT> beanItem, Supplier<Optional<BT>> beanSupplier);
}
