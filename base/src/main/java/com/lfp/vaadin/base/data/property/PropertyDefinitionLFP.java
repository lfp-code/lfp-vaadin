package com.lfp.vaadin.base.data.property;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import com.lfp.joe.core.function.Throws.ThrowingConsumer;
import com.lfp.vaadin.base.v7.property.PropertiesV7;
import com.lfp.vaadin.base.v7.property.PropertyDescriptorLFP;

import com.vaadin.data.PropertyDefinition;
import com.vaadin.data.PropertySet;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.v7.data.Property;
import com.vaadin.v7.data.Property.ReadOnlyException;

@SuppressWarnings({ "deprecation", "serial" })
public interface PropertyDefinitionLFP<X, Y> extends PropertyDefinition<X, Y> {

	default PropertyDescriptorLFP<X, Y> asPropertyDescriptor() {
		return new PropertyDescriptorLFP<X, Y>() {

			@Override
			public String getName() {
				return PropertyDefinitionLFP.this.getName();
			}

			@Override
			public Class<? extends Y> getPropertyType() {
				return PropertyDefinitionLFP.this.getType();
			}

			@Override
			public Property<Y> createProperty(X bean) {
				Supplier<Y> propertyGetter = () -> PropertyDefinitionLFP.this.getGetter().apply(bean);
				var setter = PropertyDefinitionLFP.this.getSetter().orElse(null);
				ThrowingConsumer<Y, ReadOnlyException> propertySetter = setter == null ? null
						: v -> setter.accept(bean, v);
				return PropertiesV7.create(getName(), getPropertyType(), propertyGetter, propertySetter);
			}
		};
	}

	public static <X, Y> PropertyDefinitionLFP<X, Y> create(PropertyDefinition<X, Y> propertyDefinition) {
		Objects.requireNonNull(propertyDefinition);
		if (propertyDefinition instanceof PropertyDefinitionLFP)
			return (PropertyDefinitionLFP<X, Y>) propertyDefinition;
		return new PropertyDefinitionLFP<X, Y>() {

			@Override
			public ValueProvider<X, Y> getGetter() {
				return propertyDefinition.getGetter();
			}

			@Override
			public Optional<Setter<X, Y>> getSetter() {
				return propertyDefinition.getSetter();
			}

			@Override
			public Class<Y> getType() {
				return propertyDefinition.getType();
			}

			@Override
			public Class<?> getPropertyHolderType() {
				return propertyDefinition.getPropertyHolderType();
			}

			@Override
			public String getName() {
				return propertyDefinition.getName();
			}

			@Override
			public String getCaption() {
				return propertyDefinition.getCaption();
			}

			@Override
			public PropertySet<X> getPropertySet() {
				return propertyDefinition.getPropertySet();
			}
		};
	}
}
