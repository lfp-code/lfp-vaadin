package com.lfp.vaadin.base.data;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import com.lfp.joe.utils.Utils;
import com.vaadin.data.provider.AbstractDataProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;

import one.util.streamex.StreamEx;

public abstract class DataProviderLFP<T, F> extends AbstractDataProvider<T, F> {

	@Override
	public int size(Query<T, F> query) {
		var filterOp = normalizeFilter(query);
		var size = size(Pagination.builder(query).build(), filterOp);
		if (size == null)
			return 0;
		return (int) size.longValue();
	}

	@Override
	public StreamEx<T> fetch(Query<T, F> query) {
		var filterOp = normalizeFilter(query);
		var inMemorySorting = normalizeInMemorySorting(query);
		var querySortOrders = normalizeQuerySortOrders(query);
		var ible = fetch(Pagination.builder(query).build(), filterOp, querySortOrders, inMemorySorting);
		StreamEx<T> stream = com.lfp.joe.stream.Streams.of(ible);
		stream = stream.nonNull();
		return stream;
	}

	protected static <F> Optional<F> normalizeFilter(Query<?, F> query) {
		return Optional.ofNullable(query).map(Query::getFilter).flatMap(Function.identity());
	}

	protected static <T> Optional<Comparator<T>> normalizeInMemorySorting(Query<T, ?> query) {
		return Optional.ofNullable(query).map(Query::getInMemorySorting);
	}

	protected static <T> List<QuerySortOrder> normalizeQuerySortOrders(Query<T, ?> query) {
		if (query == null)
			return List.of();
		return com.lfp.joe.stream.Streams.of(query.getSortOrders()).nonNull().distinct().toImmutableList();
	}

	protected abstract Number size(Pagination pagination, Optional<F> filterOp);

	protected abstract Iterable<? extends T> fetch(Pagination pagination, Optional<F> filterOp,
			List<QuerySortOrder> querySortOrders, Optional<Comparator<T>> inMemorySorting);

}
