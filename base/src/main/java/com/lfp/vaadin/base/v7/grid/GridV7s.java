package com.lfp.vaadin.base.v7.grid;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.vaadin.teemusav7.gridextensions.client.tableselection.TableSelectionState.TableSelectionMode;
import org.vaadin.teemusav7.gridextensions.tableselection.TableSelectionModel;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.css.Style;
import com.lfp.vaadin.base.v7.data.FieldGroupLFP;
import com.lfp.vaadin.base.v7.data.Items;
import com.vaadin.v7.event.ItemClickEvent.ItemClickListener;
import com.vaadin.v7.event.SelectionEvent;
import com.vaadin.v7.event.SelectionEvent.SelectionListener;
import com.vaadin.v7.ui.Grid;
import com.vaadin.v7.ui.Grid.SelectionMode;
import com.vaadin.v7.ui.Grid.SelectionModel;

@SuppressWarnings({ "deprecation", "serial", "unchecked", "unused" })
public class GridV7s {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Style HIDE_SELECT_ALL_CHECKBOX_STYLE = Style.build(s -> {
		String css = String.format(".%s .v-grid-select-all-checkbox{visibility:hidden}", s);
		return css;
	});

	public static TableSelectionMode getTableSelectionMode(Grid grid) {
		Objects.requireNonNull(grid);
		var selectionModel = grid.getSelectionModel();
		if (selectionModel instanceof TableSelectionModel) {
			TableSelectionModel tsModel = (TableSelectionModel) selectionModel;
			var state = TableSelectionModelGetStateFunction.INSTANCE.apply(tsModel, false);
			var selectionMode = Optional.ofNullable(state).map(v -> v.selectionMode).orElse(null);
			if (selectionMode != null)
				return selectionMode;
		}
		if (selectionModel instanceof SelectionModel.Multi) {
			return TableSelectionMode.CTRL;
		} else if (selectionModel instanceof SelectionModel.Single) {
			return TableSelectionMode.SIMPLE;
		} else
			return TableSelectionMode.NONE;
	}

	public static Scrapable setSingleCheckSelectionMode(Grid grid) {
		Objects.requireNonNull(grid);
		Scrapable result = Scrapable.create();
		var selectionModel = grid.getSelectionModel();
		result.onScrap(() -> grid.setSelectionModel(selectionModel));
		grid.setSelectionMode(SelectionMode.MULTI);
		var listener = new SelectionListener() {

			private boolean disabled;

			@Override
			public void select(SelectionEvent event) {
				if (disabled)
					return;
				disabled = true;
				try {
					var addedSelects = Optional.ofNullable(event.getAdded()).orElse(Set.of());
					var currentSelects = Optional.ofNullable(event.getSelected()).orElse(Set.of());
					var addedSelect = Utils.Lots.last(addedSelects).orElse(null);
					for (var currentSelect : currentSelects)
						if (!Objects.equals(addedSelect, currentSelect))
							grid.deselect(currentSelect);
				} finally {
					disabled = false;
				}

			}
		};
		grid.addSelectionListener(listener);
		result.onScrap(() -> grid.removeSelectionListener(listener));
		var hideSelectAllCheckboxScrapable = hideSelectAllCheckbox(grid);
		result.onScrap(hideSelectAllCheckboxScrapable::scrap);
		var clickSelectScrapable = clickSelect(grid);
		result.onScrap(clickSelectScrapable::scrap);
		return result;
	}

	public static Scrapable clickSelect(Grid grid) {
		ItemClickListener listener = evt -> {
			var itemId = evt.getItemId();
			var selected = Optional.ofNullable(grid.getSelectedRows()).orElse(Set.of());
			if (!selected.contains(itemId))
				grid.select(itemId);
		};
		grid.addItemClickListener(listener);
		return Scrapable.create(() -> grid.removeItemClickListener(listener));
	}

	public static Scrapable hideSelectAllCheckbox(Grid grid) {
		return HIDE_SELECT_ALL_CHECKBOX_STYLE.add(grid);
	}

	public static FieldGroupLFP getFieldGroup(Grid grid) {
		return FieldGroupLFP.configure(grid);
	}

	public static String getRowClassName(Object itemId) {
		return "row_" + Items.hashId(itemId).encodeHex();
	}

	public static String getCellClassName(Object itemId, Object propertyId) {
		Objects.requireNonNull(propertyId);
		return "cell_" + Utils.Crypto.hashMD5(Items.hashId(itemId), propertyId).encodeHex();
	}

}
