package com.lfp.vaadin.base.pro;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;

public class ProKeyBypass {

	private ProKeyBypass() {}

	private static final Object MUTEX = new Object();
	private static boolean _CONFIGURED;

	public static boolean configure() {
		if (!_CONFIGURED)
			synchronized (MUTEX) {
				if (!_CONFIGURED) {
					try {
						configureSynchronized();
					} catch (NotFoundException | CannotCompileException | IOException e) {
						throw new RuntimeException(e);
					}
					return true;
				}
			}
		return false;
	}

	public static boolean isConfigured() {
		return _CONFIGURED;
	}

	private static void configureSynchronized() throws NotFoundException, CannotCompileException, IOException {
		System.setProperty("vaadin.proKey", String.format("user@vaadin.com/pro-%s", UUID.randomUUID().toString()));
		ClassPool pool = ClassPool.getDefault();
		CtClass cc = pool.get("com.vaadin.pro.licensechecker.Util");
		cc.defrost();
		CtMethod m = cc.getDeclaredMethod("toString", new CtClass[] { pool.get(InputStream.class.getName()) });
		m.insertBefore(
				"{return \"{\\\"result\\\":\\\"ok\\\",\\\"message\\\":\\\"pro-key check disabled\\\",\\\"subscription\\\":\\\"vaadin-subscription\\\"}\";}");
		cc.writeFile(".");
		cc.toClass();
		// var str = Util.toString(new ByteArrayInputStream("test".getBytes()));
		// System.out.println(str);
	}

}
