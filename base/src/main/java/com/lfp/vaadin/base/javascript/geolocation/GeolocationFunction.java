package com.lfp.vaadin.base.javascript.geolocation;

import java.util.List;

import com.google.gson.JsonElement;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.javascript.JavascriptFunctionAsync;
import com.vaadin.server.ClientConnector;

import elemental.json.JsonArray;
import elemental.json.JsonValue;

public class GeolocationFunction extends JavascriptFunctionAsync.Abs<GeolocationCoordinates> {

	public static final GeolocationFunction INSTANCE = new GeolocationFunction();

	GeolocationFunction() {
	}

	@Override
	protected String getScript() {
		var script = Utils.Resources.getResourceAsString("com/lfp/vaadin/base/client/generic/clientLocation.js")
				.get();
		return script;
	}

	@Override
	protected GeolocationCoordinates parseResult(ClientConnector clientConnector, List<JsonValue> arguments) {
		for (var jsonValue : arguments) {
			if (jsonValue == null)
				continue;
			var json = jsonValue.toJson();
			JsonElement je;
			Integer errorCode;
			String errorMessage;
			if (Utils.Strings.isBlank(json)) {
				je = null;
				errorCode = 2;
				errorMessage = UnsupportedOperationException.class.getSimpleName();
			} else {
				je = Serials.Gsons.getJsonParser().parse(jsonValue.toJson());
				errorCode = Serials.Gsons.tryGetAsNumber(je, "code").map(Number::intValue).orElse(null);
				errorMessage = Serials.Gsons.tryGetAsString(je, "message").orElse(null);
			}
			GeolocationCoordinates geolocationCoordinates;
			if (errorCode != null) {
				geolocationCoordinates = new GeolocationCoordinates();
				geolocationCoordinates.setErrorCode(errorCode);
				geolocationCoordinates.setErrorMessage(errorMessage);
			} else
				geolocationCoordinates = Serials.Gsons.get().fromJson(je, GeolocationCoordinates.class);
			return geolocationCoordinates;
		}

		return null;
	}

}
