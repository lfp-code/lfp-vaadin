package com.lfp.vaadin.base.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

import com.vaadin.server.FileDownloader;
import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;

public class DynamicFileDownloader extends FileDownloader {

	private final Consumer<StreamResource> streamResourceModifier;

	public DynamicFileDownloader(ThrowingSupplier<InputStream, IOException> inputStreamSupplier,
			Consumer<StreamResource> streamResourceModifier) {
		super(createStreamResource(inputStreamSupplier));
		this.streamResourceModifier = streamResourceModifier;
	}


	@Override
	public boolean handleConnectorRequest(VaadinRequest request, VaadinResponse response, String path)
			throws IOException {
		if (this.streamResourceModifier != null)
			this.streamResourceModifier.accept(getResource());
		return super.handleConnectorRequest(request, response, path);
	}

	private StreamResource getResource() {
		StreamResource result = null;
		this.getSession().lock();
		try {
			result = (StreamResource) this.getResource("dl");
		} finally {
			this.getSession().unlock();
		}
		return result;
	}
	
	private static Resource createStreamResource(ThrowingSupplier<InputStream, IOException> inputStreamSupplier) {
		var sr = new StreamResource(() -> {
			InputStream is;
			if (inputStreamSupplier == null)
				is = null;
			else
				is = Utils.Functions.unchecked(inputStreamSupplier::get);
			if (is == null)
				is = Utils.Bits.emptyInputStream();
			return is;
		}, "");
		sr.setCacheTime(0);
		return sr;
	}

}
