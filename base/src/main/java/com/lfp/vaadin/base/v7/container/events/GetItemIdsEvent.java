package com.lfp.vaadin.base.v7.container.events;

import java.lang.reflect.Method;
import java.util.EventObject;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.v7.container.ContainerLFP;

import com.vaadin.event.SerializableEventListener;
import com.vaadin.server.ClientConnector;

public class GetItemIdsEvent<ID, T> extends EventObject {

	private static final long serialVersionUID = -6921339612898285989L;

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static final String EVENT_ID = THIS_CLASS.getName();
	public static final Method METHOD = Utils.Functions.unchecked(() -> {
		return Consumer.class.getDeclaredMethod("accept", Object.class);
	});

	public static interface Listener<ID, T> extends SerializableEventListener, Consumer<GetItemIdsEvent<ID, T>> {
	}

	public static enum Action {
		DIRECT, RPC, FILTER, SORT;
	}

	private final Action action;
	private final List<ID> itemIds;

	public GetItemIdsEvent(ContainerLFP<ID, T> container, Action action, Iterable<ID> itemIds) {
		super(container);
		this.action = Objects.requireNonNull(action);
		this.itemIds = com.lfp.joe.stream.Streams.of(itemIds).nonNull().distinct().toImmutableList();
	}

	public GetItemIdsEvent<ID, T> withItemIds(List<ID> itemIds) {
		if (itemIds == getItemIds())
			return this;
		return new GetItemIdsEvent<>(this.getSource(), this.getAction(), itemIds);
	}

	public Action getAction() {
		return this.action;
	}

	public List<ID> getItemIds() {
		return this.itemIds;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ContainerLFP<ID, T> getSource() {
		return (ContainerLFP<ID, T>) super.getSource();
	}
}
