package com.lfp.vaadin.base.v7.property;

import com.vaadin.v7.data.Property;
import com.vaadin.v7.data.util.VaadinPropertyDescriptor;

@SuppressWarnings({ "deprecation" })
public interface PropertyDescriptorLFP<B, P> extends VaadinPropertyDescriptor<B> {

	@Override
	public Class<? extends P> getPropertyType();

	@Override
	public Property<P> createProperty(B bean);
}
