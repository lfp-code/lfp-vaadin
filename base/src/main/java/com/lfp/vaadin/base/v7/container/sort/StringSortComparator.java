package com.lfp.vaadin.base.v7.container.sort;

import com.lfp.joe.core.lot.Lot; import com.lfp.joe.core.lot.AbstractLot; import java.time.Duration;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.lot.AbstractLot;

public enum StringSortComparator implements Comparator<String> {
	INSTANCE;

	private final Cache<Character, Character> CONVERSION_CACHE = Caffeine.newBuilder()
			.expireAfterAccess(Duration.ofSeconds(1)).build();

	@Override
	public int compare(String str1, String str2) {
		Iterator<Character> charIter1 = toCharacterIterator(str1);
		Iterator<Character> charIter2 = toCharacterIterator(str2);
		while (charIter1.hasNext() || charIter2.hasNext()) {
			var char1 = charIter1.hasNext() ? charIter1.next() : null;
			var char2 = charIter2.hasNext() ? charIter2.next() : null;
			if (char1 == null || char2 == null)
				return Utils.Lots.comparatorNullsLast().compare(char1, char2);
			var comparison = char1.compareTo(char2);
			if (comparison != 0)
				return comparison;
		}
		return 0;
	}

	private Iterator<Character> toCharacterIterator(String value) {
		if (value == null || value.isEmpty())
			return Collections.emptyIterator();
		return new AbstractLot.Indexed<Character>() {

			private int currentIndex = -1;
			private boolean foundNonWhitespace;

			@Override
			protected Character computeNext(long index) {
				while (currentIndex + 1 < value.length()) {
					currentIndex++;
					Optional<Character> nextOp = computeNextCharacter();
					if (nextOp.isPresent())
						return nextOp.get();
				}
				return this.end();
			}

			private Optional<Character> computeNextCharacter() {
				char charAt = nextChar();
				if (Character.isWhitespace(charAt) && !foundNonWhitespace)
					return Optional.empty();
				foundNonWhitespace = true;
				return Optional.of(charAt);
			}

			private char nextChar() {
				char charAt = value.charAt(currentIndex);
				return CONVERSION_CACHE.get(charAt, nil -> convert(charAt));
			}
		};
	}

	private Character convert(char charAt) {
		if (Character.isWhitespace(charAt))
			return ' ';
		var str = charAt + "";
		str = Utils.Strings.stripAccents(str);
		str = str.replaceAll("[^\\s\\p{L}\\p{N}]+", "");
		str = Utils.Strings.trimToEmpty(str);
		str = str.toLowerCase();
		if (str.isEmpty())
			return ' ';
		return str.charAt(0);
	}

}