package com.lfp.vaadin.base.session;

import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory;
import java.util.concurrent.ForkJoinWorkerThread;
import java.util.concurrent.Future;
import java.util.function.Function;

import org.threadly.concurrent.wrapper.interceptor.ExecutorTaskInterceptor;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.FNV;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Sequencer;
import com.lfp.joe.core.lock.ThreadRegistry;
import com.lfp.joe.core.process.ThreadRenamer;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.reactor.support.ReactorSubmitterScheduler;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.executor.ExecutorServiceAdapter;
import com.lfp.joe.threads.executor.SubmitterSchedulerLFP;
import com.lfp.joe.utils.Utils;
import com.vaadin.ui.UI;

import one.util.streamex.LongStreamEx;

public class SessionExecutor extends Scrapable.Impl implements ReactorSubmitterScheduler {

	public static SessionExecutor getCurrent() {
		return SessionAttributes.getCurrent().compute(SessionExecutor.class, current -> {
			if (current != null)
				return current;
			return new SessionExecutor();
		});
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Sequencer SEQUENCER = new Sequencer();

	private final Object delegateMutex = new Object();
	private Executor delegate;
	private final ThreadRenamer threadRenamer;
	private final ThreadRegistry threadRegistry;

	protected SessionExecutor() {
		var idParts = LongStreamEx.of(SEQUENCER.next().values())
				.append(Optional.ofNullable(UI.getCurrent())
						.map(v -> v.getSession())
						.map(v -> v.getSession())
						.map(v -> v.getId())
						.map(Utils.Strings::trimToNull)
						.stream()
						.mapToLong(FNV::hash64));
		var identifier = Utils.Crypto.hashFNV(idParts.toArray()).encodeHex();
		var threadIdentitifer = String.format("%s_%s", this.getClass().getSimpleName(), identifier);
		this.threadRenamer = ThreadRenamer.of(threadIdentitifer);
		this.threadRegistry = new ThreadRegistry();
	}

	@Override
	public Executor getContainedExecutor() {
		if (delegate == null)
			synchronized (delegateMutex) {
				if (delegate == null)
					delegate = createDelegate();
			}
		return delegate;
	}

	protected SubmitterSchedulerLFP createDelegate() {
		logger.info("creating session executor:{}", this.threadRenamer.name());
		Function<Runnable, Runnable> taskModifier = threadRenamer;
		taskModifier = taskModifier.andThen(task -> () -> {
			try (var r = threadRegistry.register()) {
				task.run();
			}
		});
		var executor = CoreTasks.executor();
		executor = new ExecutorTaskInterceptor(executor, taskModifier);
		executor = CurrentInstances.intercept(executor);
		var executorService = ExecutorServiceAdapter.adapt(executor);
		this.onScrap(() -> {
			Streams.of(executorService.shutdownNow())
					.flatMap(Threads.Futures::streamContainedRunnables)
					.select(Future.class)
					.forEach(v -> v.cancel(true));
			this.threadRegistry.close(true);
		});
		return SubmitterSchedulerLFP.from(executorService);
	}

	public ForkJoinPool forkJoinPool() {
		return forkJoinPool(MachineConfig.logicalCoreCount());
	}

	public ForkJoinPool forkJoinPool(int parallelism) {
		var currentInstances0 = CurrentInstances.getInstances();
		ForkJoinWorkerThreadFactory threadFactory = pool -> {
			var currentInstances1 = CurrentInstances.concatInstances(currentInstances0,
					CurrentInstances.getInstances());
			return new ForkJoinWorkerThread(pool) {

				@Override
				protected void onStart() {
					super.onStart();
					threadRegistry.register(this);
					CurrentInstances.restoreInstances(currentInstances1, CurrentInstances.getInstances());
				}

				@Override
				protected void onTermination(Throwable exception) {
					try {
						threadRegistry.unregister(this);
					} finally {
						super.onTermination(exception);
					}
				}

			};
		};
		return new ForkJoinPool(parallelism, threadFactory, null, false);
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			scrap();
		} finally {
			super.finalize();
		}
	}

}
