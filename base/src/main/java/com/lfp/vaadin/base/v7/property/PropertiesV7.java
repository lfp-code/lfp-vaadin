package com.lfp.vaadin.base.v7.property;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingBiConsumer;
import com.lfp.joe.core.function.Throws.ThrowingConsumer;

import org.apache.commons.lang3.Validate;

import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.Property;
import com.vaadin.v7.data.Property.ReadOnlyException;

@SuppressWarnings({ "deprecation", "rawtypes", "unchecked" })
public class PropertiesV7 {

	public static <X> Optional<X> tryGetValue(Item item, Object propertyId) {
		if (item == null)
			return Optional.empty();
		return tryGetValue(item.getItemProperty(propertyId));
	}

	public static <X> Optional<X> tryGetValue(Property<X> property) {
		if (property == null)
			return Optional.empty();
		return Optional.ofNullable(property.getValue());
	}

	public static <P> boolean fireValueChange(Property<P> property) {
		Objects.requireNonNull(property);
		if (!(property instanceof Property.ValueChangeNotifier))
			return false;
		if (property instanceof AbstractPropertyLFP) {
			((AbstractPropertyLFP) property).fireValueChange();
			return true;
		}
		var methods = JavaCode.Reflections.streamMethods(property.getClass(), true, m -> m.getParameterCount() == 0,
				m -> m.getName().startsWith("fire"), m -> Utils.Strings.containsIgnoreCase(m.getName(), "valuechange"))
				.limit(2).toList();
		if (methods.size() != 1)
			return false;
		var method = methods.get(0);
		method.setAccessible(true);
		Utils.Functions.unchecked(() -> method.invoke(property));
		return true;
	}

	public static <X, P> PropertyDescriptorLFP<X, P> createDescriptor(String propertyId, Class<P> propertyType,
			Function<X, P> getter) {
		return createDescriptor(propertyId, propertyType, getter, null);
	}

	@SuppressWarnings("serial")
	public static <X, P> PropertyDescriptorLFP<X, P> createDescriptor(String propertyName,
			Class<? extends P> propertyType, Function<X, P> getter,
			ThrowingBiConsumer<X, P, ReadOnlyException> setter) {
		Validate.notBlank(propertyName);
		Objects.requireNonNull(propertyType);
		Objects.requireNonNull(getter);
		return new PropertyDescriptorLFP<X, P>() {

			@Override
			public String getName() {
				return propertyName;
			}

			@Override
			public Class<? extends P> getPropertyType() {
				return propertyType;
			}

			@Override
			public Property<P> createProperty(X bean) {
				return create(propertyName, propertyType, () -> getter.apply(bean),
						setter == null ? null : v -> setter.accept(bean, v));
			}
		};
	}

	public static <X, P> Property<P> create(String propertyName, Class<P> propertyType, Supplier<P> getter) {
		return create(propertyName, propertyType, getter, null);
	}

	@SuppressWarnings("serial")
	public static <X, P> Property<P> create(String propertyName, Class<? extends P> propertyType, Supplier<P> getter,
			ThrowingConsumer<P, ReadOnlyException> setter) {
		Validate.notBlank(propertyName);
		Objects.requireNonNull(propertyType);
		Objects.requireNonNull(getter);
		return new AbstractPropertyLFP<P>() {

			@Override
			public P getValue() {
				var value = getter.get();
				return value;
			}

			@Override
			public void setValueInternal(P newValue) throws ReadOnlyException {
				if (setter == null)
					throw new ReadOnlyException("read only property:" + propertyName);
				setter.accept(newValue);
			}

			@Override
			public Class getType() {
				return propertyType;
			}

		};
	}

}
