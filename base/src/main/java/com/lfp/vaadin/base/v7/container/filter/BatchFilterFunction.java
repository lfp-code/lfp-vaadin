package com.lfp.vaadin.base.v7.container.filter;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.function.Function;
import java.util.stream.Stream;

import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public class BatchFilterFunction<X> extends Scrapable.Impl implements Function<Stream<X>, StreamEx<X>> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private Set<ListenableFuture<List<X>>> runningFutures = new HashSet<>();
	private int batchSize;
	private SubmitterExecutor executor;
	private Function<Collection<X>, Collection<X>> filterFunction;

	public BatchFilterFunction(int batchSize, Function<Collection<X>, Collection<X>> filterFunction) {
		this(batchSize, filterFunction, Threads.Pools.centralPool().limit(Utils.Machine.logicalProcessorCount()));
	}

	public BatchFilterFunction(int batchSize, Function<Collection<X>, Collection<X>> filterFunction,
			Executor executor) {
		this.batchSize = batchSize;
		this.executor = SubmitterExecutorAdapter.adaptExecutor(executor);
		this.filterFunction = Objects.requireNonNull(filterFunction);
		this.onScrap(() -> {
			synchronized (runningFutures) {
				for (var runningFuture : runningFutures)
					Threads.Futures.cancel(runningFuture, true);
			}
		});
	}

	@Override
	public StreamEx<X> apply(Stream<X> input) {
		if (input == null)
			return StreamEx.empty();
		var batchStream = Utils.Lots.buffer(com.lfp.joe.stream.Streams.of(input), batchSize);
		var futureStream = batchStream.map(batch -> {
			ListenableFuture<List<X>> filterFuture;
			synchronized (runningFutures) {
				if (this.isScrapped())
					return FutureUtils.immediateResultFuture(batch);
				filterFuture = executor.submit(() -> {
					if (this.isScrapped())
						return batch;
					var passing = this.filterFunction.apply(batch);
					return com.lfp.joe.stream.Streams.of(passing).nonNull().toImmutableList();
				});
				runningFutures.add(filterFuture);
			}
			var resultFuture = filterFuture.map(passing -> {
				return com.lfp.joe.stream.Streams.of(batch).filter(v -> passing.contains(v)).toList();
			});
			resultFuture = resultFuture.mapFailure(Throwable.class, t -> {
				return onError(batch, t);
			});
			resultFuture = resultFuture.map(v -> {
				synchronized (runningFutures) {
					runningFutures.remove(filterFuture);
				}
				return v;
			});
			return resultFuture;
		});
		batchStream = Threads.Futures.completeStream(futureStream, true);
		return Utils.Lots.flatMapIterables(batchStream);
	}

	protected List<X> onError(List<X> batch, Throwable t) {
		if (!Utils.Exceptions.isCancelException(t))
			logger.error("error during filter check", t);
		return batch;
	}

}