package com.lfp.vaadin.base.javascript;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.vaadin.server.ClientConnector;

import elemental.json.JsonValue;

public interface JavascriptFunctionAsync<X>
		extends Function<ClientConnector, ListenableFuture<X>>, Supplier<ListenableFuture<X>> {

	@Override
	default ListenableFuture<X> get() {
		return apply(null);
	}

	@Override
	ListenableFuture<X> apply(ClientConnector clientConnector);

	public static abstract class Abs<X> implements JavascriptFunctionAsync<X> {

		@Override
		public ListenableFuture<X> apply(ClientConnector clientConnector) {
			if (clientConnector != null && !clientConnector.isAttached()) {
				SettableListenableFuture<X> sfuture = new SettableListenableFuture<>(false);
				clientConnector.addAttachListener(evt -> {
					if (sfuture.isDone())
						return;
					var future = apply(clientConnector);
					future.failureCallback(sfuture::setFailure);
					future.resultCallback(sfuture::setResult);
					sfuture.listener(() -> future.cancel(true));
				});
				return sfuture;
			}
			String script = getScript();
			return JavaScripts.executeAsync(script).map(v -> parseResult(clientConnector, v));
		}

		protected abstract String getScript();

		protected abstract X parseResult(ClientConnector clientConnector, List<JsonValue> arguments);

	}
}
