package com.lfp.vaadin.base.css;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.function.FNV;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.events.bobbus.SubscribeOptions;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.javascript.JavaScripts;
import com.lfp.vaadin.base.ui.RefreshEvent;
import com.lfp.vaadin.base.ui.UIEventBus;
import com.lfp.vaadin.base.ui.UIX;
import com.lfp.vaadin.base.ui.UIs;

import net.engio.mbassy.listener.References;

public class CSS {

	public static CSS getCurrent() {
		var attributes = UIX.getCurrent().getAttributes();
		return attributes.computeIfAbsent(CSS.class, CSS::new);
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final int VERSION = 0;
	private final Map<Long, CSSRecord> styleMap = new ConcurrentHashMap<>();

	public CSS() {
		setup();
		UIEventBus.getCurrent().subscribe((Consumer<RefreshEvent>) evt -> {
			setup(true);
		}, SubscribeOptions.builder().references(References.Strong).build());
	}

	public Scrapable add(String css) {
		var styleTagId = getStyleId(css);
		this.styleMap.computeIfAbsent(styleTagId, nil -> CSSRecord.create(css));
		setup();
		return Scrapable.create(() -> {
			this.styleMap.put(styleTagId, CSSRecord.empty());
			setup();
		});
	}

	private void setup() {
		setup(false);
	}

	private void setup(boolean forceReAdd) {
		List<String> scripts = new ArrayList<>();
		for (var key : List.copyOf(styleMap.keySet())) {
			styleMap.compute(key, (nil, rec) -> {
				if (rec == null)
					return null;
				if (rec.getValue().isEmpty()) {
					scripts.add(createScript(key, null));
					return null;
				}
				if (forceReAdd || rec.isWriteRequired())
					scripts.add(createScript(key, rec.getValue().get()));
				rec.writeComplete();
				return rec;
			});
		}
		var allScripts = com.lfp.joe.stream.Streams.of(scripts).filter(Utils.Strings::isNotBlank).joining("\n");
		if (Utils.Strings.isBlank(allScripts))
			return;
		UIs.getCurrent().access(() -> JavaScripts.execute(allScripts));
	}

	private static String createScript(long styleId, String css) {
		String styleTagId = getStyleTagId(styleId);
		String removeScript;
		{
			var script = Utils.Resources.getResourceAsString(Configs.get(CSSConfig.class).removeScriptPath()).get();
			removeScript = Utils.Strings.templateApply(script, "ID", styleTagId);
		}
		String addScript;
		if (Utils.Strings.isBlank(css))
			addScript = null;
		else {
			var script = Utils.Resources.getResourceAsString(Configs.get(CSSConfig.class).addScriptPath()).get();
			addScript = Utils.Strings.templateApply(script, "ID", styleTagId, "CSS",
					StringEscapeUtils.escapeEcmaScript(css));
		}
		return com.lfp.joe.stream.Streams.of(removeScript, addScript).filter(Utils.Strings::isNotBlank).joining("\n");
	}

	private static final long getStyleId(String css) {
		css = Utils.Strings.trim(css);
		Validate.notBlank(css);
		var hash = FNV.hash64(Utils.Bits.from(THIS_CLASS.getName()).array(), Utils.Bits.from(VERSION).array(),
				Utils.Bits.from(css).array());
		return hash;
	}

	private static final String getStyleTagId(long styleId) {
		String id = "x";
		if (styleId < 0) {
			id += "0";
			styleId = styleId * -1;
		}
		id += styleId;
		return id;
	}

}
