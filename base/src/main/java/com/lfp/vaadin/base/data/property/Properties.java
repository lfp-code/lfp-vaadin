package com.lfp.vaadin.base.data.property;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.lang3.Validate;
import org.joda.beans.Bean;
import org.joda.beans.ImmutableBean;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;

import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.utils.Utils;
import com.vaadin.data.PropertyDefinition;
import com.vaadin.data.PropertySet;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.shared.util.SharedUtil;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

@SuppressWarnings({ "rawtypes", "serial", "unchecked" })
public class Properties {

	private static final List<Pattern> BEAN_PATH_EXCLUDE_PACKAGE = List.of(Pattern.compile("^java\\..*"));
	private static final List<Pattern> BEAN_PATH_EXCLUDE_CLASS_NAME = List.of(Pattern.compile(".*\\$.*"));
	private static final List<Pattern> BEAN_PATH_EXCLUDE_PATH = List.of(Pattern.compile("^build.*"),
			Pattern.compile("^toBuild.*"), Pattern.compile("^meta.*"), Pattern.compile("^declaringClass.*"),
			Pattern.compile("^ordinal.*"));

	public static Optional<String> tryGetName(Object propertyId) {
		String propertyName;
		if (propertyId instanceof MetaProperty)
			propertyName = ((MetaProperty) propertyId).name();
		else if (propertyId instanceof BeanPath)
			propertyName = ((BeanPath) propertyId).getPath();
		else if (propertyId != null)
			propertyName = propertyId.toString();
		else
			propertyName = null;
		if (Utils.Strings.isBlank(propertyName))
			return Optional.empty();
		return Optional.of(propertyName);
	}

	public static String getName(Object propertyId) {
		Optional<String> propertyNameOp = tryGetName(propertyId);
		return propertyNameOp.orElseThrow(() -> {
			throw new IllegalArgumentException(String.format("invalid property name:%s", propertyId));
		});
	}

	public static <X> PropertySetLFP<X> createSet(PropertySet<X>... propertySets) {
		if (propertySets != null && propertySets.length == 1 && propertySets[0] instanceof PropertySetLFP)
			return (PropertySetLFP<X>) propertySets[0];
		return new PropertySetLFP<X>() {

			@Override
			public Optional<PropertyDefinition<X, ?>> getProperty(String name) {
				if (propertySets != null)
					for (var propertySet : propertySets) {
						if (propertySet == null)
							continue;
						var op = propertySet.getProperty(name);
						if (op.isPresent())
							return op;
					}
				return Optional.empty();
			}

			@Override
			public StreamEx<PropertyDefinition<X, ?>> getProperties() {
				StreamEx<PropertyDefinition<X, ?>> resultStream = StreamEx.empty();
				if (propertySets != null)
					for (var propertySet : propertySets) {
						if (propertySet == null)
							continue;
						var stream = propertySet.getProperties();
						if (stream == null)
							continue;
						resultStream = resultStream.append(stream);
					}
				return resultStream;
			}
		};
	}

	public static <X> PropertySetLFP<X> createSet(boolean nested, Class<X> beanType, Class<?>... additionalBeanTypes) {
		var propertySet = new PropertySetLFP<X>() {

			private final MemoizedSupplier<Map<String, PropertyDefinition<X, ?>>> propertyMapSupplier = Utils.Functions
					.memoize(() -> createDefinitionMap(this, nested, beanType, additionalBeanTypes));

			@Override
			public StreamEx<PropertyDefinition<X, ?>> getProperties() {
				return EntryStreams.of(propertyMapSupplier.get()).values();
			}

			@Override
			public Optional<PropertyDefinition<X, ?>> getProperty(String name) {
				var map = propertyMapSupplier.get();
				var pd = map.get(name);
				return Optional.ofNullable(pd);
			}
		};
		return propertySet;
	}

	public static <X> Map<String, PropertyDefinition<X, ?>> createDefinitionMap(PropertySet<X> propertySet,
			boolean nested, Class<X> beanType, Class<?>... additionalBeanTypes) {
		List<Class<?>> beanTypeList;
		{
			StreamEx<Class<?>> beanTypeStream = com.lfp.joe.stream.Streams.of(Arrays.asList(beanType));
			if (additionalBeanTypes != null)
				beanTypeStream = beanTypeStream.append(additionalBeanTypes);
			beanTypeStream = beanTypeStream.nonNull().distinct();
			beanTypeList = beanTypeStream.toList();
		}
		var pathStream = streamPaths(nested, beanTypeList);
		Function<String, BeanPath<X, ?>> beanPathLookup = path -> {
			for (var bt : beanTypeList)
				try {
					var beanPath = BeanRef.$(bt).$(path);
					if (beanPath != null)
						return (BeanPath<X, ?>) beanPath;
				} catch (Exception e) {
					// suppress
				}
			return null;
		};
		Map<String, PropertyDefinition<X, ?>> map = new LinkedHashMap<>();
		for (var path : pathStream) {
			BeanPath<X, ?> beanPath = beanPathLookup.apply(path);
			Validate.notNull(beanPath, "unable to find bean path:%s beanTypes:%s", path, beanTypeList);
			map.computeIfAbsent(path, nil -> createDefinition(propertySet, beanPath));
		}
		return map;
	}

	public static <X, Y> PropertyDefinition<X, Y> createDefinition(PropertySet<X> propertySet,
			BeanPath<X, Y> beanPath) {
		Objects.requireNonNull(beanPath);
		BiConsumer<X, Y> setter;
		if (!beanPath.isReadOnly())
			setter = (b, v) -> beanPath.set(b, v);
		else {
			Field immutableBeanField = lookupImmutableBeanField(beanPath);
			if (immutableBeanField != null)
				setter = (b, v) -> Utils.Functions.unchecked(() -> immutableBeanField.set(b, v));
			else
				setter = null;
		}
		return createDefinition(propertySet, beanPath.getBeanClass(), beanPath.getPath(), beanPath.getType(),
				b -> beanPath.get(b), setter);
	}

	public static <X, V> PropertyDefinition<X, V> createDefinition(PropertySet<X> propertySet,
			Class<X> propertyHolderType, String propertyName, Class<V> propertyType, Function<X, V> getter,
			BiConsumer<X, V> setter) {
		Objects.requireNonNull(propertySet);
		Objects.requireNonNull(propertyName);
		Objects.requireNonNull(propertyType);
		Objects.requireNonNull(getter);
		return new PropertyDefinition<X, V>() {

			@Override
			public String getName() {
				return propertyName;
			}

			@Override
			public ValueProvider<X, V> getGetter() {
				return getter::apply;
			}

			@Override
			public Optional<Setter<X, V>> getSetter() {
				if (setter == null)
					return Optional.empty();
				return Optional.of(setter::accept);
			}

			@Override
			public Class<V> getType() {
				return propertyType;
			}

			@Override
			public Class<?> getPropertyHolderType() {
				return propertyHolderType;
			}

			@Override
			public String getCaption() {
				return SharedUtil.propertyIdToHumanFriendly(getName());
			}

			@Override
			public PropertySet<X> getPropertySet() {
				return propertySet;
			}

		};
	}

	private static StreamEx<String> streamPaths(boolean nested, List<Class<?>> beanTypeList) {
		StreamEx<String> result = StreamEx.empty();
		Set<String> uniqueTracker = new HashSet<>();
		for (var bt : beanTypeList)
			result = result.append(streamPaths(nested, bt, Collections.emptyList(), uniqueTracker));
		return result;
	}

	private static StreamEx<String> streamPaths(boolean nested, Class<?> beanType, List<Entry<Class<?>, String>> chain,
			Set<String> uniqueTracker) {
		if (beanType == null)
			return StreamEx.empty();
		beanType = Utils.Types.wrapperToPrimitive(beanType);
		if (beanType.isPrimitive())
			return StreamEx.empty();
		if (Iterable.class.isAssignableFrom(beanType))
			return StreamEx.empty();
		if (Iterator.class.isAssignableFrom(beanType))
			return StreamEx.empty();
		if (Stream.class.isAssignableFrom(beanType))
			return StreamEx.empty();
		for (var patt : BEAN_PATH_EXCLUDE_PACKAGE)
			if (patt.matcher(beanType.getPackageName()).matches())
				return StreamEx.empty();
		for (var patt : BEAN_PATH_EXCLUDE_CLASS_NAME)
			if (patt.matcher(beanType.getName()).matches())
				return StreamEx.empty();
		StreamEx<String> result = StreamEx.empty();
		var pathPrefix = com.lfp.joe.stream.Streams.of(chain).map(v -> v.getValue()).joining(".");
		EntryStream<Class<?>, String> typeToPathStream = EntryStream.empty();
		{// joda bean
			if (Bean.class.isAssignableFrom(beanType)) {
				var metaBean = JodaBeanUtils.metaBean(beanType);
				for (var mp : metaBean.metaPropertyIterable())
					typeToPathStream = typeToPathStream.append(mp.propertyType(), mp.name());
			}
		}
		{// bean paths
			var beanPaths = BeanRef.$(beanType).all();
			for (var bp : beanPaths)
				typeToPathStream = typeToPathStream.append(bp.getType(), bp.getPath());
		}
		typeToPathStream = typeToPathStream.nonNullKeys().nonNullValues();
		typeToPathStream = typeToPathStream.distinctValues();
		var typeToPathIter = typeToPathStream.iterator();
		while (typeToPathIter.hasNext()) {
			var typeToPath = typeToPathIter.next();
			Class<?> type = typeToPath.getKey();
			String path = typeToPath.getValue();
			for (var patt : BEAN_PATH_EXCLUDE_PATH)
				if (patt.matcher(path).matches()) {
					path = null;
					break;
				}
			if (Utils.Strings.isBlank(path))
				continue;
			String fullPath = com.lfp.joe.stream.Streams.of(pathPrefix, path)
					.filter(Utils.Strings::isNotBlank)
					.joining(".");
			if (!uniqueTracker.add(fullPath))
				continue;
			result = result.append(fullPath);
			if (nested) {
				Entry<Class<?>, String> chainEntry = Utils.Lots.entry(type, path);
				if (!chain.contains(chainEntry)) {
					var nestedChain = com.lfp.joe.stream.Streams.of(chain).append(chainEntry).toList();
					result = result.append(streamPaths(nested, chainEntry.getKey(), nestedChain, uniqueTracker));
				}
			}
		}
		return result;
	}

	private static Field lookupImmutableBeanField(BeanPath<?, ?> beanPath) {
		if (beanPath == null)
			return null;
		var rootClassType = beanPath.getBeanClass();
		var path = beanPath.getPath();
		var splitAt = Utils.Strings.lastIndexOf(path, ".");
		var localPath = splitAt < 0 ? path : path.substring(splitAt);
		var parentPath = splitAt < 0 ? "" : path.substring(0, splitAt);
		Class<?> parentClassType;
		if (Utils.Strings.isBlank(parentPath))
			parentClassType = rootClassType;
		else
			parentClassType = BeanRef.$(rootClassType).$(parentPath).getType();
		if (!ImmutableBean.class.isAssignableFrom(parentClassType))
			return null;
		var metaBean = JodaBeanUtils.metaBean(parentClassType);
		for (var mp : metaBean.metaPropertyIterable()) {
			if (mp.name().equals(localPath)) {
				var field = JodaBeans.getField(mp).orElse(null);
				if (field != null)
					return field;
			}
		}
		return null;
	}
}
