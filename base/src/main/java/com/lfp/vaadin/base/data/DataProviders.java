package com.lfp.vaadin.base.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.ForkJoinPoolContext;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.vaadin.data.provider.CallbackDataProvider.CountCallback;
import com.vaadin.data.provider.CallbackDataProvider.FetchCallback;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.shared.data.sort.SortDirection;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class DataProviders {

	public static <X, F extends Predicate<X>> DataProvider<X, F> create() {
		return create(new ArrayList<>());
	}

	public static <X, F extends Predicate<X>> DataProvider<X, F> create(List<? extends X> backingList) {
		return create(backingList, null);
	}

	public static <X, F extends Predicate<X>> DataProvider<X, F> create(List<? extends X> backingList,
			Function<? super X, byte[]> itemHashFunction) {
		FetchCallback<X, F> fetchCallback = createFetchCallback(backingList, itemHashFunction);
		CountCallback<X, F> countCallback = createCountCallback(backingList);
		return DataProvider.fromFilteringCallbacks(fetchCallback, countCallback);
	}

	public static <X, F extends Predicate<X>> FetchCallback<X, F> createFetchCallback(Iterable<? extends X> iterable,
			Function<? super X, byte[]> itemHashFunction) {
		Cache<Long, Map<Long, Integer>> itemSetCache = Caffeine.newBuilder().softValues().build();
		Cache<Long, Integer> compareToCache = Caffeine.newBuilder().softValues().build();
		FetchCallback<X, F> fetchCallback = req -> {
			if (iterable == null)
				return StreamEx.empty();
			StreamEx<X> stream = sort(iterable, itemHashFunction, itemSetCache, compareToCache, req);
			stream = stream.skip(req.getOffset());
			stream = stream.limit(req.getLimit());
			var filterOp = req.getFilter();
			if (filterOp.isPresent())
				stream = stream.filter(filterOp.get()).chain(Streams.parallelSequential(ForkJoinPoolContext::create));
			return stream;
		};
		return fetchCallback;
	}

	public static <X, F extends Predicate<X>> CountCallback<X, F> createCountCallback(Iterable<? extends X> iterable) {
		CountCallback<X, F> countCallback = req -> {
			if (iterable == null)
				return 0;
			var filterOp = req.getFilter();
			if (filterOp.isEmpty()) {
				if (iterable instanceof Collection)
					return ((Collection<?>) iterable).size();
				return (int) Streams.of(iterable).count();
			}
			var stream = Streams.of(iterable);
			stream = stream.filter(filterOp.get()).chain(Streams.parallelSequential(ForkJoinPoolContext::create));
			var count = stream.count();
			return (int) count;
		};
		return countCallback;
	}

	public static <X, F> Query<X, F> queryAll() {
		return new Query<>();
	}

	public static <X> StreamEx<X> streamAll(DataProvider<X, ?> dataProvider) {
		if (dataProvider == null)
			return StreamEx.empty();
		var stream = com.lfp.joe.stream.Streams.of(dataProvider.fetch(DataProviders.queryAll()));
		return stream;
	}

	public static <X> Optional<X> next(DataProvider<X, ?> dataProvider, X item) {
		if (item == null)
			return Optional.empty();
		var iter = streamAll(dataProvider).iterator();
		while (iter.hasNext()) {
			if (!Objects.equals(item, iter.next()))
				continue;
			if (iter.hasNext())
				return Optional.of(iter.next());
		}
		return Optional.empty();
	}

	public static <X> Optional<X> previous(DataProvider<X, ?> dataProvider, X item) {
		if (item == null)
			return Optional.empty();
		var iter = streamAll(dataProvider).iterator();
		X previous = null;
		while (iter.hasNext()) {
			var next = iter.next();
			if (!Objects.equals(item, next)) {
				previous = next;
				continue;
			}
			return Optional.ofNullable(previous);
		}
		return Optional.empty();
	}

	private static <X> StreamEx<X> sort(Iterable<? extends X> iterable, Function<? super X, byte[]> itemHashFunction,
			Cache<Long, Map<Long, Integer>> itemSetCache, Cache<Long, Integer> compareToCache, Query<X, ?> req) {
		var sorter = req.getInMemorySorting();
		if (sorter == null)
			return Streams.of(iterable);
		long itemSetHash = createItemSetHasher(itemHashFunction, iterable).hash().asLong();
		long key = createSortCacheKey(itemSetHash, req, false);
		Function<X, Long> hashFunction = v -> createItemSetHasher(itemHashFunction, v).hash().asLong();
		Map<Long, Integer> sortMap;
		boolean reverse;
		{
			var cacheValue = itemSetCache.getIfPresent(key);
			if (cacheValue == null) {
				cacheValue = itemSetCache.getIfPresent(createSortCacheKey(itemSetHash, req, true));
				if (cacheValue != null)
					reverse = true;
				else
					reverse = false;
			} else
				reverse = false;
			if (cacheValue == null)
				cacheValue = itemSetCache.get(key, nil -> {
					var fjp = CoreTasks.forkJoinPool();
					try {
						var estream = Streams.of(iterable)
								.parallel(fjp)
								.<Long>mapToEntry(hashFunction)
								.invert()
								.sorted((ent1, ent2) -> {
									var v1 = ent1.getValue();
									var v2 = ent2.getValue();
									long compareToCacheKey = Hashing.farmHashFingerprint64()
											.newHasher()
											.putLong(ent1.getKey())
											.putLong(ent2.getKey())
											.putLong(itemSetHash)
											.hash()
											.asLong();
									return compareToCache.get(compareToCacheKey, nil1 -> sorter.compare(v1, v2));
								});
						return estream.keys()
								.chain(EntryStreams.indexed())
								.mapValues(Number::intValue)
								.toImmutableMap();
					} finally {
						fjp.shutdownNow();
					}
				});
			sortMap = cacheValue;
		}
		StreamEx<X> stream = Streams.of(iterable);
		stream = stream.mapToEntry(hashFunction).invert().sortedBy(ent -> {
			var sortMapKey = ent.getKey();
			var index = sortMap.get(sortMapKey);
			if (index == null)
				return Integer.MAX_VALUE;
			if (reverse)
				index = index * -1;
			return index;
		}).values();
		return stream;

	}

	private static <X> Hasher createItemSetHasher(Function<? super X, byte[]> itemHashFunction,
			Iterable<? extends X> iterable) {
		var hasher = Hashing.farmHashFingerprint64().newHasher();
		Streams.of(iterable).forEach(v -> {
			byte[] itemHash = itemHashFunction == null ? null : itemHashFunction.apply(v);
			if (itemHash != null)
				hasher.putBytes(itemHash);
			else
				hasher.putInt(Objects.hash(v));
		});
		return hasher;
	}

	@SafeVarargs
	private static <X> Hasher createItemSetHasher(Function<? super X, byte[]> itemHashFunction, X... items) {
		return createItemSetHasher(itemHashFunction, Streams.of(items));
	}

	private static Long createSortCacheKey(long itemSetKey, Query<?, ?> req, boolean invert) {
		var hasher = Hashing.farmHashFingerprint64().newHasher();
		hasher.putLong(itemSetKey);
		streamSortOrders(req, invert).forEach(ent -> {
			hasher.putBytes(Utils.Crypto.hashMD5(ent.getKey()).array());
			hasher.putBytes(Utils.Crypto.hashMD5(ent.getValue()).array());
		});
		return hasher.hash().asLong();
	}

	private static EntryStream<String, SortDirection> streamSortOrders(Query<?, ?> req, boolean invert) {
		return Streams.ofNullable(req)
				.flatMap(v -> Streams.of(req.getSortOrders()))
				.nonNull()
				.mapToEntry(v -> v.getSorted(), v -> {
					var ascending = SortDirection.ASCENDING.equals(v.getDirection());
					if (invert)
						ascending = !ascending;
					return ascending ? SortDirection.ASCENDING : SortDirection.DESCENDING;
				})
				.nonNullKeys();

	}

}
