package com.lfp.vaadin.base.v7.container.sort;

import java.util.Collection;
import java.util.HashMap;

public class CachedSortMap extends HashMap<Long, Integer> {

	public static CachedSortMap create(Iterable<Long> keyIble) {
		return create(keyIble, null);
	}

	public static CachedSortMap create(Iterable<Long> keyIble, Integer initialCapacity) {
		if (keyIble == null)
			return new CachedSortMap(0);
		if (initialCapacity == null && (keyIble instanceof Collection))
			initialCapacity = ((Collection<Long>) keyIble).size();
		CachedSortMap result;
		if (initialCapacity != null)
			result = new CachedSortMap(initialCapacity);
		else
			result = new CachedSortMap();
		var iter = keyIble.iterator();
		for (int i = 0; iter.hasNext(); i++)
			result.put(iter.next(), i);
		return result;
	}

	public CachedSortMap() {
		super();
	}

	public CachedSortMap(int initialCapacity) {
		super(initialCapacity);
	}
}
