package com.lfp.vaadin.base.css;

import java.util.LinkedHashSet;
import java.util.Objects;

import com.lfp.joe.net.html.Htmls;
import com.lfp.joe.utils.Utils;
import com.vaadin.ui.Component;

import one.util.streamex.StreamEx;

public class CSSs {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static StreamEx<String> streamStyleNames(Component component) {
		Objects.requireNonNull(component);
		var styleName = component.getStyleName();
		if (Utils.Strings.isBlank(styleName))
			return StreamEx.empty();
		return com.lfp.joe.stream.Streams.of(styleName.split("\\s+")).filter(Utils.Strings::isNotBlank);
	}

	public static boolean addStyleName(Component component, String styleName) {
		styleName = Utils.Strings.trimToNull(styleName);
		if (styleName == null)
			return false;
		var stream = streamStyleNames(component);
		var set = stream.toCollection(LinkedHashSet::new);
		if (!set.add(styleName))
			return false;
		component.setStyleName(com.lfp.joe.stream.Streams.of(set).joining(" "));
		return true;
	}

	public static boolean removeStyleName(Component component, String styleName) {
		styleName = Utils.Strings.trimToNull(styleName);
		if (styleName == null)
			return false;
		var stream = streamStyleNames(component);
		stream = stream.distinct();
		var set = stream.toCollection(LinkedHashSet::new);
		if (!set.remove(styleName))
			return false;
		component.setStyleName(com.lfp.joe.stream.Streams.of(set).joining(" "));
		return true;
	}

	@SuppressWarnings("resource")
	public static boolean containsStyleName(Component component, String... styleNames) {
		if (styleNames == null)
			return false;
		for (var check : streamStyleNames(component)) {
			for (var styleName : styleNames) {
				if (Utils.Strings.equals(styleName, check))
					return true;
			}
		}
		return false;
	}

	public static Object createStyleName(Component component) {
		Objects.requireNonNull(component);
		addStyleName(component, Htmls.generateRandomKey("sn"));
		return null;
	}
}
