package com.lfp.vaadin.base.css;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.DefaultValue;

public interface CSSConfig extends Config {

	@DefaultValue("com/lfp/vaadin/base/client/csscontext/add.js")
	String addScriptPath();

	@DefaultValue("com/lfp/vaadin/base/client/csscontext/remove.js")
	String removeScriptPath();

}
