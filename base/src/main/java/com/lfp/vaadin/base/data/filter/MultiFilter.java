package com.lfp.vaadin.base.data.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import com.lfp.joe.core.function.Muto;
import com.lfp.joe.utils.function.Requires;
import com.lfp.vaadin.base.event.EventRouterLFP;
import com.lfp.vaadin.base.event.SourceEvent;
import com.lfp.vaadin.base.event.SourceEventListener;
import com.vaadin.data.HasValue;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.shared.Registration;

public interface MultiFilter<F> extends Consumer<Consumer<List<F>>> {

	int size();

	boolean clearFilters();

	default Registration addFilter(F filter) {
		Objects.requireNonNull(filter);
		var evt = update(Arrays.asList(filter), List.of());
		return evt;
	}

	default boolean removeFilter(F filter) {
		if (filter == null)
			return false;
		var evt = update(List.of(), Arrays.asList(filter));
		return evt.getRemoved().contains(filter);
	}

	ModificationEvent<F> update(Iterable<? extends F> addFilters, Iterable<? extends F> removeFilters);

	Registration addModificationListener(SourceEventListener<ModificationEvent<F>> listener);

	@Override
	default void accept(Consumer<List<F>> filterConsumer) {
		apply(filterConsumer == null ? null : list -> {
			filterConsumer.accept(list);
			return null;
		});
	}

	@SuppressWarnings("serial")
	default <V> Registration addGenerator(HasValue<V> hasValue, Function<ValueChangeEvent<V>, F> filterFunction) {
		Objects.requireNonNull(hasValue);
		Objects.requireNonNull(filterFunction);
		var listener = new ValueChangeListener<V>() {

			private F previousFilter;

			@Override
			public void valueChange(ValueChangeEvent<V> event) {
				var filter = filterFunction.apply(event);
				update(Arrays.asList(filter), Arrays.asList(previousFilter));
				this.previousFilter = filter;
			}
		};
		return hasValue.addValueChangeListener(listener);
	}

	<X> X apply(Function<List<F>, X> filterFunction);

	public static <F> MultiFilter<F> create() {
		return new MultiFilter.Impl<>();
	}

	public static class Impl<F> implements MultiFilter<F> {

		private final Muto<List<F>> filterListMuto = Muto.create();
		private final EventRouterLFP eventRouter = new EventRouterLFP();

		@Override
		public int size() {
			return Optional.ofNullable(filterListMuto.get()).map(Collection::size).orElse(0);
		}

		@Override
		public boolean clearFilters() {
			var cleared = filterListMuto.apply(list -> {
				list.clear();
				return true;
			}, list -> {
				return list != null && !list.isEmpty();
			});
			return Boolean.TRUE.equals(cleared);
		}

		@Override
		public Registration addModificationListener(SourceEventListener<ModificationEvent<F>> listener) {
			return eventRouter.addListener(ModificationEvent.class, listener);
		}

		@Override
		public <X> X apply(Function<List<F>, X> filterConsumer) {
			var resultOp = filterListMuto.apply(list -> {
				list = Collections.unmodifiableList(list);
				return Optional.ofNullable(filterConsumer.apply(list));
			}, list -> {
				return list != null && !list.isEmpty();
			});
			if (resultOp == null)
				resultOp = Optional.ofNullable(filterConsumer.apply(List.of()));
			return resultOp.orElse(null);
		}

		@Override
		public ModificationEvent<F> update(Iterable<? extends F> addFilters, Iterable<? extends F> removeFilters) {
			Function<Iterable<? extends F>, List<F>> toList = list -> com.lfp.joe.stream.Streams.of(list)
					.nonNull()
					.map(v -> (F) v)
					.toList();
			var addFilterList = toList.apply(addFilters);
			var removeFilterList = toList.apply(removeFilters);
			if (addFilterList.isEmpty() && removeFilterList.isEmpty())
				return new ModificationEvent<>(this, addFilterList, removeFilterList);
			var evt = filterListMuto.apply(list -> {
				{
					var iter = addFilterList.iterator();
					while (iter.hasNext()) {
						var filter = iter.next();
						if (list == null)
							list = new ArrayList<>();
						if (list.contains(filter) || !list.add(filter))
							iter.remove();
					}
				}
				if (list == null || list.isEmpty())
					removeFilterList.clear();
				else {
					var iter = removeFilterList.iterator();
					while (iter.hasNext()) {
						var filter = iter.next();
						if (!list.remove(filter))
							iter.remove();
					}
				}
				if (list.isEmpty())
					this.filterListMuto.set(null);
				else
					this.filterListMuto.set(list);
				return new ModificationEvent<>(this, Collections.unmodifiableList(addFilterList),
						Collections.unmodifiableList(removeFilterList));
			});
			this.eventRouter.fireEvent(evt);
			return evt;
		}

	}

	@SuppressWarnings("serial")
	public static class ModificationEvent<F> extends SourceEvent<MultiFilter<F>> implements Registration {

		private final List<F> added;
		private final List<F> removed;

		public ModificationEvent(MultiFilter<F> source, List<F> added, List<F> removed) {
			super(source);
			this.added = Objects.requireNonNull(added);
			this.removed = Objects.requireNonNull(removed);
		}

		public List<F> getAdded() {
			return added;
		}

		public List<F> getRemoved() {
			return removed;
		}

		public Registration getRegistration(F filter) {
			Objects.requireNonNull(filter);
			var match = com.lfp.joe.stream.Streams.of(getAdded()).filter(filter::equals).toList();
			Requires.isPositive(match.size(), fc -> fc.exceptionArguments("filter not found:%s", filter));
			return () -> match.forEach(this.getSource()::removeFilter);
		}

		@Override
		public void remove() {
			com.lfp.joe.stream.Streams.of(getAdded()).forEach(this.getSource()::removeFilter);
		}

	}

}
