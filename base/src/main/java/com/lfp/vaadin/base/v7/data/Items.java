package com.lfp.vaadin.base.v7.data;

import java.io.Serializable;
import java.util.Objects;

import com.lfp.joe.core.function.FNV;
import com.lfp.joe.utils.crypto.Hashable;

import at.favre.lib.bytes.Bytes;

public class Items {

	public static Bytes hashId(Object itemId) {
		Objects.requireNonNull(itemId);
		if (itemId instanceof Hashable)
			return ((Hashable) itemId).hash();
		else if (itemId instanceof Serializable)
			return Bytes.from(FNV.hash64((Serializable) itemId));
		return Bytes.from(itemId.hashCode());
	}

	public static void main(String[] args) {
		System.out.println(Serializable.class.isAssignableFrom(int.class));
	}
}
