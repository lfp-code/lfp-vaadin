package com.lfp.vaadin.base.data;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public class DataProviderList<E> extends AbstractList<E> implements Scrapable {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final Scrapable scrapable = Scrapable.create();
	private final Muto<BiMap<Integer, E>> loadMapMuto = Muto.create(HashBiMap.create());
	private final Muto<SorterContext<E>> sorterContextMuto = Muto.create(new SorterContext<>());
	private final Muto<FilterContext<E>> filterContextMuto = Muto.create(new FilterContext<>());
	private final AddProcessor<E> addProcessor = new AddProcessor<>(this);

	@Override
	public E get(int index) {
		E value;
		if (index < 0)
			value = null;
		else
			value = loadMapMuto.apply(loadMap -> {
				return sorterContextMuto.apply(sorterContext -> {
					return filterContextMuto.apply(filterContext -> {
						var sortMap = sorterContext.getUnchecked();
						var filterMap = filterContext.getUnchecked();
						if (filterMap.isEmpty() && sortMap.isEmpty())
							return loadMap.get(index);
						if (filterMap.isEmpty())
							return sortMap.get(index);
						return stream(loadMap, sorterContext, filterContext).values().skip(index).findFirst()
								.orElse(null);
					});
				});
			});
		if (value == null)
			throw new NoSuchElementException("no such element at index:" + index);
		return value;
	}

	@Override
	public int size() {
		Number count = loadMapMuto.apply(loadMap -> {
			return sorterContextMuto.apply(sorterContext -> {
				return filterContextMuto.apply(filterContext -> {
					var sortMap = sorterContext.getUnchecked();
					var filterMap = filterContext.getUnchecked();
					if (filterMap.isEmpty() && sortMap.isEmpty())
						return loadMap.size();
					if (filterMap.isEmpty())
						return sortMap.size();
					return stream(loadMap, sorterContext, filterContext).count();
				});
			});
		});
		return count.intValue();
	}

	public int sizeAll() {
		return loadMapMuto.apply(Map::size);
	}

	@Override
	public int indexOf(Object o) {
		if (o == null)
			return -1;
		Integer index = loadMapMuto.apply(loadMap -> {
			return sorterContextMuto.apply(sorterContext -> {
				return filterContextMuto.apply(filterContext -> {
					if (sorterContext.getUnchecked().isEmpty() && filterContext.getUnchecked().isEmpty())
						return loadMap.inverse().get(o);
					return stream(loadMap, sorterContext, filterContext).filterValues(o::equals).keys().findFirst()
							.orElse(null);
				});
			});
		});
		if (index == null)
			return -1;
		return index;
	}

	public int loadIndexOf(Object o) {
		if (o == null)
			return -1;
		return loadMapMuto.apply(loadMap -> {
			return loadMap.inverse().getOrDefault(o, -1);
		});
	}

	@Override
	public boolean add(E e) {
		return addAll(Arrays.asList(e));
	}

	@Override
	public boolean addAll(Collection<? extends E> coll) {
		return Threads.Futures.join(addAllAsync(coll));
	}

	public ListenableFuture<Boolean> addAllAsync(Collection<? extends E> coll) {
		return addProcessor.addAllAsync(coll);
	}

	public void sort(Sorter<? super E> sorter) {
		Utils.Functions.unchecked(() -> sortAsync(sorter).get());
	}

	public ListenableFuture<Void> sortAsync(Sorter<? super E> sorter) {
		if (sorter == null)
			return sortAsync(Sorter.empty());
		Function<BiMap<Integer, E>, SorterContext<E>> sortAsyncFunction = loadMap -> {
			var newContext = new SorterContext<E>(loadMap.values(), sorter);
			return sorterContextMuto.updateAndGet(currentContext -> {
				currentContext.scrap();
				return newContext;
			}, currentContext -> {
				return !newContext.equals(currentContext);
			});
		};
		return loadMapMuto.apply(sortAsyncFunction).getCompeleteListenableFuture();
	}

	public ListenableFuture<Void> filterAsync(Filter<? super E> filter) {
		if (filter == null)
			return filterAsync(Filter.empty());
		Function<BiMap<Integer, E>, FilterContext<E>> sortAsyncFunction = loadMap -> {
			var newContext = new FilterContext<E>(loadMap.values(), filter);
			return filterContextMuto.updateAndGet(currentContext -> {
				currentContext.scrap();
				return newContext;
			}, currentContext -> {
				return !newContext.equals(currentContext);
			});
		};
		return loadMapMuto.apply(sortAsyncFunction).getCompeleteListenableFuture();
	}

	public boolean isFiltered() {
		return !this.filterContextMuto.get().getUnchecked().isEmpty();
	}

	public boolean isSorted() {
		return !this.sorterContextMuto.get().getUnchecked().isEmpty();
	}

	@Override
	public boolean isScrapped() {
		return scrapable.isScrapped();
	}

	@Override
	public Scrapable onScrap(Runnable task) {
		return scrapable.onScrap(task);
	}

	@Override
	public boolean scrap() {
		return scrapable.scrap();
	}

	protected static <E> EntryStream<Integer, E> stream(Map<Integer, E> indexToElement, SorterContext<E> sorterContext,
			FilterContext<E> filterContext) {
		if (indexToElement == null || indexToElement.isEmpty())
			return EntryStream.empty();
		var estream = IntStreamEx.range(Integer.MAX_VALUE).mapToObj(i -> {
			Map<Integer, E> map = sorterContext == null ? null : sorterContext.getUnchecked();
			if (map == null || map.isEmpty())
				map = indexToElement;
			E value = map.get(i);
			return value == null ? null : Map.entry(i, value);
		}).takeWhile(Objects::nonNull).mapToEntry(Entry::getKey, Entry::getValue);
		var filterMap = filterContext == null ? null : filterContext.getUnchecked();
		if (filterMap == null || filterMap.isEmpty())
			return estream;
		return estream.filter(ent -> {
			var future = filterMap.get(ent.getValue());
			if (future == null)
				return true;
			return Boolean.TRUE.equals(Utils.Functions.unchecked(() -> future.get()));
		});
	}

	public static interface Filter<E> extends Function<List<? extends E>, Map<E, ListenableFuture<Boolean>>>, Hashable {

		int batchSize();

		public static final Filter<Object> EMPTY = new Filter<Object>() {

			private final Bytes hash = Utils.Crypto.hashMD5("$EMPTY$", Filter.class);

			@Override
			public Map<Object, ListenableFuture<Boolean>> apply(List<? extends Object> ible) {
				return Map.of();
			}

			@Override
			public Bytes hash() {
				return hash;
			}

			@Override
			public int batchSize() {
				return 1;
			}

		};

		@SuppressWarnings("unchecked")
		public static <E> Filter<E> empty() {
			return (Filter<E>) EMPTY;
		}
	}

	public static interface Sorter<E>
			extends Function<StreamEx<? extends E>, ListenableFuture<Map<Integer, E>>>, Hashable {

		public static final Sorter<Object> EMPTY = new Sorter<Object>() {

			private final Bytes hash = Utils.Crypto.hashMD5("$EMPTY$", Sorter.class);

			@Override
			public ListenableFuture<Map<Integer, Object>> apply(StreamEx<? extends Object> t) {
				return FutureUtils.immediateResultFuture(Map.of());
			}

			@Override
			public Bytes hash() {
				return hash;
			}

		};

		@SuppressWarnings("unchecked")
		public static <E> Sorter<E> empty() {
			return (Sorter<E>) EMPTY;
		}
	}

	private static abstract class AbstractContext<E, S extends Hashable, K, V> extends Scrapable.Impl {
		private static final Bytes EMPTY_HASH = Utils.Crypto.hashMD5("$EMPTY$", AbstractContext.class);

		private final MemoizedSupplier<ListenableFuture<Map<K, V>>> futureSupplier = Utils.Functions.memoize(() -> {
			var future = Optional.ofNullable(createListenableFuture())
					.orElseGet(() -> FutureUtils.immediateResultFuture(null));
			future = future.listener(() -> onListenableFutureComplete());
			Threads.Futures.onScrapCancel(this, future, true);
			return future.map(v -> v == null ? Map.of() : v);
		});
		private final Set<E> itemSet = Collections.newSetFromMap(new WeakHashMap<E, Boolean>());
		private final S service;
		private final Bytes serviceHash;

		protected AbstractContext(Collection<? extends E> collection, S service) {
			super();
			com.lfp.joe.stream.Streams.of(collection).nonNull().forEach(itemSet::add);
			this.service = service;
			this.serviceHash = service == null ? EMPTY_HASH : Utils.Crypto.hashMD5(service.getClass(), service.hash());
			this.onScrap(() -> itemSet.clear());
		}

		@Override
		public int hashCode() {
			return Objects.hash(itemSet, serviceHash);
		}

		@SuppressWarnings("rawtypes")
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AbstractContext other = (AbstractContext) obj;
			return Objects.equals(serviceHash, other.serviceHash) && Objects.equals(itemSet, other.itemSet);
		}

		public boolean equalService(AbstractContext<E, ?, ?, ?> context) {
			if (this == context)
				return true;
			if (context == null)
				return false;
			return serviceHash.equals(context.serviceHash);
		}

		public S getService() {
			return service;
		}

		public ListenableFuture<Map<K, V>> getListenableFuture() {
			return futureSupplier.get();
		}

		public ListenableFuture<Void> getCompeleteListenableFuture() {
			return getListenableFuture().map(v -> null);
		}

		public Map<K, V> getUnchecked() {
			return Threads.Futures.join(getListenableFuture());
		}

		protected abstract ListenableFuture<Map<K, V>> createListenableFuture();

		protected abstract void onListenableFutureComplete();

	}

	private static class FilterContext<E> extends AbstractContext<E, Filter<E>, E, ListenableFuture<Boolean>> {

		private Collection<? extends E> collection;

		public FilterContext() {
			this(null, null);
		}

		@SuppressWarnings("unchecked")
		public FilterContext(Collection<? extends E> collection, Filter<? super E> filter) {
			super(collection, (Filter<E>) filter);
			this.collection = collection;
			this.onScrap(() -> {
				this.collection = null;
			});
		}

		@Override
		protected ListenableFuture<Map<E, ListenableFuture<Boolean>>> createListenableFuture() {
			if (this.isScrapped())
				return null;
			var filter = this.getService();
			if (filter == null)
				return null;
			StreamEx<? extends E> stream = com.lfp.joe.stream.Streams.of(collection).nonNull().distinct();
			StreamEx<List<? extends E>> bufferStream = stream.chain(Utils.Lots.buffer(filter.batchSize(), null))
					.map(v -> (List<? extends E>) v);
			Map<E, ListenableFuture<Boolean>> map = new HashMap<>();
			Runnable loader = () -> {
				for (var list : bufferStream) {
					var filterMap = filter.apply(list);
					for (var ent : filterMap.entrySet()) {
						var key = ent.getKey();
						var future = ent.getValue();
						map.put(key, future);
						Threads.Futures.onScrapCancel(this, future, true);
						future.resultCallback(v -> {
							if (Boolean.TRUE.equals(v))
								map.remove(key);
						});
					}
				}
			};
			ListenableFuture<?> future = Threads.Pools.centralPool().submit(loader);
			return future.map(nil -> map);
		}

		@Override
		protected void onListenableFutureComplete() {
			this.collection = null;
		}

	}

	private static class SorterContext<E> extends AbstractContext<E, Sorter<E>, Integer, E> {

		private Collection<? extends E> collection;

		public SorterContext() {
			this(null, null);
		}

		@SuppressWarnings("unchecked")
		public SorterContext(Collection<? extends E> collection, Sorter<? super E> sorter) {
			super(collection, (Sorter<E>) sorter);
			this.collection = collection;
			this.onScrap(() -> {
				this.collection = null;
			});
		}

		@Override
		protected ListenableFuture<Map<Integer, E>> createListenableFuture() {
			var sorter = this.getService();
			if (sorter == null)
				return FutureUtils.immediateResultFuture(null);
			var stream = com.lfp.joe.stream.Streams.of(collection).nonNull().distinct().map(v -> v);
			var future = sorter.apply(stream);
			return future;
		}

		@Override
		protected void onListenableFutureComplete() {
			this.collection = null;
		}

	}

	private static class AddProcessor<E> extends Scrapable.Impl {

		private final Muto<ListenableFuture<?>> processListenableFutureMuto = Muto.create();
		private final Map<SettableListenableFuture<Boolean>, List<E>> queue = new LinkedHashMap<>();
		private final DataProviderList<E> dataProviderList;

		public AddProcessor(DataProviderList<E> dataProviderList) {
			this.dataProviderList = dataProviderList;
			this.dataProviderList.onScrap(this);
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public ListenableFuture<Boolean> addAllAsync(Collection<? extends E> values) {
			if (this.isScrapped())
				return FutureUtils.immediateResultFuture(false);
			List<E> valueList = com.lfp.joe.stream.Streams.of(values).nonNull().distinct().map(v -> (E) v).toList();
			if (valueList.isEmpty())
				return FutureUtils.immediateResultFuture(false);
			if (MachineConfig.isDeveloper())
				logger.info("async add requested. item count:{}", valueList.size());
			SettableListenableFuture<Boolean> result = new SettableListenableFuture<>(false);
			var registration = this.onScrap(() -> result.setResult(false));
			result.listener(registration::scrap);
			processListenableFutureMuto.updateAndGet(processListenableFuture -> {
				queue.put(result, valueList);
				if (processListenableFuture == null || processListenableFuture.isDone())
					processListenableFuture = process();
				return processListenableFuture;
			});
			return result;
		}

		private ListenableFuture<?> process() {
			ListenableFuture<?> processListenableFuture = Threads.Pools.centralPool().submit(() -> processAsync());
			Threads.Futures.onScrapCancel(this, processListenableFuture, true);
			return processListenableFuture;
		}

		private void processAsync() {
			while (!Thread.currentThread().isInterrupted()) {
				var nextEntry = processListenableFutureMuto.apply(nil -> {
					var entryIterator = queue.entrySet().iterator();
					var entry = entryIterator.hasNext() ? entryIterator.next() : null;
					if (entry != null) {
						entryIterator.remove();
						return entry;
					}
					processListenableFutureMuto.set(null);
					return null;
				});
				if (nextEntry == null)
					return;
				var nextListenableFuture = nextEntry.getKey();
				try {
					nextListenableFuture.setResult(processSync(nextEntry.getValue()));
				} catch (Throwable t) {
					nextListenableFuture.setFailure(t);
				}
			}
		}

		private boolean processSync(List<E> valueList) throws InterruptedException, ExecutionException {
			var successListenableFuture = dataProviderList.loadMapMuto.apply(loadMap -> {
				var count = 0;
				for (var value : valueList) {
					var valueToIndex = loadMap.inverse();
					var previous = valueToIndex.putIfAbsent(value, valueToIndex.size());
					if (previous == null)
						count++;
				}
				if (count <= 0)
					return FutureUtils.immediateResultFuture(false);
				if (MachineConfig.isDeveloper())
					logger.info("sync add started. item count:{}", count);
				var sortContext = new SorterContext<>(loadMap.values(),
						dataProviderList.sorterContextMuto.get().getService());
				var sortUpdateListenableFuture = processOnComplete(sortContext, dataProviderList.sorterContextMuto);
				var filterContext = new FilterContext<>(loadMap.values(),
						dataProviderList.filterContextMuto.get().getService());
				var filterUpdateListenableFuture = processOnComplete(filterContext, dataProviderList.filterContextMuto);
				return FutureUtils
						.makeCompleteListFuture(Arrays.asList(sortUpdateListenableFuture, filterUpdateListenableFuture))
						.map(results -> results.contains(true));
			});
			return successListenableFuture.get();
		}

		private <C extends AbstractContext<E, ?, ?, ?>> ListenableFuture<Boolean> processOnComplete(C context,
				Muto<C> contextMuto) {
			return context.getListenableFuture().map(map -> {
				contextMuto.update(nil -> {
					return context;
				}, currentContext -> {
					if (currentContext.equalService(context))
						return true;
					return false;
				});
				return true;

			});

		}
	}

}
