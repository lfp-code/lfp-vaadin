package com.lfp.vaadin.base.client.viewportscalelock;

import org.aeonbits.owner.Config;

public interface ViewportScaleLockConfig extends Config {

	@DefaultValue("com/lfp/vaadin/base/client/viewportscalelock/script.js")
	String scriptPath();

	public static void main(String[] args) {
		System.out.println("var el = document.createElement('meta');\r\n" + "el.setAttribute('name', 'viewport');\r\n"
				+ "el.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0');\r\n"
				+ "document.getElementsByTagName('head')[0].appendChild(el);\r\n");
	}
}
