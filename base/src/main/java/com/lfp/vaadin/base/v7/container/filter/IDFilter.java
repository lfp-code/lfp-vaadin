package com.lfp.vaadin.base.v7.container.filter;

import java.util.Objects;

import com.vaadin.v7.data.Container.Filter;
import com.vaadin.v7.data.Item;

@SuppressWarnings({ "deprecation", "serial" })
public interface IDFilter extends Filter {

	Object getId();

	public static IDFilter create(Object filterId, Filter filter) {
		return new Wrapper(filterId, filter);
	}

	public static class Wrapper implements IDFilter {

		private final Object filterId;
		private final Filter delegate;

		public Wrapper(Object filterId, Filter delegate) {
			super();
			this.filterId = Objects.requireNonNull(filterId);
			this.delegate = Objects.requireNonNull(delegate);
		}

		@Override
		public Object getId() {
			return filterId;
		}

		@Override
		public boolean passesFilter(Object itemId, Item item) throws UnsupportedOperationException {
			return delegate.passesFilter(itemId, item);
		}

		@Override
		public boolean appliesToProperty(Object propertyId) {
			return delegate.appliesToProperty(propertyId);
		}

	}
}
