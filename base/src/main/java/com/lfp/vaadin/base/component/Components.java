package com.lfp.vaadin.base.component;

import java.util.EventObject;

import com.lfp.joe.reflections.reflection.ReflectionMethodCache;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.client.Connectors;

import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.TextField;

public class Components {

	public static void setRequiredIndicatorVisible(AbstractComponent component, boolean requiredIndicatorVisible) {
		ReflectionMethodCache.getDefault(AbstractComponent.class).invokeVoid("setRequiredIndicatorVisible", component,
				requiredIndicatorVisible);
	}

	public static boolean isRequiredIndicatorVisible(AbstractComponent component) {
		return ReflectionMethodCache.getDefault(AbstractComponent.class).invoke(boolean.class,
				"isRequiredIndicatorVisible", component);
	}

	public static void setReadOnly(AbstractComponent component, boolean readOnly) {
		ReflectionMethodCache.getDefault(AbstractComponent.class).invokeVoid("setReadOnly", component, readOnly);
	}

	public static boolean isReadOnly(AbstractComponent component) {
		return ReflectionMethodCache.getDefault(AbstractComponent.class).invoke(boolean.class, "isReadOnly", component);
	}



	public static String getCaption(String... input) {
		var stream = com.lfp.joe.stream.Streams.of(input);
		stream = stream.nonNull();
		stream = stream.flatMap(v -> {
			var split = Utils.Strings.splitByCharacterTypeCamelCase(v);
			return com.lfp.joe.stream.Streams.of(split);
		});
		stream = stream.flatMap(v -> {
			var split = v.split("\\s+");
			return com.lfp.joe.stream.Streams.of(split);
		});
		stream = stream.filter(Utils.Strings::isNotBlank);
		stream = stream.map(v -> Utils.Strings.capitalize(v));
		return stream.joining(" ");
	}

	public static void main(String[] args) {
		var tf = new TextField();
		System.out.println(Components.isReadOnly(tf));
		Components.setReadOnly(tf, true);
		System.out.println(Components.isReadOnly(tf));
		System.out.println(Components.isRequiredIndicatorVisible(tf));
		Components.setRequiredIndicatorVisible(tf, true);
		System.out.println(Components.isRequiredIndicatorVisible(tf));
		Connectors.fireEvent(tf, new EventObject(tf));
	}

}
