package com.lfp.vaadin.base.converter;

import com.lfp.joe.utils.Utils;
import com.vaadin.data.Converter;
import com.vaadin.data.Result;

public class Converters {

	public static Converter<String, String> trimToEmpty() {
		return Converter.from(v -> {
			v = Utils.Strings.trimToEmpty(v);
			return Result.ok(v);
		}, v -> v);
	}
}
