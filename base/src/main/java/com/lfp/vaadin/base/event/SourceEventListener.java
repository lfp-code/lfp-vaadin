package com.lfp.vaadin.base.event;

import java.lang.reflect.Method;
import java.util.function.Consumer;

import com.lfp.joe.core.function.Throws;
import com.vaadin.event.SerializableEventListener;

public interface SourceEventListener<E extends SourceEvent<?>> extends Consumer<E>, SerializableEventListener {

	public static final Method LISTENER_METHOD = Throws
			.unchecked(() -> SourceEventListener.class.getMethod("accept", SourceEvent.class));

	@Override
	void accept(E sourceEvent);

}
