package com.lfp.vaadin.base.client.viewportscalelock;

import java.util.function.Function;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.client.Connectors;
import com.lfp.vaadin.base.javascript.JavaScripts;
import com.lfp.vaadin.base.ui.UIX;

import com.vaadin.ui.Component;

public class ViewportScaleLock implements Function<UIX, Scrapable> {

	public static ViewportScaleLock INSTANCE = new ViewportScaleLock();

	@Override
	public Scrapable apply(UIX input) {
		return Connectors.addStateChangeListener(input, evt -> {
			if (evt.isAttached())
				setup(input);
		}, ops -> ops.fireIfAttached(true));
	}

	protected void setup(Component input) {
		var script = Utils.Resources.getResourceAsString(Configs.get(ViewportScaleLockConfig.class).scriptPath()).get();
		JavaScripts.execute(script);
	}

}
