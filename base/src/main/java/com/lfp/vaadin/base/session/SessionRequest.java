package com.lfp.vaadin.base.session;

import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.net.cookie.Cookie;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.stream.Streams;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;

import one.util.streamex.StreamEx;

public class SessionRequest {

	public static SessionRequest getCurrent() {
		return getCurrent(false);
	}

	public static SessionRequest getCurrent(boolean disableUpdate) {
		var sessionRequest = SessionAttributes.getCurrent().computeIfAbsent(SessionRequest.class, SessionRequest::new);
		if (!disableUpdate) {
			var currentRequest = VaadinService.getCurrentRequest();
			if (currentRequest != null)
				sessionRequest.update(currentRequest);
		}
		return sessionRequest;
	}

	private VaadinRequest vaadinRequest;
	private HeaderMap _headerMap;

	public void update(VaadinRequest vaadinRequest) {
		this.vaadinRequest = vaadinRequest;
		this._headerMap = null;
	}

	public VaadinRequest getVaadinRequest() {
		return this.vaadinRequest;
	}

	public HeaderMap getHeaderMap() {
		if (this.vaadinRequest == null)
			return HeaderMap.of();
		if (this._headerMap != null)
			return this._headerMap;
		var estream = Streams.of(this.vaadinRequest.getHeaderNames())
				.mapToEntry(this.vaadinRequest::getHeaders)
				.flatMapToValue((k, v) -> Streams.of(v));
		var headerMap = HeaderMap.of(estream);
		this._headerMap = headerMap;
		return headerMap;
	}

	public StreamEx<Cookie> streamCookies() {
		if (this.vaadinRequest == null)
			return StreamEx.empty();
		return com.lfp.joe.stream.Streams.of(vaadinRequest.getCookies()).map(v -> toCookie(v));
	}

	public StreamEx<javax.servlet.http.Cookie> streamServletCookies() {
		if (this.vaadinRequest == null)
			return StreamEx.empty();
		return com.lfp.joe.stream.Streams.of(vaadinRequest.getCookies());
	}

	private static Cookie toCookie(javax.servlet.http.Cookie cookie) {
		var cb = Cookie.builder();
		var metaPropertyMap = Cookie.meta().metaPropertyMap();
		var bps = BeanRef.$(javax.servlet.http.Cookie.class).all();
		for (var bp : bps) {
			var mp = metaPropertyMap.get(bp.getPath());
			if (mp == null)
				continue;
			if (Cookie.meta().maxAge().equals(mp))
				continue;
			var value = bp.get(cookie);
			if (value != null)
				cb.set(mp, value);
		}
		cb.maxAge(cookie.getMaxAge());
		return cb.build();
	}

}
