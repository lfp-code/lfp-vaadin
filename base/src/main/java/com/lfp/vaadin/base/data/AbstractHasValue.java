package com.lfp.vaadin.base.data;

import java.util.Collections;
import java.util.Iterator;
import java.util.function.Function;

import com.lfp.joe.utils.Utils;

import com.google.common.base.Objects;
import com.vaadin.data.HasValue;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Component;

import one.util.streamex.StreamEx;

@SuppressWarnings("serial")
public abstract class AbstractHasValue<V> implements HasValueLFP<V> {

	@Override
	public void setValue(V value, boolean userOriginated) {
		var oldValue = this.getValue();
		if (equals(oldValue, value))
			return;
		var setValueChangeEvent = new ValueChangeEvent<V>(getComponent(), new HasValue<V>() {

			@Override
			public V getValue() {
				return value;
			}

			@Override
			public void setValue(V value) {
				AbstractHasValue.this.setValue(value);
			}

			@Override
			public Registration addValueChangeListener(ValueChangeListener<V> listener) {
				return AbstractHasValue.this.addValueChangeListener(listener);
			}

			@Override
			public void setRequiredIndicatorVisible(boolean requiredIndicatorVisible) {
				AbstractHasValue.this.setRequiredIndicatorVisible(requiredIndicatorVisible);
			}

			@Override
			public boolean isRequiredIndicatorVisible() {
				return AbstractHasValue.this.isRequiredIndicatorVisible();
			}

			@Override
			public void setReadOnly(boolean readOnly) {
				AbstractHasValue.this.setReadOnly(readOnly);
			}

			@Override
			public boolean isReadOnly() {
				return AbstractHasValue.this.isReadOnly();
			}
		}, oldValue, userOriginated);
		setValueInternal(setValueChangeEvent);
		var valueChangeEvent = new ValueChangeEvent<V>(getComponent(), this, oldValue, userOriginated);
		this.fireEvent(valueChangeEvent);
	}

	protected boolean equals(V v1, V v2) {
		Function<V, Iterator<?>> toIter = v -> {
			var stream = Utils.Lots.tryStream(v).orElse(null);
			if (stream == null) {
				if (v == null)
					return Collections.emptyIterator();
				else
					return StreamEx.of(v).iterator();
			}
			return stream.iterator();
		};

		Iterator<?> iter1 = toIter.apply(v1);
		Iterator<?> iter2 = toIter.apply(v2);
		while (iter1.hasNext() && iter2.hasNext()) {
			var next1 = iter1.next();
			var next2 = iter2.next();
			if (!Objects.equal(next1, next2))
				return false;
		}
		return iter1.hasNext() == iter2.hasNext();
	}

	protected abstract void setValueInternal(ValueChangeEvent<V> valueChangeEvent);

	protected abstract void fireEvent(ValueChangeEvent<V> valueChangeEvent);

	protected abstract Component getComponent();

}
