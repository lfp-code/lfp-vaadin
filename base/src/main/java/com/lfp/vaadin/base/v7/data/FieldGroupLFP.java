package com.lfp.vaadin.base.v7.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.ReadWriteAccessor;

import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.fieldgroup.FieldGroup;
import com.vaadin.v7.data.fieldgroup.FieldGroupFieldFactory;
import com.vaadin.v7.ui.DefaultFieldFactory;
import com.vaadin.v7.ui.Field;
import com.vaadin.v7.ui.Grid;

@SuppressWarnings({ "deprecation", "rawtypes", "serial" })
public class FieldGroupLFP extends FieldGroup {

	public static FieldGroupLFP configure(Grid grid) {
		var fieldGroup = grid.getEditorFieldGroup();
		if (!(fieldGroup instanceof FieldGroupLFP)) {
			fieldGroup = create(fieldGroup);
			grid.setEditorFieldGroup(fieldGroup);
		}
		return (FieldGroupLFP) fieldGroup;
	}

	public static FieldGroupLFP create(FieldGroup fieldGroup) {
		if (fieldGroup instanceof FieldGroupLFP)
			return (FieldGroupLFP) fieldGroup;
		return new FieldGroupLFP(fieldGroup);
	}

	private final ReadWriteAccessor<List<FieldBuildInterceptor>> fieldBuilders = new ReadWriteAccessor<>(
			new ArrayList<>());
	private final FieldGroup delegate;

	protected FieldGroupLFP(FieldGroup delegate) {
		super();
		this.delegate = delegate;
	}

	public Scrapable addFieldBuildInterceptor(FieldBuildInterceptor fieldBuildInterceptor) {
		return addFieldBuildInterceptor(fieldBuildInterceptor, false);
	}

	public Scrapable addFieldBuildInterceptorFirst(FieldBuildInterceptor fieldBuildInterceptor) {
		return addFieldBuildInterceptor(fieldBuildInterceptor, true);
	}

	protected Scrapable addFieldBuildInterceptor(FieldBuildInterceptor fieldBuildInterceptor, boolean first) {
		Objects.requireNonNull(fieldBuildInterceptor);
		fieldBuilders.writeAccess(fbiList -> {
			if (first)
				fbiList.add(0, fieldBuildInterceptor);
			else
				fbiList.add(fieldBuildInterceptor);
		});
		return Scrapable.create(() -> {
			fieldBuilders.writeAccess(fbiList -> {
				fbiList.remove(fieldBuildInterceptor);
			});
		});
	}

	@Override
	public Field<?> buildAndBind(Object propertyId) throws BindException {
		String caption = DefaultFieldFactory.createCaptionByPropertyId(propertyId);
		return buildAndBind(caption, propertyId);
	}

	@Override
	public Field<?> buildAndBind(String caption, Object propertyId) throws BindException {
		return buildAndBind(caption, propertyId, Field.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends Field> T buildAndBind(String caption, Object propertyId, Class<T> fieldType)
			throws BindException {
		Class<?> propertyType = getPropertyType(propertyId);
		var context = new FieldBuildContext() {

			private Optional<Field> fieldOp;

			@Override
			public Field getField() {
				if (fieldOp == null)
					synchronized (this) {
						if (fieldOp == null)
							fieldOp = Optional.ofNullable(build(caption, propertyType, fieldType));
					}
				return fieldOp.orElse(null);
			}

			@Override
			public void setField(Field field) {
				fieldOp = Optional.ofNullable(field);
			};

			@Override
			public String getCaption() {
				return caption;
			}

			@Override
			public Object getPropertyId() {
				return propertyId;
			}

			@Override
			public Class<?> getPropertyType() {
				return propertyType;
			}
		};
		var field = this.fieldBuilders.readAccess(fbiList -> {
			for (var fbi : fbiList) {
				fbi.intercept(context);
				if (context.getField() == null)
					return null;
			}
			return context.getField();
		});
		if (field != null)
			bind(field, propertyId);
		return (T) field;
	}

	public static interface FieldBuildInterceptor {

		void intercept(FieldBuildContext context);

	}

	public static interface FieldBuildContext {

		String getCaption();

		Object getPropertyId();

		Class<?> getPropertyType();

		void setField(Field field);

		Field getField();

		default <F extends Field> Optional<F> getField(Class<F> fieldType) {
			if (fieldType == null)
				return Optional.empty();
			return Utils.Types.tryCast(getField(), fieldType);
		}

		default <F extends Field> Optional<F> getField(Class<F> fieldType, Object filterPropertyId) {
			if (!Objects.equals(filterPropertyId, this.getPropertyId()))
				return Optional.empty();
			return getField(fieldType);
		}

	}

	// ****DELEGATES****

	@Override
	public int hashCode() {
		return delegate.hashCode();
	}

	@Override
	public void setItemDataSource(Item itemDataSource) {
		delegate.setItemDataSource(itemDataSource);
	}

	@Override
	public Item getItemDataSource() {
		return delegate.getItemDataSource();
	}

	@Override
	public boolean isBuffered() {
		return delegate.isBuffered();
	}

	@Override
	public void setBuffered(boolean buffered) {
		delegate.setBuffered(buffered);
	}

	@Override
	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}

	@Override
	public boolean isEnabled() {
		return delegate.isEnabled();
	}

	@Override
	public void setEnabled(boolean fieldsEnabled) {
		delegate.setEnabled(fieldsEnabled);
	}

	@Override
	public boolean isReadOnly() {
		return delegate.isReadOnly();
	}

	@Override
	public void setReadOnly(boolean fieldsReadOnly) {
		delegate.setReadOnly(fieldsReadOnly);
	}

	@Override
	public Collection<Field<?>> getFields() {
		return delegate.getFields();
	}

	@Override
	public void bind(Field<?> field, Object propertyId) throws BindException {
		delegate.bind(field, propertyId);
	}

	@Override
	public String toString() {
		return delegate.toString();
	}

	@Override
	public void unbind(Field<?> field) throws BindException {
		delegate.unbind(field);
	}

	@Override
	public Collection<Object> getBoundPropertyIds() {
		return delegate.getBoundPropertyIds();
	}

	@Override
	public Collection<Object> getUnboundPropertyIds() {
		return delegate.getUnboundPropertyIds();
	}

	@Override
	public void commit() throws CommitException {
		delegate.commit();
	}

	@Override
	public void discard() {
		delegate.discard();
	}

	@Override
	public Field<?> getField(Object propertyId) {
		return delegate.getField(propertyId);
	}

	@Override
	public Object getPropertyId(Field<?> field) {
		return delegate.getPropertyId(field);
	}

	@Override
	public void addCommitHandler(CommitHandler commitHandler) {
		delegate.addCommitHandler(commitHandler);
	}

	@Override
	public void removeCommitHandler(CommitHandler commitHandler) {
		delegate.removeCommitHandler(commitHandler);
	}

	@Override
	public boolean isValid() {
		return delegate.isValid();
	}

	@Override
	public boolean isModified() {
		return delegate.isModified();
	}

	@Override
	public FieldGroupFieldFactory getFieldFactory() {
		return delegate.getFieldFactory();
	}

	@Override
	public void setFieldFactory(FieldGroupFieldFactory fieldFactory) {
		delegate.setFieldFactory(fieldFactory);
	}

	@Override
	public void bindMemberFields(Object objectWithMemberFields) throws BindException {
		delegate.bindMemberFields(objectWithMemberFields);
	}

	@Override
	public void buildAndBindMemberFields(Object objectWithMemberFields) throws BindException {
		delegate.buildAndBindMemberFields(objectWithMemberFields);
	}

	@Override
	public void clear() {
		delegate.clear();
	}

}
