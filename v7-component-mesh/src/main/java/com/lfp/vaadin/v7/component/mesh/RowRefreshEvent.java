package com.lfp.vaadin.v7.component.mesh;

import java.lang.reflect.Method;
import java.util.EventObject;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.v7.data.BeanItemLFP;

import com.vaadin.event.SerializableEventListener;

import one.util.streamex.EntryStream;

public class RowRefreshEvent<ID, T> extends EventObject {
	private static final long serialVersionUID = -5011112028369095758L;

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static final String EVENT_ID = THIS_CLASS.getName();
	public static final Method METHOD = Utils.Functions.unchecked(() -> {
		return Consumer.class.getDeclaredMethod("accept", Object.class);
	});

	public static interface Listener<ID, T> extends SerializableEventListener, Consumer<RowRefreshEvent<ID, T>> {
	}

	private final Set<Object> itemIds;

	public RowRefreshEvent(Mesh<ID, T> mesh) {
		super(mesh);
		this.itemIds = null;
	}

	public RowRefreshEvent(Mesh<ID, T> mesh, Object... itemIds) {
		super(mesh);
		this.itemIds = Utils.Lots.stream(itemIds).nonNull().toCollection(LinkedHashSet::new);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Mesh<ID, T> getSource() {
		return (Mesh<ID, T>) super.getSource();
	}

	@SuppressWarnings("unchecked")
	public EntryStream<ID, BeanItemLFP<T>> streamRefreshedRows() {
		Mesh<ID, T> mesh = (Mesh<ID, T>) this.getSource();
		if (this.itemIds == null)
			return mesh.getContainerDataSourceExt().streamAll();
		var estream = Utils.Lots.stream(this.itemIds).mapToEntry(v -> {
			return mesh.getContainerDataSourceExt().getItem(v);
		});
		estream = estream.nonNullValues();
		return estream.mapKeys(v -> (ID) v);
	}
}
