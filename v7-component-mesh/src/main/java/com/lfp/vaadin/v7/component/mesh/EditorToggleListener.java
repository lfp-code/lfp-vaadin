package com.lfp.vaadin.v7.component.mesh;

import com.vaadin.event.SerializableEventListener;
import com.vaadin.ui.Component;

public interface EditorToggleListener<ID, BT> extends SerializableEventListener {

	void editorToggle(EditorToggleEvent<ID, BT> event);

	@SuppressWarnings("serial")
	public static class EditorToggleEvent<ID, BT> extends Component.Event {

		private final boolean enabled;

		public EditorToggleEvent(Mesh<ID, BT> source, boolean enabled) {
			super(source);
			this.enabled = enabled;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Mesh<ID, BT> getSource() {
			return (Mesh<ID, BT>) super.getSource();
		}

		public boolean isEnabled() {
			return enabled;
		}

	}

}
