package com.lfp.vaadin.v7.component.mesh;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Spliterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.log.LogFilter;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.ReadWriteAccessor;
import com.lfp.vaadin.base.client.Connectors;
import com.lfp.vaadin.base.client.shiftuserselectdisable.ShiftUserSelectDisable;
import com.lfp.vaadin.base.v7.container.ContainerLFP;
import com.lfp.vaadin.base.v7.container.events.GetItemIdsEvent;
import com.lfp.vaadin.base.v7.grid.GridV7s;
import com.lfp.vaadin.base.v7.property.PropertiesV7;
import com.lfp.vaadin.base.v7.property.PropertyDescriptorLFP;

import org.apache.commons.lang3.Validate;
import org.jsoup.nodes.Element;
import org.vaadin.teemusav7.gridextensions.client.tableselection.TableSelectionState.TableSelectionMode;
import org.vaadin.teemusav7.gridextensions.tableselection.TableSelectionModel;

import com.vaadin.event.ContextClickEvent.ContextClickListener;
import com.vaadin.event.EventRouter;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.event.SerializableEventListener;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.ClientMethodInvocation;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.ErrorMessage;
import com.vaadin.server.Extension;
import com.vaadin.server.Resource;
import com.vaadin.server.ServerRpcManager;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.shared.Registration;
import com.vaadin.shared.communication.SharedState;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.UI;
import com.vaadin.ui.declarative.DesignContext;
import com.vaadin.v7.data.Container.Indexed;
import com.vaadin.v7.data.fieldgroup.FieldGroup;
import com.vaadin.v7.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.v7.data.fieldgroup.FieldGroupFieldFactory;
import com.vaadin.v7.data.sort.Sort;
import com.vaadin.v7.data.sort.SortOrder;
import com.vaadin.v7.event.ItemClickEvent.ItemClickListener;
import com.vaadin.v7.event.SelectionEvent.SelectionListener;
import com.vaadin.v7.event.SortEvent.SortListener;
import com.vaadin.v7.shared.ui.grid.ColumnResizeMode;
import com.vaadin.v7.shared.ui.grid.HeightMode;
import com.vaadin.v7.shared.ui.grid.ScrollDestination;
import com.vaadin.v7.ui.Grid;
import com.vaadin.v7.ui.renderers.Renderer;

import ch.qos.logback.core.spi.FilterReply;
import de.datenhahn.vaadin.componentrenderer.ComponentCellKeyExtension;
import de.datenhahn.vaadin.componentrenderer.DetailsKeysExtension;
import de.datenhahn.vaadin.componentrenderer.FocusPreserveExtension;
import elemental.json.JsonObject;
import one.util.streamex.StreamEx;

@SuppressWarnings({ "unchecked", "deprecation", "serial" })
public class Mesh<ID, T> extends Grid {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final MemoizedSupplier<Void> LOG_INIT = Utils.Functions.memoize(() -> {
		LogFilter.addFilter(ctx -> {
			var loggerName = ctx.getEvent().getLoggerName();
			if (!EventRouter.class.getName().equals(loggerName))
				return FilterReply.NEUTRAL;
			String msg = ctx.getEvent().getMessage();
			if (!Utils.Strings.containsIgnoreCase(msg, "Adding listeners with type Object is deprecated"))
				return FilterReply.NEUTRAL;
			return FilterReply.DENY;
		});
	});

	private final Map<Object, Integer> columnSortPriority = new ConcurrentHashMap<>();
	private final ReadWriteAccessor<List<Function<GetItemIdsEvent<ID, T>, List<ID>>>> getItemIdsInterceptors = new ReadWriteAccessor<>(
			new ArrayList<>());
	private final ComponentCellKeyExtension componentCellKeyExtension;
	private final FocusPreserveExtension focusPreserveExtension;
	private final DetailsKeysExtension detailsKeysExtension;
	private final TableSelectionModel tableSelectionModel;
	private final Nada constructComplete;
	private TableSelectionMode tableSelectionMode;
	private boolean configureColumnSortPriorityRunning;

	public Mesh(ContainerLFP<ID, T> container) {
		super(container);
		LOG_INIT.get();
		this.componentCellKeyExtension = ComponentCellKeyExtension.extend(this);
		this.focusPreserveExtension = FocusPreserveExtension.extend(this);
		this.detailsKeysExtension = DetailsKeysExtension.extend(this);
		this.tableSelectionModel = new TableSelectionModel();
		this.setSelectionModel(tableSelectionModel);
		this.setTableSelectionMode(null);
		this.setRowStyleGenerator(row -> {
			var itemId = row.getItemId();
			return GridV7s.getRowClassName(itemId);
		});
		this.setCellStyleGenerator(cell -> {
			var itemId = cell.getItemId();
			var propertyId = cell.getPropertyId();
			return GridV7s.getCellClassName(itemId, propertyId);
		});
		ShiftUserSelectDisable.INSTANCE.apply(this);
		this.constructComplete = Nada.get();
		this.addColumnReorderListener(evt -> configureColumnSortPriority());
		this.getContainerDataSourceExt().addPropertySetChangeListener(evt -> configureColumnSortPriority());
		GetItemIdsEvent.Listener<ID, T> getItemIdsListener = evt -> {
			if (GetItemIdsEvent.Action.DIRECT.equals(evt.getAction()))
				return;
			getItemIdsInterceptors.readAccess(v -> {
				var interceptedEvent = evt;
				for (var interceptor : v) {
					var itemIds = interceptor.apply(interceptedEvent);
					interceptedEvent = interceptedEvent.withItemIds(itemIds);
				}
			});
		};
		Connectors.addAttachListener(this, evt -> {
			this.getContainerDataSourceExt().addGetItemIdsListener(getItemIdsListener);
		});
		Connectors.addDetachListener(this, evt -> {
			this.getContainerDataSourceExt().removeGetItemIdsListener(getItemIdsListener);
		});
	}

	@Override
	public void refreshRows(Object... itemIds) {
		super.refreshRows(itemIds);
		this.fireEvent(new RowRefreshEvent<ID, T>(this, itemIds));
	}

	@Override
	public void refreshAllRows() {
		super.refreshAllRows();
		this.fireEvent(new RowRefreshEvent<ID, T>(this));
	}

	public void addRowRefreshListener(RowRefreshEvent.Listener<ID, T> listener) {
		this.addListener(RowRefreshEvent.EVENT_ID, RowRefreshEvent.class, listener, RowRefreshEvent.METHOD);
	}

	public void removeRowRefreshListener(RowRefreshEvent.Listener<ID, T> listener) {
		this.removeListener(RowRefreshEvent.EVENT_ID, RowRefreshEvent.class, listener);
	}

	public <MODEL> Column addColumn(Class<MODEL> modelType, Function<T, MODEL> modelFunction) {
		return addColumn("$gen_" + Utils.Crypto.getSecureRandomString(), modelType, modelFunction);
	}

	@Override
	public Column addColumn(Object propertyId) throws IllegalStateException {
		if (this.getContainerDataSource().getContainerPropertyIds().contains(propertyId))
			return super.addColumn(propertyId);
		return this.addColumn(Utils.Types.tryCast(propertyId, String.class).orElse(null),
				this.getContainerDataSourceExt().getBeanType(), b -> b);
	}

	public <PRESENTATION> Column addColumn(Renderer<PRESENTATION> renderer,
			Function<T, PRESENTATION> presentationFunction) {
		return addColumn("$gen_" + Utils.Crypto.getSecureRandomString(), renderer, presentationFunction);
	}

	public <MODEL> Column addColumn(String propertyId, Class<MODEL> modelType, Function<T, MODEL> modelFunction) {
		Validate.noNullElements(Arrays.asList(propertyId, modelFunction, modelType));
		PropertyDescriptorLFP<T, MODEL> pd = PropertiesV7.createDescriptor(propertyId, modelType, modelFunction);
		this.getContainerDataSourceExt().addContainerProperty(pd);
		return super.getColumn(pd.getName());
	}

	public <PRESENTATION> Column addColumn(String propertyId, Renderer<PRESENTATION> renderer,
			Function<T, PRESENTATION> presentationFunction) {
		Validate.noNullElements(Arrays.asList(presentationFunction, renderer, propertyId));
		return this.addColumn(propertyId, renderer.getPresentationType(), presentationFunction).setRenderer(renderer);
	}

	public ComponentCellKeyExtension getComponentCellKeyExtension() {
		return componentCellKeyExtension;
	}

	public ContainerLFP<ID, T> getContainerDataSourceExt() {
		var containerDataSource = this.getContainerDataSource();
		return (ContainerLFP<ID, T>) containerDataSource;
	}

	public DetailsKeysExtension getDetailsKeysExtension() {
		return detailsKeysExtension;
	}

	public FocusPreserveExtension getFocusPreserveExtension() {
		return focusPreserveExtension;
	}

	public TableSelectionMode getTableSelectionMode() {
		return this.tableSelectionMode;
	}

	public TableSelectionModel getTableSelectionModel() {
		return tableSelectionModel;
	}

	public HeaderRow getHeaderRow(int rowIndex, boolean create) {
		while (create && super.getHeaderRowCount() <= rowIndex)
			super.addHeaderRowAt(super.getHeaderRowCount());
		return super.getHeaderRow(rowIndex);
	}

	public void setColumnSortable(Column column, boolean sortable) {
		Objects.requireNonNull(column);
		this.getContainerDataSourceExt().setPropertyIdSortable(column.getPropertyId(), sortable);
		column.setSortable(sortable);
	}

	public void setColumnSortable(Object propertyId, boolean sortable) {
		setColumnSortable(getColumn(propertyId), sortable);
	}

	@Deprecated
	@Override
	public void setContainerDataSource(Indexed container) {
		if (constructComplete == null) {
			super.setContainerDataSource(container);
			return;
		}
		throw new UnsupportedOperationException("container can't be modified");
	}

	public void setTableSelectionMode(TableSelectionMode tableSelectionMode) {
		if (tableSelectionMode == null)
			tableSelectionMode = TableSelectionMode.SIMPLE;
		this.tableSelectionMode = tableSelectionMode;
		this.tableSelectionModel.setMode(tableSelectionMode);
	}

	public void setColumnPriority(Column column, Integer priority) {
		setColumnPriority(column == null ? null : column.getPropertyId(), priority);
	}

	public void setColumnPriority(Object propertyId, Integer priority) {
		Objects.requireNonNull(propertyId);
		this.columnSortPriority.compute(propertyId, (k, v) -> priority);
		configureColumnSortPriority();
	}

	public Scrapable addGetItemIdsInterceptor(Function<GetItemIdsEvent<ID, T>, List<ID>> interceptor) {
		Objects.requireNonNull(interceptor);
		this.getItemIdsInterceptors.writeAccess(v -> {
			v.add(interceptor);
		});
		return Scrapable.create(() -> {
			this.getItemIdsInterceptors.writeAccess(v -> {
				v.removeIf(vv -> interceptor == vv);
			});
		});
	}

	private void configureColumnSortPriority() {
		if (this.columnSortPriority.isEmpty())
			return;
		if (this.configureColumnSortPriorityRunning)
			return;
		Supplier<StreamEx<Object>> columnIdsStreamer = () -> Utils.Lots.stream(this.getColumns())
				.map(v -> v.getPropertyId()).nonNull().distinct();
		List<Object> sortedColumnIds;
		{
			var stream = columnIdsStreamer.get();
			stream = stream.filter(this.columnSortPriority::containsKey).sortedBy(v -> this.columnSortPriority.get(v));
			stream = stream.append(columnIdsStreamer.get());
			stream = stream.distinct();
			sortedColumnIds = stream.toList();
		}
		var currentColumnIds = columnIdsStreamer.get().toList();
		if (sortedColumnIds.equals(currentColumnIds))
			return;
		this.configureColumnSortPriorityRunning = true;
		try {
			this.setColumnOrder(sortedColumnIds.toArray(Object[]::new));
		} finally {
			this.configureColumnSortPriorityRunning = false;
		}
	}

	// ****DELEGATES****

	@Override
	public void forEach(Consumer<? super Component> action) {
		super.forEach(action);
	}

	@Override
	public Spliterator<Component> spliterator() {
		return super.spliterator();
	}

	@Override
	public Registration addAttachListener(AttachListener listener) {
		return super.addAttachListener(listener);
	}

	@Override
	public void removeAttachListener(AttachListener listener) {
		super.removeAttachListener(listener);
	}

	@Override
	public void setId(String id) {
		super.setId(id);
	}

	@Override
	public Registration addDetachListener(DetachListener listener) {
		return super.addDetachListener(listener);
	}

	@Override
	public String getId() {
		return super.getId();
	}

	@Override
	public void removeDetachListener(DetachListener listener) {
		super.removeDetachListener(listener);
	}

	@Override
	public void setDebugId(String id) {
		super.setDebugId(id);
	}

	@Override
	public void requestRepaint() {
		super.requestRepaint();
	}

	@Override
	public String getDebugId() {
		return super.getDebugId();
	}

	@Override
	public String getStyleName() {
		return super.getStyleName();
	}

	@Override
	public void setStyleName(String style, boolean add) {
		super.setStyleName(style, add);
	}

	@Override
	public void markAsDirty() {
		super.markAsDirty();
	}

	@Override
	public void setStyleName(String style) {
		super.setStyleName(style);
	}

	@Override
	public void setPrimaryStyleName(String style) {
		super.setPrimaryStyleName(style);
	}

	@Override
	public String getPrimaryStyleName() {
		return super.getPrimaryStyleName();
	}

	@Override
	public void addStyleName(String style) {
		super.addStyleName(style);
	}

	@Override
	public void removeStyleName(String style) {
		super.removeStyleName(style);
	}

	@Override
	public void addStyleNames(String... styles) {
		super.addStyleNames(styles);
	}

	@Override
	public String getCaption() {
		return super.getCaption();
	}

	@Override
	public void setCaption(String caption) {
		super.setCaption(caption);
	}

	@Override
	public void setCaptionAsHtml(boolean captionAsHtml) {
		super.setCaptionAsHtml(captionAsHtml);
	}

	@Override
	public void removeStyleNames(String... styles) {
		super.removeStyleNames(styles);
	}

	@Override
	public boolean isCaptionAsHtml() {
		return super.isCaptionAsHtml();
	}

	@Override
	public Locale getLocale() {
		return super.getLocale();
	}

	@Override
	public void setLocale(Locale locale) {
		super.setLocale(locale);
	}

	@Override
	public Resource getIcon() {
		return super.getIcon();
	}

	@Override
	public JsonObject encodeState() {
		return super.encodeState();
	}

	@Override
	public String toString() {
		return super.toString();
	}

	@Override
	public void setIcon(Resource icon) {
		super.setIcon(icon);
	}

	@Override
	public boolean isEnabled() {
		return super.isEnabled();
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
	}

	@Override
	public boolean isConnectorEnabled() {
		return super.isConnectorEnabled();
	}

	@Override
	public Class<? extends SharedState> getStateType() {
		return super.getStateType();
	}

	@Override
	public boolean isVisible() {
		return super.isVisible();
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
	}

	@Override
	public String getDescription() {
		return super.getDescription();
	}

	@Override
	public void setDescription(String description) {
		super.setDescription(description);
	}

	@Override
	public void setDescription(String description, ContentMode mode) {
		super.setDescription(description, mode);
	}

	@Override
	public HasComponents getParent() {
		return super.getParent();
	}

	@Override
	public void setParent(HasComponents parent) {
		super.setParent(parent);
	}

	@Override
	public <T extends HasComponents> T findAncestor(Class<T> parentType) {
		return super.findAncestor(parentType);
	}

	@Override
	public ServerRpcManager<?> getRpcManager(String rpcInterfaceName) {
		return super.getRpcManager(rpcInterfaceName);
	}

	@Override
	public List<ClientMethodInvocation> retrievePendingRpcCalls() {
		return super.retrievePendingRpcCalls();
	}

	@Override
	public String getConnectorId() {
		return super.getConnectorId();
	}

	@Override
	public ErrorMessage getErrorMessage() {
		return super.getErrorMessage();
	}

	@Override
	public ErrorMessage getComponentError() {
		return super.getComponentError();
	}

	@Override
	public UI getUI() {
		return super.getUI();
	}

	@Override
	public void setComponentError(ErrorMessage componentError) {
		super.setComponentError(componentError);
	}

	@Override
	public void requestRepaintAll() {
		super.requestRepaintAll();
	}

	@Override
	public void attach() {
		super.attach();
	}

	@Override
	public void markAsDirtyRecursive() {
		super.markAsDirtyRecursive();
	}

	@Override
	public void detach() {
		super.detach();
	}

	@Override
	public Collection<Extension> getExtensions() {
		return super.getExtensions();
	}

	@Override
	public void removeExtension(Extension extension) {
		super.removeExtension(extension);
	}

	@Override
	public Registration addListener(Listener listener) {
		return super.addListener(listener);
	}

	@Override
	public boolean isAttached() {
		return super.isAttached();
	}

	@Override
	public void removeListener(Listener listener) {
		super.removeListener(listener);
	}

	@Override
	public void setData(Object data) {
		super.setData(data);
	}

	@Override
	public Object getData() {
		return super.getData();
	}

	@Override
	public boolean handleConnectorRequest(VaadinRequest request, VaadinResponse response, String path)
			throws IOException {
		return super.handleConnectorRequest(request, response, path);
	}

	@Override
	public float getHeight() {
		return super.getHeight();
	}

	@Override
	public Unit getHeightUnits() {
		return super.getHeightUnits();
	}

	@Override
	public float getWidth() {
		return super.getWidth();
	}

	@Override
	public Unit getWidthUnits() {
		return super.getWidthUnits();
	}

	@Override
	public void setSizeFull() {
		super.setSizeFull();
	}

	@Override
	public void setWidthFull() {
		super.setWidthFull();
	}

	@Override
	public void setHeightFull() {
		super.setHeightFull();
	}

	@Override
	public void setSizeUndefined() {
		super.setSizeUndefined();
	}

	@Override
	public void setWidthUndefined() {
		super.setWidthUndefined();
	}

	@Override
	public void setHeightUndefined() {
		super.setHeightUndefined();
	}

	@Override
	public void setWidth(float width, Unit unit) {
		super.setWidth(width, unit);
	}

	@Override
	public void setWidth(String width) {
		super.setWidth(width);
	}

	@Override
	public void setHeight(String height) {
		super.setHeight(height);
	}

	@Override
	public void setResponsive(boolean responsive) {
		super.setResponsive(responsive);
	}

	@Override
	public boolean isResponsive() {
		return super.isResponsive();
	}

	@Override
	public Registration addListener(Class<?> eventType, Object target, Method method) {
		return super.addListener(eventType, target, method);
	}

	@Override
	public Registration addListener(Class<?> eventType, SerializableEventListener listener, Method method) {
		return super.addListener(eventType, listener, method);
	}

	@Override
	public Registration addListener(Class<?> eventType, Object target, String methodName) {
		return super.addListener(eventType, target, methodName);
	}

	@Override
	public Registration addListener(Class<?> eventType, SerializableEventListener listener, String methodName) {
		return super.addListener(eventType, listener, methodName);
	}

	@Override
	public void removeListener(Class<?> eventType, Object target) {
		super.removeListener(eventType, target);
	}

	@Override
	public void removeListener(Class<?> eventType, SerializableEventListener listener) {
		super.removeListener(eventType, listener);
	}

	@Override
	public void removeListener(Class<?> eventType, Object target, Method method) {
		super.removeListener(eventType, target, method);
	}

	@Override
	public void removeListener(Class<?> eventType, Object target, String methodName) {
		super.removeListener(eventType, target, methodName);
	}

	@Override
	public Registration addShortcutListener(ShortcutListener shortcut) {
		return super.addShortcutListener(shortcut);
	}

	@Override
	public void removeShortcutListener(ShortcutListener shortcut) {
		super.removeShortcutListener(shortcut);
	}

	@Override
	public Collection<?> getListeners(Class<?> eventType) {
		return super.getListeners(eventType);
	}

	@Override
	public Registration addContextClickListener(ContextClickListener listener) {
		return super.addContextClickListener(listener);
	}

	@Override
	public ErrorHandler getErrorHandler() {
		return super.getErrorHandler();
	}

	@Override
	public void setErrorHandler(ErrorHandler errorHandler) {
		super.setErrorHandler(errorHandler);
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public void removeContextClickListener(ContextClickListener listener) {
		super.removeContextClickListener(listener);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public void beforeClientResponse(boolean initial) {
		super.beforeClientResponse(initial);
	}

	@Override
	public Indexed getContainerDataSource() {
		return super.getContainerDataSource();
	}

	@Override
	public Column getColumn(Object propertyId) {
		return super.getColumn(propertyId);
	}

	@Override
	public List<Column> getColumns() {
		return super.getColumns();
	}

	@Override
	public Column addColumn(Object propertyId, Class<?> type) {
		return super.addColumn(propertyId, type);
	}

	@Override
	public void removeAllColumns() {
		super.removeAllColumns();
	}

	@Override
	public boolean isColumnReorderingAllowed() {
		return super.isColumnReorderingAllowed();
	}

	@Override
	public void setColumnReorderingAllowed(boolean columnReorderingAllowed) {
		super.setColumnReorderingAllowed(columnReorderingAllowed);
	}

	@Override
	public void setColumnResizeMode(ColumnResizeMode mode) {
		super.setColumnResizeMode(mode);
	}

	@Override
	public ColumnResizeMode getColumnResizeMode() {
		return super.getColumnResizeMode();
	}

	@Override
	public void removeColumn(Object propertyId) throws IllegalArgumentException {
		super.removeColumn(propertyId);
	}

	@Override
	public void setColumns(Object... propertyIds) {
		super.setColumns(propertyIds);
	}

	@Override
	public void setColumnOrder(Object... propertyIds) {
		super.setColumnOrder(propertyIds);
	}

	@Override
	public void setFrozenColumnCount(int numberOfColumns) {
		super.setFrozenColumnCount(numberOfColumns);
	}

	@Override
	public int getFrozenColumnCount() {
		return super.getFrozenColumnCount();
	}

	@Override
	public void scrollTo(Object itemId) throws IllegalArgumentException {
		super.scrollTo(itemId);
	}

	@Override
	public void scrollTo(Object itemId, ScrollDestination destination) throws IllegalArgumentException {
		super.scrollTo(itemId, destination);
	}

	@Override
	public void scrollToStart() {
		super.scrollToStart();
	}

	@Override
	public void scrollToEnd() {
		super.scrollToEnd();
	}

	@Override
	public void setHeightByRows(double rows) {
		super.setHeightByRows(rows);
	}

	@Override
	public double getHeightByRows() {
		return super.getHeightByRows();
	}

	@Override
	public void setHeight(float height, Unit unit) {
		super.setHeight(height, unit);
	}

	@Override
	public void setHeightMode(HeightMode heightMode) {
		super.setHeightMode(heightMode);
	}

	@Override
	public HeightMode getHeightMode() {
		return super.getHeightMode();
	}

	@Override
	public void setSelectionModel(SelectionModel selectionModel) throws IllegalArgumentException {
		super.setSelectionModel(selectionModel);
	}

	@Override
	public SelectionModel getSelectionModel() {
		return super.getSelectionModel();
	}

	@Override
	public SelectionModel setSelectionMode(SelectionMode selectionMode) throws IllegalArgumentException {
		return super.setSelectionMode(selectionMode);
	}

	@Override
	public boolean isSelected(Object itemId) {
		return super.isSelected(itemId);
	}

	@Override
	public Collection<Object> getSelectedRows() {
		return super.getSelectedRows();
	}

	@Override
	public Object getSelectedRow() throws IllegalStateException {
		return super.getSelectedRow();
	}

	@Override
	public boolean select(Object itemId) throws IllegalArgumentException, IllegalStateException {
		return super.select(itemId);
	}

	@Override
	public boolean deselect(Object itemId) throws IllegalStateException {
		return super.deselect(itemId);
	}

	@Override
	public boolean deselectAll() throws IllegalStateException {
		return super.deselectAll();
	}

	@Override
	public void fireSelectionEvent(Collection<Object> oldSelection, Collection<Object> newSelection) {
		super.fireSelectionEvent(oldSelection, newSelection);
	}

	@Override
	public void addSelectionListener(SelectionListener listener) {
		super.addSelectionListener(listener);
	}

	@Override
	public void removeSelectionListener(SelectionListener listener) {
		super.removeSelectionListener(listener);
	}

	@Override
	public void addColumnReorderListener(ColumnReorderListener listener) {
		super.addColumnReorderListener(listener);
	}

	@Override
	public void removeColumnReorderListener(ColumnReorderListener listener) {
		super.removeColumnReorderListener(listener);
	}

	@Override
	public void addColumnResizeListener(ColumnResizeListener listener) {
		super.addColumnResizeListener(listener);
	}

	@Override
	public void removeColumnResizeListener(ColumnResizeListener listener) {
		super.removeColumnResizeListener(listener);
	}

	@Override
	public void sort(Sort s) {
		super.sort(s);
	}

	@Override
	public void sort(Object propertyId) {
		super.sort(propertyId);
	}

	@Override
	public void sort(Object propertyId, SortDirection direction) {
		super.sort(propertyId, direction);
	}

	@Override
	public void clearSortOrder() {
		super.clearSortOrder();
	}

	@Override
	public void setSortOrder(List<SortOrder> order) {
		super.setSortOrder(order);
	}

	@Override
	public List<SortOrder> getSortOrder() {
		return super.getSortOrder();
	}

	@Override
	public Registration addSortListener(SortListener listener) {
		return super.addSortListener(listener);
	}

	@Override
	public void removeSortListener(SortListener listener) {
		super.removeSortListener(listener);
	}

	@Override
	public HeaderRow getHeaderRow(int rowIndex) {
		return super.getHeaderRow(rowIndex);
	}

	@Override
	public HeaderRow addHeaderRowAt(int index) {
		return super.addHeaderRowAt(index);
	}

	@Override
	public HeaderRow appendHeaderRow() {
		return super.appendHeaderRow();
	}

	@Override
	public HeaderRow getDefaultHeaderRow() {
		return super.getDefaultHeaderRow();
	}

	@Override
	public int getHeaderRowCount() {
		return super.getHeaderRowCount();
	}

	@Override
	public HeaderRow prependHeaderRow() {
		return super.prependHeaderRow();
	}

	@Override
	public void removeHeaderRow(HeaderRow row) {
		super.removeHeaderRow(row);
	}

	@Override
	public void removeHeaderRow(int rowIndex) {
		super.removeHeaderRow(rowIndex);
	}

	@Override
	public void setDefaultHeaderRow(HeaderRow row) {
		super.setDefaultHeaderRow(row);
	}

	@Override
	public void setHeaderVisible(boolean visible) {
		super.setHeaderVisible(visible);
	}

	@Override
	public boolean isHeaderVisible() {
		return super.isHeaderVisible();
	}

	@Override
	public FooterRow getFooterRow(int rowIndex) {
		return super.getFooterRow(rowIndex);
	}

	@Override
	public FooterRow addFooterRowAt(int index) {
		return super.addFooterRowAt(index);
	}

	@Override
	public FooterRow appendFooterRow() {
		return super.appendFooterRow();
	}

	@Override
	public int getFooterRowCount() {
		return super.getFooterRowCount();
	}

	@Override
	public FooterRow prependFooterRow() {
		return super.prependFooterRow();
	}

	@Override
	public void removeFooterRow(FooterRow row) {
		super.removeFooterRow(row);
	}

	@Override
	public void removeFooterRow(int rowIndex) {
		super.removeFooterRow(rowIndex);
	}

	@Override
	public void setFooterVisible(boolean visible) {
		super.setFooterVisible(visible);
	}

	@Override
	public boolean isFooterVisible() {
		return super.isFooterVisible();
	}

	@Override
	public Iterator<Component> iterator() {
		return super.iterator();
	}

	@Override
	public boolean isRendered(Component childComponent) {
		return super.isRendered(childComponent);
	}

	@Override
	public void setCellDescriptionGenerator(CellDescriptionGenerator generator) {
		super.setCellDescriptionGenerator(generator);
	}

	@Override
	public void setCellDescriptionGenerator(CellDescriptionGenerator generator, ContentMode contentMode) {
		super.setCellDescriptionGenerator(generator, contentMode);
	}

	@Override
	public CellDescriptionGenerator getCellDescriptionGenerator() {
		return super.getCellDescriptionGenerator();
	}

	@Override
	public ContentMode getCellDescriptionContentMode() {
		return super.getCellDescriptionContentMode();
	}

	@Override
	public void setRowDescriptionGenerator(RowDescriptionGenerator generator) {
		super.setRowDescriptionGenerator(generator);
	}

	@Override
	public void setRowDescriptionGenerator(RowDescriptionGenerator generator, ContentMode contentMode) {
		super.setRowDescriptionGenerator(generator, contentMode);
	}

	@Override
	public ContentMode getRowDescriptionContentMode() {
		return super.getRowDescriptionContentMode();
	}

	@Override
	public RowDescriptionGenerator getRowDescriptionGenerator() {
		return super.getRowDescriptionGenerator();
	}

	@Override
	public void setCellStyleGenerator(CellStyleGenerator cellStyleGenerator) {
		super.setCellStyleGenerator(cellStyleGenerator);
	}

	@Override
	public CellStyleGenerator getCellStyleGenerator() {
		return super.getCellStyleGenerator();
	}

	@Override
	public void setRowStyleGenerator(RowStyleGenerator rowStyleGenerator) {
		super.setRowStyleGenerator(rowStyleGenerator);
	}

	@Override
	public RowStyleGenerator getRowStyleGenerator() {
		return super.getRowStyleGenerator();
	}

	@Override
	public Object addRow(Object... values) {
		return super.addRow(values);
	}

	@Override
	public void setEditorEnabled(boolean isEnabled) throws IllegalStateException {
		super.setEditorEnabled(isEnabled);
	}

	@Override
	public boolean isEditorEnabled() {
		return super.isEditorEnabled();
	}

	@Override
	public Object getEditedItemId() {
		return super.getEditedItemId();
	}

	@Override
	public FieldGroup getEditorFieldGroup() {
		return super.getEditorFieldGroup();
	}

	@Override
	public void setEditorFieldGroup(FieldGroup fieldGroup) {
		super.setEditorFieldGroup(fieldGroup);
	}

	@Override
	public boolean isEditorActive() {
		return super.isEditorActive();
	}

	@Override
	public void editItem(Object itemId) throws IllegalStateException, IllegalArgumentException {
		super.editItem(itemId);
	}

	@Override
	public void saveEditor() throws CommitException {
		super.saveEditor();
	}

	@Override
	public void cancelEditor() {
		super.cancelEditor();
	}

	@Override
	public void setEditorFieldFactory(FieldGroupFieldFactory fieldFactory) {
		super.setEditorFieldFactory(fieldFactory);
	}

	@Override
	public void setEditorErrorHandler(EditorErrorHandler editorErrorHandler) throws IllegalArgumentException {
		super.setEditorErrorHandler(editorErrorHandler);
	}

	@Override
	public EditorErrorHandler getEditorErrorHandler() {
		return super.getEditorErrorHandler();
	}

	@Override
	public FieldGroupFieldFactory getEditorFieldFactory() {
		return super.getEditorFieldFactory();
	}

	@Override
	public void setEditorSaveCaption(String saveCaption) throws IllegalArgumentException {
		super.setEditorSaveCaption(saveCaption);
	}

	@Override
	public String getEditorSaveCaption() {
		return super.getEditorSaveCaption();
	}

	@Override
	public void setEditorCancelCaption(String cancelCaption) throws IllegalArgumentException {
		super.setEditorCancelCaption(cancelCaption);
	}

	@Override
	public String getEditorCancelCaption() {
		return super.getEditorCancelCaption();
	}

	@Override
	public void setEditorBuffered(boolean editorBuffered) throws IllegalStateException {
		super.setEditorBuffered(editorBuffered);
	}

	@Override
	public boolean isEditorBuffered() {
		return super.isEditorBuffered();
	}

	@Override
	public void addItemClickListener(ItemClickListener listener) {
		super.addItemClickListener(listener);
	}

	@Override
	public void addListener(ItemClickListener listener) {
		super.addListener(listener);
	}

	@Override
	public void removeItemClickListener(ItemClickListener listener) {
		super.removeItemClickListener(listener);
	}

	@Override
	public void removeListener(ItemClickListener listener) {
		super.removeListener(listener);
	}

	@Override
	public void recalculateColumnWidths() {
		super.recalculateColumnWidths();
	}

	@Override
	public void addColumnVisibilityChangeListener(ColumnVisibilityChangeListener listener) {
		super.addColumnVisibilityChangeListener(listener);
	}

	@Override
	public void removeColumnVisibilityChangeListener(ColumnVisibilityChangeListener listener) {
		super.removeColumnVisibilityChangeListener(listener);
	}

	@Override
	public void setDetailsGenerator(DetailsGenerator detailsGenerator) throws IllegalArgumentException {
		super.setDetailsGenerator(detailsGenerator);
	}

	@Override
	public DetailsGenerator getDetailsGenerator() {
		return super.getDetailsGenerator();
	}

	@Override
	public void setDetailsVisible(Object itemId, boolean visible) {
		super.setDetailsVisible(itemId, visible);
	}

	@Override
	public boolean isDetailsVisible(Object itemId) {
		return super.isDetailsVisible(itemId);
	}

	@Override
	public void readDesign(Element design, DesignContext context) {
		super.readDesign(design, context);
	}

	@Override
	public void writeDesign(Element design, DesignContext context) {
		super.writeDesign(design, context);
	}

	@Override
	public void addBlurListener(BlurListener listener) {
		super.addBlurListener(listener);
	}

	@Override
	public void removeBlurListener(BlurListener listener) {
		super.removeBlurListener(listener);
	}

	@Override
	public void addFocusListener(FocusListener listener) {
		super.addFocusListener(listener);
	}

	@Override
	public void removeFocusListener(FocusListener listener) {
		super.removeFocusListener(listener);
	}

	@Override
	public void focus() {
		super.focus();
	}

	@Override
	public int getTabIndex() {
		return super.getTabIndex();
	}

	@Override
	public void setTabIndex(int tabIndex) {
		super.setTabIndex(tabIndex);
	}

}
