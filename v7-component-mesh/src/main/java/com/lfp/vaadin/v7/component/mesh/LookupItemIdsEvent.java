package com.lfp.vaadin.v7.component.mesh;

import java.lang.reflect.Method;
import java.util.EventObject;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.v7.container.ContainerLFP;

import com.vaadin.event.SerializableEventListener;

public class LookupItemIdsEvent<ID> extends EventObject {

	private static final long serialVersionUID = -6921339612898285989L;

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static final String EVENT_ID = THIS_CLASS.getName();
	public static final Method METHOD = Utils.Functions.unchecked(() -> {
		return Consumer.class.getDeclaredMethod("accept", Object.class);
	});

	public static interface Listener<ID> extends SerializableEventListener, Consumer<LookupItemIdsEvent<ID>> {
	}

	public static enum Action {
		DIRECT, RPC, FILTER, SORT;
	}

	private final Action action;
	private final List<ID> itemIds;

	public LookupItemIdsEvent(ContainerLFP<ID, ?> container, Action action, Iterable<ID> itemIds) {
		super(container);
		this.action = Objects.requireNonNull(action);
		this.itemIds = Utils.Lots.stream(itemIds).nonNull().distinct().toImmutableList();
	}

	public Action getAction() {
		return this.action;
	}

	public List<ID> getItemIds() {
		return this.itemIds;
	}
}
