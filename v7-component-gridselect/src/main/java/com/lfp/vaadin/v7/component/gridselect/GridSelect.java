package com.lfp.vaadin.v7.component.gridselect;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.client.Connectors;
import com.lfp.vaadin.base.css.CSSs;
import com.lfp.vaadin.base.javascript.JavaScripts;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.v7.event.ItemClickEvent;
import com.vaadin.v7.event.ItemClickEvent.ItemClickListener;
import com.vaadin.v7.event.SelectionEvent;
import com.vaadin.v7.event.SelectionEvent.SelectionListener;
import com.vaadin.v7.ui.Grid;
import com.vaadin.v7.ui.Grid.ColumnResizeEvent;
import com.vaadin.v7.ui.Grid.ColumnResizeListener;

@SuppressWarnings({ "deprecation", "serial" })
public class GridSelect extends CssLayout implements SelectionListener, ItemClickListener, ColumnResizeListener {
	private final Grid grid;
	private final Object itemId;
	private final Object propertyId;
	private final MemoizedSupplier<Component> nonSelectComponentSupplier;
	private final MemoizedSupplier<Component> selectComponentSupplier;
	private boolean updating;
	private boolean selected;
	private boolean focusOnAttach;

	public GridSelect(Grid grid, Object itemId, Object propertyId, Supplier<Component> nonSelectComponentSupplier,
			Supplier<Component> selectComponentSupplier) {
		this.grid = Objects.requireNonNull(grid);
		this.itemId = itemId;
		this.propertyId = propertyId;
		this.nonSelectComponentSupplier = Utils.Functions.memoize(
				() -> configureComponent(nonSelectComponentSupplier == null ? null : nonSelectComponentSupplier.get()));
		this.selectComponentSupplier = Utils.Functions.memoize(
				() -> configureComponent(nonSelectComponentSupplier == null ? null : selectComponentSupplier.get()));
		Connectors.addStateChangeListener(this, evt -> {
			if (evt.isRefreshTrigger())
				return;
			if (evt.isAttached()) {
				this.grid.addColumnResizeListener(this);
				this.grid.addSelectionListener(this);
				this.grid.addItemClickListener(this);
				this.update();
			} else {
				this.grid.removeColumnResizeListener(this);
				this.grid.removeSelectionListener(this);
				this.grid.removeItemClickListener(this);
			}
		}, ops -> ops.fireIfAttached(true));
		var layoutClickStyleName = CSSs.createStyleName(this);
		this.addLayoutClickListener(evt -> {
			if (this.selected)
				return;
			JavaScripts.clickParent("." + layoutClickStyleName,
					Map.of("shiftKey", evt.isShiftKey(), "ctrlKey", evt.isCtrlKey()));
		});

	}

	protected void update() {
		update(this.grid.getSelectedRows());
	}

	protected void update(Collection<Object> selection) {
		if (updating)
			return;
		try {
			updating = true;
			if (selection == null)
				selection = Collections.emptyList();
			var selectedItemId = Utils.Lots.stream(selection).reduce((f, s) -> s).orElse(null);
			Component component;
			if (Objects.equals(selectedItemId, this.itemId)) {
				this.selected = true;
				component = this.selectComponentSupplier.get();
			} else {
				this.selected = false;
				component = this.nonSelectComponentSupplier.get();
			}
			boolean exists = Utils.Lots.stream(this).filter(v -> v.equals(component)).findFirst().isPresent();
			if (exists)
				return;
			this.removeAllComponents();
			if (component == null)
				return;
			this.addComponent(component);
		} finally {
			updating = false;
		}
	}

	@Override
	public void itemClick(ItemClickEvent event) {
		this.focusOnAttach = shouldFocusOnAttach(event);
	}

	@Override
	public void columnResize(ColumnResizeEvent event) {
		var propertyId = event.getColumn().getPropertyId();
		if (!Objects.equals(propertyId, this.propertyId))
			return;

	}

	@Override
	public void select(SelectionEvent event) {
		this.update(event.getSelected());
	}

	private boolean shouldFocusOnAttach(ItemClickEvent event) {
		if (!Objects.equals(this.itemId, event.getItemId()))
			return false;
		if (!Objects.equals(this.propertyId, event.getPropertyId()))
			return false;
		return true;
	}

	protected Component configureComponent(Component component) {
		if (component == null)
			return component;
		if (component instanceof Focusable) {
			var styleName = CSSs.createStyleName(component);
			component.addAttachListener(evt -> {
				if (!this.focusOnAttach)
					return;
				((Focusable) component).focus();
				JavaScripts.select(String.format(".%s input", styleName));
			});
		}
		return component;
	}

}
