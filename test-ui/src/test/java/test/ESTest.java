package test;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import pl.allegro.tech.embeddedelasticsearch.EmbeddedElastic;
import pl.allegro.tech.embeddedelasticsearch.PopularProperties;

public class ESTest {

	private static final String ELASTICSEARCH_USERNAME = "user";
	private static final String ELASTICSEARCH_PASSWORD = "pass";

	public static void main(String[] args) throws IOException, InterruptedException {
		EmbeddedElastic embeddedElastic = EmbeddedElastic.builder().withElasticVersion("5.0.0")
				.withStartTimeout(Duration.ofMinutes(5).toMillis(), TimeUnit.MILLISECONDS)
				.withSetting(PopularProperties.HTTP_PORT, 8888)
				.withSetting(PopularProperties.CLUSTER_NAME, "my_cluster").withPlugin("analysis-stempel").build()
				.start();
		System.out.println("started");
		Thread.currentThread().join();
	}

}
