package com.lfp.vaadin.test.ui;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.lfp.joe.serial.Serials;

import com.vaadin.data.provider.DataCommunicator;
import com.vaadin.shared.Range;

import at.favre.lib.bytes.Bytes;
import elemental.json.Json;
import elemental.json.JsonArray;
import elemental.json.JsonObject;

public class TestDataCommunicator<T> extends DataCommunicator<T> {

	private Range currentRange = Range.emptyRange();
	private final Map<Integer, Bytes> currentRangeHashes = new ConcurrentHashMap<>();

	@Override
	protected void onRequestRows(int firstRowIndex, int numberOfRows, int firstCachedRowIndex, int cacheSize) {
		super.onRequestRows(firstRowIndex, numberOfRows, firstCachedRowIndex, cacheSize);
		var start = firstCachedRowIndex;
		var end = Math.max(firstRowIndex + numberOfRows, firstCachedRowIndex + cacheSize);
		this.currentRange = Range.between(start, end);
		currentRangeHashes.keySet().removeIf(k -> {
			if (!this.currentRange.contains(k))
				return true;
			return false;
		});
	}

	public Range getCurrentRange() {
		return currentRange;
	}

	protected void pushData(int firstIndex, List<T> data) {
		JsonArray dataArray = Json.createArray();
		var rowIndex = firstIndex--;
		int i = 0;
		for (T item : data) {
			var dataObject = getDataObject(item);
			dataArray.set(i++, dataObject);
			rowIndex++;
			if (getCurrentRange().contains(rowIndex)) {
				var je = Serials.Gsons.getJsonParser().parse(dataObject.toString());
				currentRangeHashes.put(rowIndex, Serials.Gsons.deepHash(je));
			}
		}

		getClientRpc().setData(firstIndex, dataArray);
		getActiveDataHandler().addActiveData(data.stream());
		getActiveDataHandler().cleanUp(data.stream());
	}

	public List<T> fetchCurrentItems() {
		var range = this.getCurrentRange();
		if (range.isEmpty())
			return List.of();
		return this.fetchItemsWithRange(range.getStart(), range.length());
	}
}
