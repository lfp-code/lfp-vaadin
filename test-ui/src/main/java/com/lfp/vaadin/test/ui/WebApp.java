package com.lfp.vaadin.test.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAsync
@SpringBootApplication(exclude = { org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration.class,
		org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration.class,
		org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration.class,
		org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration.class,
		org.springframework.boot.autoconfigure.elasticsearch.jest.JestAutoConfiguration.class })
@EnableScheduling
public class WebApp {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(new Object() {
	}.getClass().getEnclosingClass());

	public static void main(String[] args) {
		logger.info("starting app and scanning for vaadin ui's on classpath");
		SpringApplication.run(WebApp.class, args);
	}

}
