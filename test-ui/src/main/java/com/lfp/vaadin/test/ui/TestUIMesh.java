package com.lfp.vaadin.test.ui;

import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.data.property.Properties;
import com.lfp.vaadin.base.ui.UIX;
import com.lfp.vaadin.base.v7.container.ContainerLFP;
import com.lfp.vaadin.base.v7.converter.ConvertersV7;
import com.lfp.vaadin.v7.component.mesh.Mesh;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.v7.data.Container.Filter;
import com.vaadin.v7.data.Item;

@SpringUI(path = "/mesh/*")
@Push
@PreserveOnRefresh
@Widgetset("AppWidgetset")
public class TestUIMesh extends UIX {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private VerticalLayout mainLayout;

	@SuppressWarnings("deprecation")
	@Override
	protected void initx(VaadinRequest request) {
		this.setSizeFull();
		this.mainLayout = new VerticalLayout();
		this.setContent(this.mainLayout);
		this.mainLayout.setSizeFull();
		this.mainLayout.setMargin(false);
		var container = new ContainerLFP<String, TestBean>(TestBean.class, Properties.createSet(false, TestBean.class));
		for (int i = 0; i < 10; i++) {
			var bean = TestBean.builder().message("message " + i).build();
			container.add(bean.getId(), bean);
		}

		var grid = new Mesh<String, TestBean>(container);
		grid.setSizeFull();
		var col = grid.getColumn(TestBean.meta().createdAt().name());
		col.setRenderer(new com.vaadin.v7.ui.renderers.HtmlRenderer());
		ConvertersV7.setConverter(col, v -> {
			Date date = (Date) v;
			long elapsed = System.currentTimeMillis() - date.getTime();
			return "" + elapsed;
		});
		this.mainLayout.addComponent(grid);
		this.mainLayout.setExpandRatio(grid, 1f);
		Consumer<Boolean> modder = remove -> {
			if (remove) {
				if (container.size() > 0)
					container.removeItem(container.firstItemId());
			} else {
				var bean = TestBean.builder().message("new item").build();
				container.add(bean.getId(), bean);
			}
		};
		for (var remove : List.of(false, true)) {
			var button = new Button(remove ? "Remove" : "Add");
			this.mainLayout.addComponent(button);
			container.addContainerFilter(new Filter() {

				@Override
				public boolean passesFilter(Object itemId, Item item) throws UnsupportedOperationException {
					var bean = container.getBean(itemId).orElse(null);
					if (bean == null)
						return false;
					if (Utils.Strings.containsIgnoreCase(bean.getMessage(), "new item"))
						return true;
					return true;
				}

				@Override
				public boolean appliesToProperty(Object propertyId) {
					return Properties.getName(TestBean.meta().message()).equals(propertyId);
				}
			});
			button.addClickListener(evt -> modder.accept(remove));
		}

	}
}
