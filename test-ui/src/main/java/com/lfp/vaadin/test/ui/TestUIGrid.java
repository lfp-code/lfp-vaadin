package com.lfp.vaadin.test.ui;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.lots.iterable.ObservableList;
import com.lfp.vaadin.base.data.DataProviders;
import com.lfp.vaadin.base.data.property.Properties;
import com.lfp.vaadin.base.grid.GridLFP;
import com.lfp.vaadin.base.ui.UIX;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.HtmlRenderer;

import vjavax.observer.collection.CollectionChange;

@SpringUI(path = "/*")
@Push
@PreserveOnRefresh
@Widgetset("AppWidgetset")
public class TestUIGrid extends UIX {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private VerticalLayout mainLayout;

	@Override
	protected void initx(VaadinRequest request) {
		this.setSizeFull();
		this.mainLayout = new VerticalLayout();
		this.setContent(this.mainLayout);
		this.mainLayout.setSizeFull();
		this.mainLayout.setMargin(false);
		var list = new ObservableList<TestBean>();
		for (int i = 0; i < 1000; i++)
			list.add(TestBean.builder().message("message " + i).build());
		list.addChangeListener(evt -> {
			var added = Optional.ofNullable(evt.added()).map(CollectionChange::elements).orElse(List.of());
			var removed = Optional.ofNullable(evt.removed()).map(CollectionChange::elements).orElse(List.of());

		});
		var dp = DataProviders.create(list).withConfigurableFilter();
		var dc = new TestDataCommunicator<TestBean>();
		var grid = new GridLFP<TestBean>(Properties.createSet(false, TestBean.class), dc);
		grid.setDataProvider(dp);
		grid.setSizeFull();
		grid.getColumn(TestBean.meta().createdAt().name()).setRenderer(v -> {
			Date date = (Date) v;
			long elapsed = System.currentTimeMillis() - date.getTime();
			return "" + elapsed;
		}, new HtmlRenderer());
		this.mainLayout.addComponent(grid);
		this.mainLayout.setExpandRatio(grid, 1f);
		Consumer<Boolean> modder = remove -> {
			var sizeBefore = dc.getDataProviderSize();
			if (remove)
				list.remove(0);
			else {
				var bean = TestBean.builder().message("new item").build();
				list.add(bean);
			}
			var sizeAfter = dc.getDataProviderSize();
			System.out.println(sizeAfter);
		};
		for (var remove : List.of(false, true)) {
			var button = new Button(remove ? "Remove" : "Add");
			this.mainLayout.addComponent(button);
			dp.setFilter(v -> {
				if (!Utils.Strings.containsIgnoreCase(v.getMessage(), "new item"))
					return true;
				return true;
			});
			button.addClickListener(evt -> modder.accept(remove));
		}

	}
}
