package com.lfp.vaadin.component.location.form;

import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.Callable;

import org.joda.beans.MetaProperty;
import org.threadly.concurrent.future.ListenableFuture;
import org.vaadin.addons.autocomplete.AutocompleteExtension;
import org.vaadin.addons.autocomplete.converter.SuggestionCaptionConverter;
import org.vaadin.addons.autocomplete.converter.SuggestionValueConverter;
import org.vaadin.addons.autocomplete.event.SuggestionSelectListener;
import org.vaadin.addons.autocomplete.generator.SuggestionGenerator;

import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.events.eventbus.ValueNotifier;
import com.lfp.joe.location.Location;
import com.lfp.joe.location.search.LocationService;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.vaadin.base.css.CSS;
import com.lfp.vaadin.base.session.SessionExecutor;
import com.lfp.vaadin.base.ui.UIX;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.ValueContext;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import one.util.streamex.StreamEx;

public abstract class LocationFormLayout<S> extends VerticalLayout {

	private final ValueNotifier.Writeable<Location> valueNotifier = ValueNotifier.createWriteable();

	private final Binder<Location.Builder> binder = new Binder<>();
	private final LocationService<S> locationService;
	private final AutocompleteExtension<S> autoCompleteExtension;
	private final TextField numberAndStreet;
	private final TextField locality;
	private final TextField region;
	private final TextField postalCode;

	private boolean disableValueChangeEvent;
	private ListenableFuture<Location> setValueAsyncFuture;

	public LocationFormLayout(LocationService<S> locationService, ListenableFuture<Location> initialLocationFuture) {
		this.locationService = Objects.requireNonNull(locationService);
		this.setMargin(false);
		binder.setBean(Location.builder());
		{
			var mp = Location.meta().street();
			TextField tf = createTextField(mp);
			this.autoCompleteExtension = createAutocompleteExtension(tf);
			this.addComponent(tf);
			configureBind(tf, mp);
			this.numberAndStreet = tf;
		}
		var bottomLayout = new HorizontalLayout();
		bottomLayout.setMargin(false);
		this.addComponent(bottomLayout);
		{
			var mp = Location.meta().locality();
			TextField tf = createTextField(mp);
			configureBind(tf, mp);
			bottomLayout.addComponent(tf);
			bottomLayout.setExpandRatio(tf, 1f);
			this.locality = tf;
		}
		{
			var mp = Location.meta().region();
			TextField tf = createTextField(mp);
			tf.setWidth(75, Unit.PIXELS);
			configureBind(tf, mp);
			bottomLayout.addComponent(tf);
			this.region = tf;
		}
		{
			var mp = Location.meta().postalCode();
			TextField tf = createTextField(mp);
			tf.setWidth(100, Unit.PIXELS);
			configureBind(tf, mp);
			bottomLayout.addComponent(tf);
			this.postalCode = tf;
		}
		this.iterator().forEachRemaining(v -> {
			v.setWidthFull();
		});
		ValueChangeListener<Location> valueChangeListener = evt -> {
			var source = evt.getSource();
			var location = this.binder.getBean().build();
			boolean lookup;
			if (this.locality.equals(source)) {
				lookup = true;
				location = location.toBuilder().postalCode(null).build();
			} else if (this.region.equals(source)) {
				lookup = true;
				location = location.toBuilder().postalCode(null).build();
			} else if (this.postalCode.equals(source)) {
				lookup = true;
				location = location.toBuilder().locality(null).region(null).build();
			} else
				lookup = false;
			if (!lookup) {
				this.onBinderStatusChange(false);
				return;
			}
			location = location.toBuilder().latitude(null).longitude(null).number(null).street(null).text(null).build();
			var query = location.getText();
			setValueAsync(() -> this.locationService.searchLocations(query, 1).findFirst().orElse(null));
		};
		this.binder.addValueChangeListener(valueChangeListener);
		if (initialLocationFuture != null) {
			initialLocationFuture.resultCallback(l -> {
				if (l != null) {
					UIX.getCurrent().access(() -> {
						setValue(l);
						onBinderStatusChange(true);
					});
				}
			});
		}
	}

	protected void configureBind(HasValue<String> field, MetaProperty<String> mp) {
		if (field instanceof TextField)
			((TextField) field).setValueChangeMode(ValueChangeMode.BLUR);
		this.binder.forField(field).withNullRepresentation("").withValidator((v, ctx) -> validate(v, ctx, mp))
				.bind(b -> getValue(b, mp), (b, v) -> setValue(b, v, mp));
	}

	protected AutocompleteExtension<S> createAutocompleteExtension(TextField textField) {
		textField.setValueChangeTimeout(100);
		SuggestionGenerator<S> suggestionGenerator = (q, limit) -> {
			q = Utils.Strings.trimToNull(Utils.Strings.lowerCase(q));
			if (q == null)
				return Collections.emptyList();
			var suggs = Utils.Functions.unchecked(q, v -> {
				return this.locationService.search(v, limit);
			});
			return Utils.Lots.stream(suggs).nonNull().distinct().toList();
		};
		SuggestionValueConverter<S> valueConverter = suggestion -> {
			if (suggestion == null)
				return "";
			var value = convertValue(suggestion);
			if (value == null)
				return "";
			return value;
		};
		SuggestionCaptionConverter<S> captionConverter = (suggestion, q) -> {
			if (suggestion == null)
				return "";
			return Utils.Strings.trimToEmpty(convertCaption(suggestion, q));
		};
		var autocompleteExtension = new AutocompleteExtension<S>(textField);
		autocompleteExtension.setSuggestionGenerator(suggestionGenerator, valueConverter, captionConverter);
		SuggestionSelectListener<S> listener = evt -> {
			var suggestion = evt.getSelectedItem().orElse(null);
			if (suggestion == null) {
				setValue(null, true);
				onBinderStatusChange(true);
				return;
			}
			setValueAsync(() -> this.locationService.mapToLocation(suggestion).orElse(null));
		};
		autocompleteExtension.addSuggestionSelectListener(listener);
		CSS.getCurrent().add(".autocomplete-textfield.v-widget{width: 100% !important}");
		return autocompleteExtension;
	}

	protected void onBinderStatusChange(boolean skipValidation) {
		if (disableValueChangeEvent)
			return;
		if (!skipValidation && !this.binder.isValid())
			return;
		var location = this.binder.getBean().build();
		if (Objects.equals(location, valueNotifier.getValue().orElse(null)))
			return;
		valueNotifier.set(location);
	}

	protected TextField createTextField(MetaProperty<String> mp) {
		String caption;
		if (mp == null)
			caption = null;
		else {
			caption = getCaption(mp);
			if (Utils.Strings.isBlank(caption)) {
				caption = Utils.Lots.stream(Utils.Strings.splitByCharacterTypeCamelCase(mp.name()))
						.map(Utils.Strings::capitalize).joining(" ");
			}
		}
		var tf = new TextField(Utils.Strings.trimToNull(caption));
		tf.setRequiredIndicatorVisible(true);
		tf.setWidthFull();
		return tf;
	}

	protected String convertValue(S suggestion) {
		return null;
	}

	protected String getValidationErrorMessage(Component component) {
		var caption = component == null ? null : component.getCaption();
		caption = Utils.Strings.trimToNull(caption);
		if (caption == null)
			caption = "Field";
		return caption + " is required";
	}

	protected ValidationResult validate(String input, ValueContext valueContext, MetaProperty<String> metaProperty) {
		if (Utils.Strings.isBlank(input))
			return ValidationResult.error(getValidationErrorMessage(valueContext.getComponent().orElse(null)));
		return ValidationResult.ok();
	}

	protected String getValue(Location.Builder builder, MetaProperty<String> metaProperty) {
		if (builder == null)
			return "";
		if (metaProperty == null)
			return null;
		var meta = Location.meta();
		StreamEx<MetaProperty<?>> mpStream = StreamEx.of(metaProperty);
		if (meta.street().equals(metaProperty))
			mpStream = mpStream.prepend(meta.number());
		var valueStream = mpStream.map(mp -> JodaBeans.get(builder, mp));
		valueStream = valueStream.nonNull();
		var strStream = valueStream.map(Object::toString);
		strStream = strStream.map(Utils.Strings::trimToNull);
		strStream = strStream.nonNull();
		var result = strStream.joining(" ");
		return result;
	}

	protected void setValue(Location.Builder builder, String value, MetaProperty<String> metaProperty) {
		if (value == null)
			value = "";
		builder.set(metaProperty, value);
	}

	public ValueNotifier<Location> getValueNotifier() {
		return valueNotifier;
	}

	public void setValue(Location location) {
		setValue(location, false);
	}

	public void setValue(Location location, boolean disableValueChangeEvent) {
		this.disableValueChangeEvent = disableValueChangeEvent;
		try {
			this.binder.setBean(location == null ? Location.builder() : location.toBuilder());
			onBinderStatusChange(true);
		} finally {
			this.disableValueChangeEvent = false;
		}
	}

	protected synchronized void setValueAsync(Callable<Location> loader) {
		Threads.Futures.cancel(setValueAsyncFuture, true);
		var future = SessionExecutor.getCurrent().submit(loader);
		Threads.Futures.logFailureError(future, true, "set value async error");
		future = future.mapFailure(Throwable.class, t -> null);
		future.resultCallback(l -> {
			UIX.getCurrent().access(() -> {
				setValue(l);
				onBinderStatusChange(true);
			});
		});
		this.setValueAsyncFuture = future;
	}

	public AutocompleteExtension<S> getAutoCompleteExtension() {
		return autoCompleteExtension;
	}

	public TextField getNumberAndStreet() {
		return numberAndStreet;
	}

	public TextField getLocality() {
		return locality;
	}

	public TextField getRegion() {
		return region;
	}

	public TextField getPostalCode() {
		return postalCode;
	}

	protected abstract String getCaption(MetaProperty<?> metaProperty);

	protected abstract String convertCaption(S suggestion, String query);

}
