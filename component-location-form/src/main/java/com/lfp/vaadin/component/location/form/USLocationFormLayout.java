package com.lfp.vaadin.component.location.form;

import java.util.regex.Pattern;

import org.joda.beans.MetaProperty;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.location.Location;
import com.lfp.joe.location.USState;
import com.lfp.joe.location.search.LocationService;
import com.lfp.joe.utils.Utils;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.ValueContext;

public abstract class USLocationFormLayout<S> extends LocationFormLayout<S> {

	private static final Pattern ZIP_CODE_PATTERN = Pattern.compile("^[0-9]{5}(?:-[0-9]{4})?$");

	public USLocationFormLayout(LocationService<S> locationService, ListenableFuture<Location> initialLocationFuture) {
		super(locationService, initialLocationFuture);
	}

	@Override
	protected String getValue(Location.Builder builder, MetaProperty<String> metaProperty) {
		var value = super.getValue(builder, metaProperty);
		var meta = Location.meta();
		if (meta.region().equals(metaProperty)) {
			var state = USState.parse(value).orElse(null);
			return state == null ? "" : state.getAbbreviation();
		}
		return value;
	}

	@Override
	protected void setValue(Location.Builder builder, String value, MetaProperty<String> metaProperty) {
		var meta = Location.meta();
		if (meta.region().equals(metaProperty)) {
			var state = USState.parse(value).orElse(null);
			value = state == null ? null : state.toString();
		}
		super.setValue(builder, value, metaProperty);
	}

	@Override
	protected ValidationResult validate(String input, ValueContext valueContext, MetaProperty<String> metaProperty) {
		if (Utils.Strings.isNotBlank(input) && Location.meta().postalCode().equals(metaProperty)) {
			var matches = ZIP_CODE_PATTERN.matcher(input).matches();
			if (!matches)
				return ValidationResult.error(getValidationErrorMessage(valueContext.getComponent().orElse(null)));
		}
		return super.validate(input, valueContext, metaProperty);
	}

	@Override
	protected String getCaption(MetaProperty<?> metaProperty) {
		var meta = Location.meta();
		if (meta.street().equals(metaProperty))
			return "Address";
		if (meta.locality().equals(metaProperty))
			return "City";
		if (meta.region().equals(metaProperty))
			return "State";
		if (meta.postalCode().equals(metaProperty))
			return "Zip Code";
		return null;
	}

}
