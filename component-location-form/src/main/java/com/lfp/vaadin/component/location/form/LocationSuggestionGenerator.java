package com.lfp.vaadin.component.location.form;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Objects;

import org.vaadin.addons.autocomplete.generator.SuggestionGenerator;

import com.github.benmanes.caffeine.cache.Cache;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.location.Location;
import com.lfp.joe.location.search.LocationService;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public class LocationSuggestionGenerator implements SuggestionGenerator<Location> {
	private final LocationService locationService;
	private final Cache<String, List<Location>> cache;

	public LocationSuggestionGenerator(LocationService locationService) {
		this.locationService = Objects.requireNonNull(locationService);
		this.cache = Caches.newCaffeineBuilder(100_000, null, Duration.ofSeconds(30)).build();
	}

	@Override
	public List<Location> apply(String query, Integer limit) {
		query = Utils.Strings.trimToNull(Utils.Strings.lowerCase(query));
		if (query == null)
			return List.of();
		return this.cache.get(query, key -> {
			StreamEx<Location> stream;
			try {
				stream = this.locationService.search(key, limit == null ? -1 : limit);
			} catch (IOException e) {
				throw Utils.Exceptions.asRuntimeException(e);
			}
			stream = stream.distinct(v -> {
				var str = Utils.Strings.trimToEmpty(v.getText());
				str = str.toLowerCase();
				str = str.replaceAll("[^A-Za-z0-9]", " ");
				str = str.replaceAll("\\s+", " ");
				return str;
			});
			return stream.toImmutableList();
		});
	}

}
